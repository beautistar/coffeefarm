-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 30, 2018 at 11:33 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `coffeefarm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_activity`
--

CREATE TABLE IF NOT EXISTS `tb_activity` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `organic_activity` varchar(225) DEFAULT NULL,
  `organic_companyid` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tb_activity`
--

INSERT INTO `tb_activity` (`id`, `organic_activity`, `organic_companyid`) VALUES
(1, 'Weedwhacking', '1'),
(2, 'fertilizing', '1'),
(3, 'spraying', '1'),
(4, 'picking', '1'),
(5, 'clipping', '1'),
(6, 'planting', '1'),
(7, 'Weedwhacking', '2'),
(8, 'fertilizing', '2'),
(9, 'spraying', '2'),
(10, 'picking', '2'),
(11, 'clipping', '2'),
(12, 'planting', '2'),
(13, 'General cleaning', '2');

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE IF NOT EXISTS `tb_category` (
  `category_id` int(225) NOT NULL AUTO_INCREMENT,
  `category_company` int(225) DEFAULT NULL,
  `category_name` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`category_id`, `category_company`, `category_name`) VALUES
(1, 1, 'Market'),
(2, 1, 'Web'),
(3, 1, 'visitor center'),
(4, 2, 'Market'),
(5, 2, 'Web'),
(6, 2, 'visitor center'),
(7, 2, 'Farm Management'),
(8, 2, 'Wet Mill'),
(9, 2, 'WholeSale'),
(10, 2, 'WholeSale Hawaii'),
(11, 2, 'Green'),
(12, 2, 'Green Hawaii'),
(13, 2, 'ADJUSTMENT');

-- --------------------------------------------------------

--
-- Table structure for table `tb_categoryforexpeneses`
--

CREATE TABLE IF NOT EXISTS `tb_categoryforexpeneses` (
  `category_id` int(225) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(225) DEFAULT NULL,
  `category_company` int(225) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tb_categoryforexpeneses`
--

INSERT INTO `tb_categoryforexpeneses` (`category_id`, `category_name`, `category_company`) VALUES
(1, 'miscellaneous', 1),
(2, 'fertilizer', 1),
(3, 'Organic input', 1),
(4, 'gas', 1),
(5, 'miscellaneous', 2),
(6, 'fertilizer', 2),
(7, 'gas', 2),
(9, 'Payroll', 2),
(10, 'Rent', 2),
(11, 'ADJUSTMENT', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_client`
--

CREATE TABLE IF NOT EXISTS `tb_client` (
  `client_id` int(225) NOT NULL AUTO_INCREMENT,
  `client_company` int(225) DEFAULT NULL,
  `client_name` varchar(225) DEFAULT NULL,
  `client_email` varchar(225) DEFAULT NULL,
  `client_phone` varchar(225) DEFAULT NULL,
  `client_address` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `tb_client`
--

INSERT INTO `tb_client` (`client_id`, `client_company`, `client_name`, `client_email`, `client_phone`, `client_address`) VALUES
(1, 1, 'test', 'test@sdf', '234234', '0000-00-00'),
(2, 1, 'sdfa', 'sdfasf', '234234', '0000-00-00'),
(3, 2, 'WFM Honolulu', NULL, NULL, NULL),
(4, 2, 'WFM Kailua', NULL, NULL, NULL),
(5, 2, 'WFM Maui', NULL, NULL, NULL),
(6, 2, 'Airport Honolulu', NULL, NULL, NULL),
(7, 2, 'Airport Kona Aloha', NULL, NULL, NULL),
(8, 2, 'Airport Kona Hawaiian', NULL, NULL, NULL),
(9, 2, 'Airport Hilo', NULL, NULL, NULL),
(10, 2, 'Fresh Roasted Coffee', NULL, NULL, NULL),
(11, 2, 'Pooki''s Mahi', NULL, NULL, NULL),
(12, 2, 'Dee Fulton', NULL, NULL, NULL),
(13, 2, 'Mountain Thunder', NULL, NULL, NULL),
(14, 2, 'Adaptation', NULL, NULL, NULL),
(15, 2, 'Tapper', NULL, NULL, NULL),
(16, 2, 'Tae Kim', NULL, NULL, NULL),
(17, 2, 'Abundant Life', NULL, NULL, NULL),
(18, 2, 'WFM Queen', NULL, NULL, NULL),
(19, 2, 'Mari''s Garden', NULL, NULL, NULL),
(20, 2, 'Burman', NULL, NULL, NULL),
(21, 2, 'MT OGR', NULL, NULL, NULL),
(22, 2, 'KPM', NULL, NULL, NULL),
(23, 2, 'KGM', NULL, NULL, NULL),
(24, 2, 'DayLightMind', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_company`
--

CREATE TABLE IF NOT EXISTS `tb_company` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `company_password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tb_company`
--

INSERT INTO `tb_company` (`id`, `company_name`, `company_email`, `company_password`) VALUES
(1, 'test company', 'test@gmail.com', '123456'),
(2, 'Hala Tree', 'jean@halatreecoffee.com', 'Jea00112');

-- --------------------------------------------------------

--
-- Table structure for table `tb_expensses`
--

CREATE TABLE IF NOT EXISTS `tb_expensses` (
  `expenses_id` int(225) NOT NULL AUTO_INCREMENT,
  `expenses_date` varchar(225) DEFAULT NULL,
  `expenses_invoice` varchar(225) DEFAULT NULL,
  `expenses_provider` varchar(225) DEFAULT NULL,
  `expenses_amount` varchar(225) DEFAULT NULL,
  `expenses_farm` int(128) DEFAULT NULL,
  `expenses_company` int(128) DEFAULT NULL,
  `expenses_category` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`expenses_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `tb_expensses`
--

INSERT INTO `tb_expensses` (`expenses_id`, `expenses_date`, `expenses_invoice`, `expenses_provider`, `expenses_amount`, `expenses_farm`, `expenses_company`, `expenses_category`) VALUES
(25, '2018-05-14', '127', '', '18.68', 6, 2, 'gas'),
(26, '2018-05-14', '128', '', '20.33', 6, 2, 'gas'),
(24, '2018-05-14', '126', '', '19.78', 6, 2, 'gas'),
(23, '2018-05-14', '125', '', '166.67', 6, 2, 'miscellaneous'),
(22, '2018-05-14', '124', '', '74.47', 6, 2, 'gas'),
(21, '2018-05-14', '', 'Ford', '750', 6, 2, 'miscellaneous'),
(19, '2018-05-14', '122', '', '278.33', 6, 2, 'miscellaneous'),
(20, '2018-05-14', '123', '', '374.75', 6, 2, 'miscellaneous'),
(17, '2018-04-28', '', '', '118049.74', 6, 2, 'ADJUSTMENT'),
(18, '2018-05-14', '121', '', '39.93', 6, 2, 'gas'),
(27, '2018-05-14', '129', '', '19.41', 6, 2, 'gas'),
(28, '2018-05-14', '130', '', '116.15', 13, 2, 'miscellaneous'),
(29, '2018-05-14', '131', '', '740', 9, 2, 'miscellaneous'),
(30, '2018-05-14', '132', '', '46.87', 6, 2, 'miscellaneous'),
(31, '2018-05-14', '133', '', '28.02', 6, 2, 'gas'),
(32, '2018-05-14', '134', '', '370.85', 6, 2, 'miscellaneous'),
(33, '2018-05-14', '135', 'KRFC', '8040', 6, 2, 'fertilizer'),
(34, '2018-05-14', '136', '', '59.76', 6, 2, 'gas'),
(35, '2018-05-14', '4/19/18-5/4/18', '', '3884.7', 6, 2, 'Payroll'),
(36, '2018-05-25', '', 'test provider', '34', 3, 1, 'miscellaneous'),
(37, '2018-05-21', '', 'asdfaf', '3', 3, 1, 'miscellaneous'),
(38, '2018-05-24', '', 'asdfaf', '5', 4, 1, 'fertilizer'),
(39, '2018-05-28', '139', 'HKCC', '2211', 6, 2, 'miscellaneous'),
(40, '2018-05-28', '137', '', '54.04', 6, 2, 'miscellaneous'),
(41, '2018-05-28', '138', '', '41.99', 6, 2, 'gas'),
(42, '2018-05-28', '141', 'HKCC', '4000', 6, 2, 'miscellaneous'),
(43, '2018-05-28', '140', '', '315.88', 6, 2, 'miscellaneous'),
(44, '2018-05-28', '142', '', '502.13', 6, 2, 'miscellaneous'),
(45, '2018-05-28', '143', '', '1148.02', 6, 2, 'miscellaneous'),
(46, '2018-05-28', '144', '', '202.60', 6, 2, 'miscellaneous'),
(47, '2018-05-28', '145', '', '79.70', 6, 2, 'gas'),
(48, '2018-05-28', '146', '', '103.12', 6, 2, 'miscellaneous'),
(49, '2018-05-28', '147', '', '43.55', 6, 2, 'gas'),
(50, '2018-05-28', '148', '', '68.01', 6, 2, 'gas'),
(51, '2018-05-28', '149', '', '19.78', 6, 2, 'gas'),
(52, '2018-05-28', '150', '', '57.40', 6, 2, 'gas'),
(53, '2018-05-28', '151', '', '37.29', 6, 2, 'gas'),
(54, '2018-05-28', '152', '', '54.05', 6, 2, 'gas'),
(55, '2018-05-28', '153', '', '55.01', 6, 2, 'gas'),
(56, '2018-05-28', '154', '', '240', 6, 2, 'miscellaneous'),
(57, '2018-05-28', '155', '', '240', 6, 2, 'miscellaneous'),
(58, '2018-05-28', '5/3/2018 - 5/16/2018', '', '4035', 6, 2, 'Payroll'),
(59, '2018-05-28', 'may 2018', 'Kubota', '549.86', 6, 2, 'miscellaneous'),
(61, '2018-05-28', 'june 2018', '', '1250', 6, 2, 'Rent'),
(62, '2018-05-28', 'mule may 2018', '', '450', 6, 2, 'miscellaneous');

-- --------------------------------------------------------

--
-- Table structure for table `tb_farm`
--

CREATE TABLE IF NOT EXISTS `tb_farm` (
  `farm_id` int(225) NOT NULL AUTO_INCREMENT,
  `company_id` int(225) NOT NULL DEFAULT '0',
  `farm_name` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`farm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `tb_farm`
--

INSERT INTO `tb_farm` (`farm_id`, `company_id`, `farm_name`) VALUES
(2, 1, 'test1 farm'),
(3, 1, 'test2 farm'),
(4, 1, 'test3 farm'),
(5, 2, 'Hala Tree 82'),
(6, 2, 'Hala Tree 87'),
(7, 2, 'Hawaii Ola Miloli'),
(8, 2, 'Fire Island'),
(9, 2, 'Tapper'),
(10, 2, 'Dee Fulton'),
(11, 2, 'Adaptation'),
(12, 2, 'Monkey Pod - Andrea Pro'),
(13, 2, 'Deborah Richards'),
(14, 2, 'Mountain Thunder'),
(15, 2, 'MT OGR'),
(18, 2, 'OceanView'),
(17, 2, 'KPM');

-- --------------------------------------------------------

--
-- Table structure for table `tb_income`
--

CREATE TABLE IF NOT EXISTS `tb_income` (
  `income_id` int(225) NOT NULL AUTO_INCREMENT,
  `income_company` int(225) DEFAULT NULL,
  `income_date` varchar(225) DEFAULT NULL,
  `income_category` varchar(225) DEFAULT NULL,
  `income_client` varchar(225) DEFAULT NULL,
  `income_invoice` varchar(225) DEFAULT NULL,
  `income_amount` varchar(225) DEFAULT NULL,
  `income_taxstatus` varchar(10) NOT NULL DEFAULT 'No' COMMENT '0: not included , 1: included',
  `income_wholescale` varchar(128) DEFAULT NULL,
  `income_retail` varchar(128) DEFAULT NULL,
  `income_invicestatus` varchar(50) DEFAULT NULL COMMENT 'sent or pending',
  `income_productsold` varchar(225) DEFAULT NULL,
  `income_productquantity` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`income_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=148 ;

--
-- Dumping data for table `tb_income`
--

INSERT INTO `tb_income` (`income_id`, `income_company`, `income_date`, `income_category`, `income_client`, `income_invoice`, `income_amount`, `income_taxstatus`, `income_wholescale`, `income_retail`, `income_invicestatus`, `income_productsold`, `income_productquantity`) VALUES
(106, 2, '2018-05-07', 'visitor center', '', '', '79', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans,8 oz Peaberry', '1,1,1,1'),
(107, 2, '2018-05-08', 'Web', '', '439', '40', 'No', NULL, NULL, 'Pending', '1 lb Medium Ground', '1'),
(108, 2, '2018-05-08', 'visitor center', '', '', '108', 'No', NULL, NULL, 'Pending', '2 oz Cascara Tea,8 oz Peaberry,Jam Ginger Guava,1 lb Medium XF', '1,2,1,1'),
(109, 2, '2018-05-08', 'Web', '', '240 - Club', '240', 'No', NULL, NULL, 'Pending', '1 lb Dark Ground', '6'),
(104, 2, '2018-05-05', 'Web', '', '437', '10', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea', '1'),
(105, 2, '2018-05-07', 'Web', '', '438', '160', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea', '16'),
(101, 2, '2018-05-04', 'Web', '', '436', '22', 'No', NULL, NULL, 'Pending', '8 oz Medium Roast Ground', '1'),
(102, 2, '2018-05-04', 'visitor center', '', '', '158', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,2 oz Peaberry,8 oz Honeys,1 lb Peaberry,1 lb Dark XF', '2,2,1,1,1'),
(100, 2, '2018-05-08', 'WholeSale Hawaii', 'Airport Kona Hawaiian', 'HAW050818', '2920.5', 'No', NULL, NULL, 'Sent', '1 oz Mamaki Tea,8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans,8 oz Medium Roast Ground,8 oz Dark Roast Ground,8 oz Decaf,1 lb Medium XF,1 lb Dark XF', '24,36,36,24,18,36,18,18'),
(99, 2, '2018-05-08', 'WholeSale Hawaii', 'Airport Kona Aloha', 'AL050818', '532.5', 'No', NULL, NULL, 'Sent', '8 oz Dark Roast Ground,1 lb Medium XF', '18,12'),
(77, 1, '2018-04-28', 'Market', 'test', '', '', 'No', NULL, NULL, 'Pending', 'green coffee1', '3'),
(95, 2, '2018-04-30', 'visitor center', '', '', '40', 'No', NULL, NULL, 'Pending', '1 lb Decaf', '1'),
(96, 2, '2018-05-01', 'visitor center', '', '', '363', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,2 oz Cascara Tea,8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans,8 oz Honeys,Honey Christmas Berry,Jam Guava,Jam Ginger Guava,1 lb Dark XF', '9,3,1,2,3,2,3,1,1'),
(97, 2, '2018-05-05', 'WholeSale Hawaii', 'Airport Hilo', 'HIL050518', '895.5', 'No', NULL, NULL, 'Sent', '8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans,8 oz Medium Roast Ground,8 oz Dark Roast Ground,8 oz Decaf', '18,12,12,12,18'),
(98, 2, '2018-05-02', 'visitor center', '', '', '269', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,2 oz Cascara Tea,8 oz Dark Roast Whole Beans,8 oz Dark Roast Ground,8 oz Honeys,Honey Lehua Blossom,1 lb Dark XF', '3,2,2,2,1,2,2'),
(91, 2, '2018-04-30', 'ADJUSTMENT', '', '', '199337.38', 'No', NULL, NULL, 'Pending', '', ''),
(92, 2, '2018-05-01', 'Green', 'Burman', 'BUR050118', '2200', 'No', NULL, NULL, 'Pending', '1 lb Green Extra Fancy', '100'),
(93, 2, '2018-04-30', 'visitor center', '', '', '575', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,2 oz Cascara Tea,Black Elixir,1 lb Peaberry,Jam Guava,Club 6 months', '1,4,1,4,2,1'),
(94, 2, '2018-05-03', 'WholeSale Hawaii', 'Abundant Life', 'ABL050318', '408', 'No', NULL, NULL, 'Sent', '1 oz Mamaki Tea,8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans', '12,12,12'),
(110, 2, '2018-05-09', 'visitor center', '', '', '20', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea', '2'),
(111, 2, '2018-05-15', '', 'Airport Honolulu', 'HNL051518', '3707.5', 'No', NULL, NULL, 'Sent', '1 oz Mamaki Tea,2 oz Peaberry,8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans,8 oz Medium Roast Ground,8 oz Dark Roast Ground', '50,30,50,60,80,80'),
(112, 2, '2018-05-10', 'visitor center', '', '', '302', 'No', NULL, NULL, 'Pending', '2 oz sampler (3),8 oz Medium Roast Whole Beans,8 oz Peaberry,Honey Christmas Berry,Jam Ginger Guava,1 lb Roasted Decaf,1 lb Medium XF,1 lb Dark XF', '2,1,3,1,1,1,1,1'),
(113, 2, '2018-05-10', 'Web', '', '441', '16', 'No', NULL, NULL, 'Pending', 'Coffee Extract', '2'),
(114, 2, '2018-05-15', 'WholeSale Hawaii', 'Mountain Thunder', 'MT051518', '3600', 'No', NULL, NULL, 'Pending', '2 oz Cascara Tea', '960'),
(115, 2, '2018-05-11', 'visitor center', '', '', '80', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,Black Elixir,Honey Christmas Berry,1 lb Dark XF', '2,1,1,1'),
(119, 2, '2018-05-15', 'Web', '', '443, 444, 445', '250', 'No', NULL, NULL, 'Pending', '1 lb Roasted Peaberry,1 lb Roasted Honey,1 lb Medium XF,1 lb Dark XF', '1,1,2,2'),
(117, 2, '2018-05-14', 'visitor center', '', '', '82', 'No', NULL, NULL, 'Pending', '8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans,Honey Christmas Berry,Jam Ginger Guava', '2,1,1,1'),
(118, 2, '2018-05-15', 'Wet Mill', 'KPM', 'KPM051518', '60', 'No', NULL, NULL, 'Pending', '', ''),
(120, 2, '2018-05-15', 'visitor center', '', '', '90', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,2 oz Cascara Tea,8 oz Dark Roast Whole Beans,Honey Christmas Berry,1 lb Dark XF', '1,1,1,1,1'),
(121, 2, '2018-05-16', 'Web', '', '446', '88', 'No', NULL, NULL, 'Pending', '8 oz Dark Roast Ground', '4'),
(122, 2, '2018-05-16', 'visitor center', '', '', '80', 'No', NULL, NULL, 'Pending', '1 lb Medium XF,1 lb Dark XF', '1,1'),
(123, 2, '2018-05-17', 'WholeSale Hawaii', 'KGM', 'KGM051718', '276', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,8 oz Medium Roast Whole Beans', '16,12'),
(124, 2, '2018-05-17', 'Web', '', '447,448,449', '90', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,1 lb Dark XF', '1,2'),
(125, 2, '2018-05-17', 'visitor center', '', '', '44', 'No', NULL, NULL, 'Pending', '2 oz sampler (3)', '2'),
(126, 1, '2018-05-17', 'Market', 'test', '', '5', 'No', NULL, NULL, 'Pending', 'green coffee test1,cascara1', '3,2'),
(127, 2, '2018-05-18', 'visitor center', '', '', '240', 'No', NULL, NULL, 'Pending', '1 lb Roasted Decaf', '6'),
(128, 2, '2018-05-19', 'Web', '', '450', '80', 'No', NULL, NULL, 'Pending', '1 lb Roasted Decaf', '2'),
(129, 2, '2018-05-21', 'WholeSale Hawaii', 'DayLightMind', 'DLM052118', '600', 'No', NULL, NULL, 'Pending', '2 oz Cascara Tea', '160'),
(130, 2, '2018-05-21', 'WholeSale Hawaii', 'WFM Honolulu', '67965853', '300', 'No', NULL, NULL, 'Sent', '8 oz Medium Roast Whole Beans', '20'),
(131, 2, '2018-05-20', 'Web', '', '452,453', '260', 'No', NULL, NULL, 'Pending', '8 oz Dark Roast Whole Beans,1 lb Dark XF', '10,1'),
(132, 2, '2018-05-21', 'visitor center', '', '', '160', 'No', NULL, NULL, 'Pending', 'Black Elixir,8 oz Medium Roast Whole Beans,8 oz Peaberry,Honey Lehua Blossom', '1,3,3,1'),
(133, 2, '2018-05-22', 'Web', '', '454', '66', 'No', NULL, NULL, 'Pending', '8 oz Medium Roast Whole Beans', '3'),
(136, 2, '2018-05-22', 'visitor center', '', '', '66', 'No', NULL, NULL, 'Pending', '2 oz Cascara Tea,8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans', '1,1,2'),
(135, 2, '2018-05-14', 'Web', '', '442', '120', 'No', NULL, NULL, 'Pending', '1 lb Medium XF', '3'),
(139, 2, '2018-05-23', 'Web', '', '455,456', '270', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,1 lb Dark XF', '3,6'),
(138, 2, '2018-05-23', 'visitor center', '', '', '40', 'No', NULL, NULL, 'Pending', '1 lb Decaf', '1'),
(140, 2, '2018-05-24', 'Web', '', 'I024', '90', 'No', NULL, NULL, 'Pending', '1 lb Peaberry', '2'),
(141, 2, '2018-05-24', 'visitor center', '', '', '80', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,2 oz Cascara Tea,Black Elixir,Jam Ginger Guava,1 lb Medium XF', '1,1,1,1,1'),
(142, 2, '2018-05-25', 'WholeSale Hawaii', 'WFM Queen', '68065003', '600', 'No', NULL, NULL, 'Sent', '8 oz Medium Roast Whole Beans', '40'),
(143, 2, '2018-05-25', 'visitor center', '', '', '178', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,2 oz sampler (3),8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans,Honey Lehua Blossom,Jam Ginger Guava', '3,3,2,1,1,1'),
(144, 2, '2018-05-26', 'Web', '', '457', '44', 'No', NULL, NULL, 'Pending', '8 oz Medium Roast Whole Beans', '2'),
(145, 2, '2018-05-27', 'Web', '', 'I025', '90', 'No', NULL, NULL, 'Pending', '1 lb Peaberry', '2'),
(146, 2, '2018-05-29', 'visitor center', '', '', '243', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,2 oz Cascara Tea,Black Elixir,2 oz sampler (3),8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans,8 oz Peaberry,1 lb Dark XF', '3,2,1,1,1,1,3,1'),
(147, 2, '2018-05-28', '', 'Airport Honolulu', 'HNL052818', '3630', 'No', NULL, NULL, 'Pending', '1 oz Mamaki Tea,2 oz Cascara Tea,8 oz Medium Roast Whole Beans,8 oz Dark Roast Whole Beans,8 oz Medium Roast Ground,8 oz Dark Roast Ground,8 oz Decaf', '50,30,40,40,60,60,60');

-- --------------------------------------------------------

--
-- Table structure for table `tb_normaluser`
--

CREATE TABLE IF NOT EXISTS `tb_normaluser` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `normaluser_name` varchar(225) DEFAULT NULL,
  `normaluser_email` varchar(225) DEFAULT NULL,
  `normaluser_phone` varchar(225) DEFAULT NULL,
  `normaluser_address` varchar(225) DEFAULT NULL,
  `normaluser_password` varchar(225) DEFAULT NULL,
  `normaluser_permission` varchar(128) DEFAULT NULL,
  `normaluser_company` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tb_normaluser`
--

INSERT INTO `tb_normaluser` (`id`, `normaluser_name`, `normaluser_email`, `normaluser_phone`, `normaluser_address`, `normaluser_password`, `normaluser_permission`, `normaluser_company`) VALUES
(7, 'gg', 'gg@gmail.com', '66646', 'yyshe', 'hhh', '1,1,0,0,1,0,1,0,1,0,1', '1'),
(8, 'test team leader', 'teamleader@gmail.com', '234234', 'asdfa', '123', '0,0,0,1,0,1,1,0,0,0,0', '1'),
(10, 'Danielle', 'd.orlowski@live.com', '1 201 417 1506', '', 'Dan00112', '1,1,1,1,1,1,0,0,0,0,0', '2');

-- --------------------------------------------------------

--
-- Table structure for table `tb_organiclog`
--

CREATE TABLE IF NOT EXISTS `tb_organiclog` (
  `organiclog_id` int(255) NOT NULL AUTO_INCREMENT,
  `organic_companyid` int(255) NOT NULL DEFAULT '0',
  `organic_farmid` int(255) NOT NULL DEFAULT '0',
  `organic_userid` int(225) DEFAULT NULL,
  `organiclog_date` date DEFAULT NULL,
  `organiclog_acitivty` varchar(125) DEFAULT NULL,
  `organiclog_productused` varchar(125) DEFAULT NULL,
  `organiclog_productquantity` varchar(125) DEFAULT NULL,
  `organiclog_refofbill` varchar(225) DEFAULT NULL,
  `organic_teamlead` varchar(128) DEFAULT NULL COMMENT 'assigned team lead id',
  `organic_workers` varchar(225) DEFAULT NULL COMMENT 'assigned workers',
  `organic_times` varchar(225) DEFAULT NULL COMMENT 'work time for each workers',
  `organic_status` int(128) NOT NULL DEFAULT '0' COMMENT '0: pending, 1: completed',
  PRIMARY KEY (`organiclog_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `tb_organiclog`
--

INSERT INTO `tb_organiclog` (`organiclog_id`, `organic_companyid`, `organic_farmid`, `organic_userid`, `organiclog_date`, `organiclog_acitivty`, `organiclog_productused`, `organiclog_productquantity`, `organiclog_refofbill`, `organic_teamlead`, `organic_workers`, `organic_times`, `organic_status`) VALUES
(65, 1, 3, NULL, '2018-05-20', 'fertilizing', 'Charfish', '35', '', '6', 'test leader,you,tyujh', '10,10,20', 1),
(52, 2, 5, NULL, '2018-05-11', 'spraying', 'WPO', '5', '', NULL, NULL, NULL, 1),
(66, 2, 14, NULL, '2018-05-15', 'fertilizing', 'Charfish', '20', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '8,8,8', 1),
(21, 2, 15, NULL, '2018-04-23', 'spraying', 'WPO', '2', '', NULL, NULL, NULL, 1),
(20, 2, 14, NULL, '2018-04-23', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(19, 2, 11, NULL, '2018-04-24', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(18, 2, 6, NULL, '2018-04-24', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(17, 2, 6, NULL, '2018-04-25', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(28, 2, 6, NULL, '2018-04-26', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(29, 2, 6, NULL, '2018-04-27', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(30, 2, 6, NULL, '2018-04-27', 'fertilizing', 'Charfish', '40', '', NULL, NULL, NULL, 1),
(32, 2, 6, NULL, '2018-04-30', 'fertilizing', 'Charfish', '40', '', NULL, NULL, NULL, 1),
(33, 2, 10, NULL, '2018-05-01', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(34, 2, 10, NULL, '2018-05-01', 'fertilizing', 'Charfish', '17', '', NULL, NULL, NULL, 1),
(35, 2, 10, NULL, '2018-05-01', 'clipping', '', '', '', NULL, NULL, NULL, 1),
(36, 2, 11, NULL, '2018-05-02', 'spraying', 'WPO', '1', '', NULL, NULL, NULL, 1),
(37, 2, 5, NULL, '2018-05-02', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(38, 2, 5, NULL, '2018-05-02', 'fertilizing', 'Charfish', '30', '', NULL, NULL, NULL, 1),
(39, 2, 5, NULL, '2018-05-03', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(40, 2, 15, NULL, '2018-05-04', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(41, 2, 15, NULL, '2018-05-04', 'fertilizing', 'Charfish', '20', '', NULL, NULL, NULL, 1),
(42, 2, 9, NULL, '2018-05-04', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(43, 2, 9, NULL, '2018-05-04', 'fertilizing', 'Charfish', '10', '', NULL, NULL, NULL, 1),
(44, 2, 13, NULL, '2018-05-07', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(45, 2, 13, NULL, '2018-05-07', 'spraying', 'WPO', '2', '', NULL, NULL, NULL, 1),
(46, 2, 13, NULL, '2018-05-07', 'fertilizing', 'Charfish', '12', '', NULL, NULL, NULL, 1),
(47, 2, 9, NULL, '2018-05-08', 'spraying', 'WPO', '2', '', NULL, NULL, NULL, 1),
(48, 2, 6, NULL, '2018-05-09', 'spraying', 'WPO', '7', '', NULL, NULL, NULL, 1),
(49, 2, 6, NULL, '2018-05-09', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(50, 2, 8, NULL, '2018-05-10', 'Weedwhacking', '', '', '', NULL, NULL, NULL, 1),
(51, 2, 8, NULL, '2018-05-10', 'fertilizing', 'Charfish', '25', '', NULL, NULL, NULL, 1),
(64, 1, 4, NULL, '2018-05-15', 'spraying', 'WPO', '6', '', '1', 'teamlead,rrrr4444', '89,4', 1),
(71, 2, 8, NULL, '2018-05-18', 'spraying', 'WPO', '3', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '2,2,2', 1),
(70, 2, 6, NULL, '2018-05-17', 'General cleaning', '', '', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '8,8,8', 1),
(69, 2, 6, NULL, '2018-05-16', 'General cleaning', '', '', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '8,8,8', 1),
(67, 2, 14, NULL, '2018-05-15', 'spraying', 'WPO', '3', '', '8', NULL, NULL, 1),
(59, 2, 14, NULL, '2018-05-14', 'Weedwhacking', '', '', '', '8', 'Albert Andon,Thomas Damarlin', '8,8', 1),
(63, 1, 3, NULL, '2018-05-15', 'spraying', 'Charfish', '2', '', '6', 'test leader,zhan worker', '46.6,3', 1),
(62, 1, 2, NULL, '2018-05-15', 'Weedwhacking', 'Charfish', '2', '', '6', 'test leader,zhan worker', '34,6', 1),
(72, 2, 11, NULL, '2018-05-18', 'spraying', 'WPO', '1', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '2,2,2', 1),
(73, 2, 9, NULL, '2018-05-18', 'spraying', 'WPO', '2', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '2,2,2', 1),
(74, 2, 10, NULL, '2018-05-18', 'spraying', 'WPO', '2', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '2,2,2', 1),
(75, 2, 5, NULL, '2018-05-21', 'Weedwhacking', '', '', '', '8', 'Albert Andon,Thomas Damarlin', '8,8', 1),
(76, 1, 2, NULL, '2018-05-21', 'planting', 'clay', '2', '', '1', NULL, NULL, 0),
(77, 2, 11, NULL, '2018-05-22', 'Weedwhacking', '', '', '', '8', 'Primus,Albert Andon', '4,4', 1),
(78, 2, 9, NULL, '2018-05-22', 'clipping', '', '', '', '8', 'Primus,Albert Andon', '4,4', 1),
(79, 2, 5, NULL, '2018-05-22', 'Weedwhacking', '', '', '', '8', 'Thomas Damarlin', '8', 1),
(80, 2, 5, NULL, '2018-05-23', 'Weedwhacking', '', '', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '4,8,8', 1),
(83, 2, 5, NULL, '2018-05-23', 'spraying', 'foliar', '4', '', '8', 'Primus', '4', 1),
(82, 2, 5, NULL, '2018-05-23', 'spraying', 'BadgeX2', '4', '', '8', NULL, NULL, 1),
(84, 2, 18, NULL, '2018-05-24', 'General cleaning', '', '', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '8,8,8', 1),
(85, 2, 10, NULL, '2018-05-25', 'clipping', '', '', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '4,4,4', 1),
(86, 2, 12, NULL, '2018-05-25', 'clipping', '', '', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '2,2,2', 1),
(87, 2, 13, NULL, '2018-05-25', 'clipping', '', '', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '2,2,2', 1),
(88, 2, 15, NULL, '2018-05-29', 'Weedwhacking', '', '', '', '8', 'Primus,Albert Andon,Thomas Damarlin', '8,8,8', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pickerlead`
--

CREATE TABLE IF NOT EXISTS `tb_pickerlead` (
  `pickerlead_id` int(225) NOT NULL AUTO_INCREMENT,
  `leadidofpick` int(225) NOT NULL DEFAULT '-1' COMMENT 'picker leader id for pickers',
  `pickerlead_company` varchar(225) NOT NULL DEFAULT '0',
  `pickerlead_name` varchar(225) DEFAULT NULL,
  `pickerlead_email` varchar(225) DEFAULT NULL,
  `pickerlead_phone` varchar(225) DEFAULT NULL,
  `pickerlead_address` varchar(225) DEFAULT NULL,
  `pickerlead_password` varchar(225) DEFAULT NULL,
  `rate_lb` varchar(128) DEFAULT NULL,
  `picker_type` int(128) NOT NULL DEFAULT '0' COMMENT '0: picker leader, 1: picker',
  `deletestatus` int(128) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pickerlead_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tb_pickerlead`
--

INSERT INTO `tb_pickerlead` (`pickerlead_id`, `leadidofpick`, `pickerlead_company`, `pickerlead_name`, `pickerlead_email`, `pickerlead_phone`, `pickerlead_address`, `pickerlead_password`, `rate_lb`, `picker_type`, `deletestatus`) VALUES
(11, -1, '1', 'pickerleader2', 'pickerlead32@gmail.com', '434', 'sfadf', '123', '0.6', 0, 0),
(10, 9, '1', 'picker', 'pickesdfsdrle@gmail.com', 'we', 'sdf', '', '23', 1, 0),
(9, -1, '1', 'pickerleader1', 'pickerleader1@gmail.com', '', '', '123', '0.6', 0, 0),
(14, -1, '1', 'thr', 'there@gmail.com', '23432', 'sdfa', '123', '21', 0, 0),
(15, 9, '1', 'testpicker5', 'testpicker@gmail.com', '234', 'safd', '', '2', 1, 0),
(16, 14, '1', 'testpicker_1', 'testpicker_1@gmail.com', '234', 'safdsdfsdd', '', '2', 1, 1),
(17, 11, '1', 'fe', 'dsfsdfsr545dfsd', '2373', 'sdfa', '', '2', 1, 0),
(18, -1, '2', 'Missila', 'Missila', '', '', 'mmm', '.75', 0, 0),
(19, 18, '2', 'Merlinda', '', '', '', '', '.65', 1, 0),
(20, -1, '1', 'yui', 'jjsshh@', '68', 'hhdjjdjjg', 'zhh', '1', 0, 1),
(21, 20, '1', 'uud', 'sjkd', '44', 'ws', '', '4', 1, 0),
(22, 14, '1', 'ui', 'jd@jx', '488', 'qfc', '', '1', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pickingandreceiving`
--

CREATE TABLE IF NOT EXISTS `tb_pickingandreceiving` (
  `picking_id` int(255) NOT NULL AUTO_INCREMENT,
  `pickingtype` int(128) NOT NULL DEFAULT '0' COMMENT '0: picking, 1: receiving',
  `picking_farm` varchar(128) DEFAULT NULL,
  `picking_pickerleaderid` varchar(256) DEFAULT NULL,
  `picking_date` varchar(128) DEFAULT NULL,
  `picking_pickername` varchar(128) DEFAULT NULL,
  `picking_lbs` varchar(128) DEFAULT NULL,
  `used_lbscherries` varchar(225) NOT NULL DEFAULT '0',
  `picking_bags` varchar(128) DEFAULT NULL,
  `picking_company` int(225) DEFAULT NULL,
  `receiving_clientid` int(255) DEFAULT NULL,
  `receiving_quality` varchar(256) DEFAULT NULL,
  `receiving_comment` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`picking_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `tb_pickingandreceiving`
--

INSERT INTO `tb_pickingandreceiving` (`picking_id`, `pickingtype`, `picking_farm`, `picking_pickerleaderid`, `picking_date`, `picking_pickername`, `picking_lbs`, `used_lbscherries`, `picking_bags`, `picking_company`, `receiving_clientid`, `receiving_quality`, `receiving_comment`) VALUES
(13, 0, '2', '11', '2018-05-26', 'pickerleader2,fe', '10,2', '-4', '3,5', 1, NULL, NULL, NULL),
(14, 0, '3', '9', '2018-05-26', 'pickerleader1,testpicker5', '4,6', '-6', '2,3', 1, NULL, NULL, NULL),
(17, 0, '2', '11', '2018-05-26', 'fe', '3', '0', '65', 1, NULL, NULL, NULL),
(20, 1, '4', NULL, '2018-05-26', NULL, '23', '-4', '233', 1, 2, '', 'sdfsd'),
(22, 1, '3', NULL, '2018-05-26', NULL, '34', '0', '3', 1, 1, '', ''),
(27, 1, '-1', NULL, '2018-05-25', NULL, '100', '100', '1', 2, 12, '', ''),
(24, 1, '2', NULL, '2018-05-26', NULL, '23', '0', '2', 1, 1, '', 'sdfds'),
(25, 1, '-1', NULL, '2018-05-26', NULL, '234', '0', '2', 1, 1, 'sdf', 'dsf'),
(26, 1, '-1', NULL, '2018-05-26', NULL, '30', '3', '', 1, 1, '', ''),
(28, 0, '8', '18', '2018-05-26', 'Missila', '100', '100', '1', 2, NULL, NULL, NULL),
(29, 1, '2', NULL, '2018-05-27', NULL, '25', '7', '2', 1, 1, 'rtt', 'dghh');

-- --------------------------------------------------------

--
-- Table structure for table `tb_processing`
--

CREATE TABLE IF NOT EXISTS `tb_processing` (
  `processing_id` int(255) NOT NULL AUTO_INCREMENT,
  `processing_type` int(128) NOT NULL DEFAULT '0' COMMENT '0: from picking , 1: from client',
  `receivingorpickinglog_id` int(128) DEFAULT NULL,
  `processing_date` varchar(128) DEFAULT NULL,
  `processed_lbs` varchar(255) NOT NULL DEFAULT '0',
  `parchment_date` varchar(225) DEFAULT NULL,
  `moisture` varchar(225) DEFAULT NULL,
  `skin_quantity` varchar(256) DEFAULT NULL,
  `method` varchar(128) DEFAULT NULL,
  `skin_weightproduced` varchar(256) DEFAULT NULL,
  `processing_farmorclientname` varchar(256) DEFAULT NULL,
  `lbs_parchmentproduced` varchar(256) DEFAULT NULL,
  `burlap_bags` varchar(256) NOT NULL DEFAULT '0',
  `grainpro_bags` varchar(256) NOT NULL DEFAULT '0',
  `comment` varchar(512) DEFAULT NULL,
  `company_id` varchar(225) DEFAULT NULL,
  `status` varchar(128) NOT NULL DEFAULT 'Pending' COMMENT 'Pending or closed',
  PRIMARY KEY (`processing_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `tb_processing`
--

INSERT INTO `tb_processing` (`processing_id`, `processing_type`, `receivingorpickinglog_id`, `processing_date`, `processed_lbs`, `parchment_date`, `moisture`, `skin_quantity`, `method`, `skin_weightproduced`, `processing_farmorclientname`, `lbs_parchmentproduced`, `burlap_bags`, `grainpro_bags`, `comment`, `company_id`, `status`) VALUES
(14, 1, 26, '2018-05-26', '5', '', '', NULL, '', NULL, 'test', '2', '', '', '', '1', 'Closed'),
(18, 1, 27, '2018-05-25', '50', '2018-05-26', '12', NULL, 'Naturals', NULL, 'Dee Fulton', '15', '1', '1', '', '2', 'Closed'),
(19, 0, 28, '2018-05-26', '50', '2018-05-28', '', NULL, 'Honey', NULL, 'Fire Island', '20', '', '', '', '2', 'Closed'),
(20, 0, 28, '2018-05-26', '50', '', '', NULL, 'Naturals', NULL, 'Fire Island', '0', '', '', '', '2', 'Pending'),
(21, 1, 29, '2018-05-27', '2', '2018-05-27', '2', NULL, 'method 2', NULL, 'test', '25', '5', 'r', '', '1', 'Closed'),
(17, 1, 27, '2018-05-25', '50', '', '', NULL, 'Wet Fermented (12 hours)', NULL, 'Dee Fulton', '0', '', '', '', '2', 'Pending'),
(24, 1, 29, '2018-05-27', '1', '', '3', NULL, 'method 2', NULL, 'test', '5', '3', '2', 'jd', '1', 'Closed'),
(25, 1, 29, '2018-05-27', '1', '', '3', NULL, 'method 2', NULL, 'test', '5', '3', '2', 'jd', '1', 'Closed'),
(26, 1, 29, '2018-05-27', '1', '', '3', NULL, 'method 2', NULL, 'test', '5', '3', '2', 'wer', '1', 'Closed'),
(27, 1, 29, '2018-05-27', '1', '', '3', NULL, 'method 2', NULL, 'test', '5', '3', '2', 'wer', '1', 'Closed'),
(28, 1, 29, '2018-05-27', '1', '', '3', NULL, 'method 2', NULL, 'test', '5', '3', '2', 'jd', '1', 'Closed');

-- --------------------------------------------------------

--
-- Table structure for table `tb_processingmethods`
--

CREATE TABLE IF NOT EXISTS `tb_processingmethods` (
  `me_id` int(255) NOT NULL AUTO_INCREMENT,
  `me_company` int(255) DEFAULT NULL,
  `me_methodname` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`me_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tb_processingmethods`
--

INSERT INTO `tb_processingmethods` (`me_id`, `me_company`, `me_methodname`) VALUES
(1, 1, 'method 1'),
(2, 1, 'method 2'),
(3, 2, 'Wet Fermented (12 hours)'),
(4, 2, 'Honey'),
(5, 2, 'Naturals');

-- --------------------------------------------------------

--
-- Table structure for table `tb_productfororganiclog`
--

CREATE TABLE IF NOT EXISTS `tb_productfororganiclog` (
  `product_id` int(50) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(225) DEFAULT NULL,
  `company_id` int(50) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tb_productfororganiclog`
--

INSERT INTO `tb_productfororganiclog` (`product_id`, `product_name`, `company_id`) VALUES
(1, 'Charfish', 1),
(2, 'WPO', 1),
(3, 'foliar', 1),
(4, 'copper', 1),
(5, 'clay', 1),
(6, 'Charfish', 2),
(7, 'WPO', 2),
(8, 'foliar', 2),
(9, 'copper', 2),
(10, 'clay', 2),
(11, 'BadgeX2', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_productforproduction`
--

CREATE TABLE IF NOT EXISTS `tb_productforproduction` (
  `product_id` int(225) NOT NULL AUTO_INCREMENT,
  `product_company` int(225) DEFAULT NULL,
  `product_name` varchar(225) DEFAULT NULL,
  `product_farm` int(128) DEFAULT NULL,
  `product_price` varchar(128) DEFAULT '0',
  `product_tax` int(50) NOT NULL DEFAULT '0',
  `product_type` varchar(20) DEFAULT NULL,
  `product_quantity` int(225) DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

--
-- Dumping data for table `tb_productforproduction`
--

INSERT INTO `tb_productforproduction` (`product_id`, `product_company`, `product_name`, `product_farm`, `product_price`, `product_tax`, `product_type`, `product_quantity`) VALUES
(1, 1, 'green coffee1', NULL, '25', 1, 'Green', -319),
(2, 1, 'Cherries', 3, '45', 1, 'Roasted', 18),
(3, 1, 'green coffee test1', NULL, '25', 1, 'Green', -3),
(4, 1, 'cascara1', NULL, '45', 1, 'Roasted', 32),
(5, 2, '1 oz Mamaki Tea', NULL, '10', 0, 'Tea', -201),
(13, 2, '2 oz Peaberry', NULL, '7', 0, 'Coffee', -32),
(7, 2, '2 oz Cascara Tea', NULL, '10', 0, 'Tea', -1165),
(40, 2, 'Black Elixir', NULL, '12', 0, 'Other Coffee', -5),
(9, 2, '2 oz Medium roast ', NULL, '6', 0, 'Coffee', 0),
(15, 2, '2 oz sampler (3)', NULL, '22', 0, 'Coffee', -8),
(11, 2, '2 oz Dark roast', NULL, '6', 0, 'Coffee', 0),
(17, 2, '8 oz Medium Roast Whole Beans', NULL, '22', 0, 'Coffee', -245),
(22, 2, '8 oz Dark Roast Whole Beans', NULL, '22', 0, 'Coffee', -181),
(21, 2, '8 oz Medium Roast Ground', NULL, '22', 0, 'Coffee', -177),
(23, 2, '8 oz Dark Roast Ground', NULL, '22', 0, 'Coffee', -194),
(24, 2, '8 oz Peaberry', NULL, '25', 0, 'Coffee', -12),
(25, 2, '8 oz Naturals', NULL, '25', 0, 'Coffee', 0),
(26, 2, '8 oz Honeys', NULL, '25', 0, 'Coffee', -5),
(27, 2, '8 oz Decaf', NULL, '22', 0, 'Coffee', -114),
(28, 2, '1 lb Medium Whole Beans', NULL, '40', 0, 'Coffee', 0),
(29, 2, '1 lb Medium Ground', NULL, '40', 0, 'Coffee', -1),
(30, 2, '1 lb Dark Whole Beans', NULL, '40', 0, 'Coffee', 0),
(31, 2, '1 lb Dark Ground', NULL, '40', 0, 'Coffee', -6),
(32, 2, '1 lb Peaberry', NULL, '45', 0, 'Coffee', -9),
(33, 2, '1 lb Naturals', NULL, '45', 0, 'Coffee', 0),
(34, 2, '1 lb Honey', NULL, '45', 0, 'Coffee', 0),
(35, 2, '1 lb Decaf', NULL, '40', 0, 'Coffee', -2),
(36, 2, '1 lb Green Extra Fancy', NULL, '22', 0, 'Green', 381),
(37, 2, '1 lb Green Prime', NULL, '20', 0, 'Green', 1436),
(38, 2, '1 lb Green Peaberry', NULL, '25', 0, 'Green', 4),
(39, 2, '1 lb Green Naturals', NULL, '25', 0, 'Green', 105),
(41, 2, 'Coffee Extract', NULL, '8', 0, 'Other Coffee', -2),
(42, 2, 'Honey Coffee Blossom', NULL, '8', 0, 'Honey', 0),
(43, 2, 'Honey Christmas Berry', NULL, '8', 0, 'Honey', -6),
(44, 2, 'Honey Lehua Blossom', NULL, '8', 0, 'Honey', -4),
(45, 2, 'Jam Guava', NULL, '8', 0, 'Jam', -5),
(46, 2, 'Jam Ginger Guava', NULL, '8', 0, 'Jam', -6),
(47, 2, 'Club 6 months', NULL, '240', 0, 'Coffee', -1),
(48, 2, '1 lb Green Decaf', NULL, '25', 0, 'Green', 60),
(49, 2, '1 lb Roasted Medium', NULL, '0', 0, 'Roasted', 303),
(50, 2, '1 lb Roasted Dark', NULL, '0', 0, 'Roasted', 272),
(51, 2, '1 lb Roasted Peaberry', NULL, '0', 0, 'Roasted', -1),
(52, 2, '1 lb Roasted Naturals', NULL, '0', 0, 'Roasted', 0),
(53, 2, '1 lb Roasted Honey', NULL, '0', 0, 'Roasted', -1),
(54, 2, '1 lb Roasted Decaf', NULL, '0', 0, 'Roasted', 21),
(55, 2, '1 lb Medium XF', NULL, '0', 0, 'Roasted', -39),
(56, 2, '1 lb Dark XF', NULL, '0', 0, 'Roasted', -18),
(57, 2, 'Cherries', 5, '0', 0, 'Raw material', 13),
(58, 2, 'Parchment', 5, '0', 0, 'Raw material', 0),
(59, 2, 'Parchment Natural', NULL, '0', 0, 'Raw material', 0),
(60, 2, '1lb Green Honey', NULL, '25', 0, 'Green', 21),
(62, 1, 'Cherries', 4, '45', 1, 'Roasted', 13),
(61, 1, 'Cherries', 2, '45', 1, 'Roasted', 5),
(63, 2, 'Cherries', 6, '0', 0, 'Raw material', 13),
(64, 2, 'Cherries', 7, '0', 0, 'Raw material', 13),
(65, 2, 'Cherries', 8, '0', 0, 'Raw material', 13),
(66, 2, 'Cherries', 9, '0', 0, 'Raw material', 13),
(67, 2, 'Cherries', 10, '0', 0, 'Raw material', 13),
(68, 2, 'Cherries', 11, '0', 0, 'Raw material', 13),
(69, 2, 'Cherries', 12, '0', 0, 'Raw material', 13),
(70, 2, 'Cherries', 13, '0', 0, 'Raw material', 13),
(71, 2, 'Cherries', 14, '0', 0, 'Raw material', 13),
(72, 2, 'Cherries', 15, '0', 0, 'Raw material', 13),
(73, 2, 'Cherries', 16, '0', 0, 'Raw material', 13),
(74, 2, 'Cherries', 17, '0', 0, 'Raw material', 13),
(75, 2, 'Parchment', 6, '0', 0, 'Raw material', 0),
(76, 2, 'Parchment', 7, '0', 0, 'Raw material', 0),
(77, 2, 'Parchment', 8, '0', 0, 'Raw material', 20),
(78, 2, 'Parchment', 9, '0', 0, 'Raw material', 0),
(79, 2, 'Parchment', 10, '0', 0, 'Raw material', 0),
(80, 2, 'Parchment', 11, '0', 0, 'Raw material', 0),
(81, 2, 'Parchment', 12, '0', 0, 'Raw material', 0),
(82, 2, 'Parchment', 13, '0', 0, 'Raw material', 0),
(83, 2, 'Parchment', 14, '0', 0, 'Raw material', 0),
(84, 2, 'Parchment', 15, '0', 0, 'Raw material', 0),
(85, 2, 'Parchment', 17, '0', 0, 'Raw material', 0),
(86, 2, 'Parchment', 18, '0', 0, 'Raw material', 0),
(87, 1, 'Parchment', 2, '0', 0, 'Raw material', 34),
(88, 1, 'Parchment', 3, '0', 0, 'Raw material', 1),
(89, 1, 'Parchment', 4, '0', 0, 'Raw material', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_production`
--

CREATE TABLE IF NOT EXISTS `tb_production` (
  `production_id` int(225) NOT NULL AUTO_INCREMENT,
  `production_companyid` int(225) DEFAULT NULL,
  `production_farmid` varchar(225) DEFAULT NULL,
  `production_userid` int(225) DEFAULT NULL,
  `production_date` varchar(128) DEFAULT NULL,
  `production_product` varchar(128) DEFAULT NULL,
  `production_quantity` varchar(128) DEFAULT NULL,
  `production_batch` varchar(128) DEFAULT NULL,
  `production_moisture` varchar(128) DEFAULT NULL,
  `production_millprocessed` varchar(128) DEFAULT NULL,
  `production_roasted` varchar(128) NOT NULL,
  PRIMARY KEY (`production_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `tb_production`
--

INSERT INTO `tb_production` (`production_id`, `production_companyid`, `production_farmid`, `production_userid`, `production_date`, `production_product`, `production_quantity`, `production_batch`, `production_moisture`, `production_millprocessed`, `production_roasted`) VALUES
(45, 2, 'Hala Tree 82', NULL, '2018-05-25', '1 lb Green Prime', '200', 'keei', '', 'keii', ''),
(44, 2, 'Hala Tree 82', NULL, '2018-05-28', '1 lb Green Prime', '100', 'hkcc', '', 'hkcc', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_producttypeforproduction`
--

CREATE TABLE IF NOT EXISTS `tb_producttypeforproduction` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `product_type` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_provider`
--

CREATE TABLE IF NOT EXISTS `tb_provider` (
  `provider_id` int(225) NOT NULL AUTO_INCREMENT,
  `provider_name` varchar(128) DEFAULT NULL,
  `provider_email` varchar(128) DEFAULT NULL,
  `provider_phone` varchar(128) DEFAULT NULL,
  `provider_address` varchar(128) DEFAULT NULL,
  `provider_company` int(128) DEFAULT NULL,
  PRIMARY KEY (`provider_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tb_provider`
--

INSERT INTO `tb_provider` (`provider_id`, `provider_name`, `provider_email`, `provider_phone`, `provider_address`, `provider_company`) VALUES
(1, 'test provider', 'testprovider@gmail.com', '234234', 'test address', 1),
(2, 'asdfaf', 'sdaf', '323423', 'sdfa', 1),
(3, 'Ford', NULL, NULL, NULL, 2),
(4, 'Kubota', NULL, NULL, NULL, 2),
(5, 'KRFC', NULL, NULL, NULL, 2),
(6, 'HKCC', NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_roastlog`
--

CREATE TABLE IF NOT EXISTS `tb_roastlog` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `roast_company` int(128) DEFAULT NULL,
  `roast_farm` varchar(225) DEFAULT NULL,
  `roast_date` varchar(256) DEFAULT NULL,
  `roast_typein` varchar(256) DEFAULT NULL,
  `roast_typeout` varchar(256) DEFAULT NULL,
  `roast_intemp` varchar(256) DEFAULT NULL,
  `roast_outtemp` varchar(256) DEFAULT NULL,
  `roast_duration` varchar(256) DEFAULT NULL,
  `roast_lbsin` varchar(256) DEFAULT NULL,
  `roast_lbsout` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `tb_roastlog`
--

INSERT INTO `tb_roastlog` (`id`, `roast_company`, `roast_farm`, `roast_date`, `roast_typein`, `roast_typeout`, `roast_intemp`, `roast_outtemp`, `roast_duration`, `roast_lbsin`, `roast_lbsout`) VALUES
(26, 2, '6', '2018-05-01', '1 lb Green Extra Fancy', '1 lb Roasted Dark', '176', '440', '14:07', '12', '10.2'),
(27, 2, '6', '2018-05-01', '1 lb Green Prime', '1 lb Roasted Medium', '176', '412', '11:09', '12', '10'),
(28, 2, '6', '2018-05-01', '1 lb Green Prime', '1 lb Roasted Medium', '176', '412', '10:06', '12', '10.4'),
(25, 2, '6', '2018-05-01', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '176', '410', '11:40', '12', '10.2'),
(24, 2, '6', '2018-04-30', '1 lb Green Extra Fancy', '1 lb Roasted Dark', '179', '440', '13:45', '12', '9.8'),
(23, 2, '6', '2018-04-30', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '172', '410', '11:34', '12', '10.2'),
(29, 2, '6', '2018-05-03', '1 lb Green Prime', '1 lb Roasted Dark', '165', '440', '14:52', '12', '9.8'),
(30, 2, '6', '2018-05-03', '1 lb Green Prime', '1 lb Roasted Medium', '139', '412', '16:48', '16', '13.4'),
(31, 2, '6', '2018-05-03', '1 lb Green Decaf', '1 lb Roasted Decaf', '158', '440', '13:55', '12', '9.8'),
(32, 2, '6', '2018-05-07', '1 lb Green Prime', '1 lb Roasted Medium', '140', '412', '16:16', '15', '12.6'),
(33, 2, '6', '2018-05-07', '1 lb Green Prime', '1 lb Roasted Medium', '144', '412', '14:52', '15', '12.6'),
(34, 2, '6', '2018-05-07', '1 lb Green Prime', '1 lb Roasted Medium', '142', '412', '14:45', '15', '12.8'),
(35, 2, '6', '2018-05-07', '1 lb Green Prime', '1 lb Roasted Dark', '145', '440', '17:12', '15', '12.7'),
(36, 2, '6', '2018-05-07', '1 lb Green Extra Fancy', '1 lb Dark XF', '170', '440', '14:20', '12', '10'),
(37, 2, '6', '2018-05-07', '1 lb Green Prime', '1 lb Roasted Dark', '146', '440', '17:41', '15', '12.2'),
(38, 2, '6', '2018-05-07', '1 lb Green Decaf', '1 lb Roasted Decaf', '178', '440', '12:56', '12.4', '10'),
(39, 2, '6', '2018-05-07', '1 lb Green Decaf', '1 lb Roasted Decaf', '168', '440', '12:42', '12.4', '10'),
(40, 2, '6', '2018-05-08', '1 lb Green Prime', '1 lb Roasted Dark', '177', '440', '13:35', '12', '10'),
(41, 2, '6', '2018-05-08', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '176', '410', '11:35', '12', '10.3'),
(42, 2, '6', '2018-05-08', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '176', '410', '11:16', '12', '10.2'),
(43, 2, '6', '2018-05-08', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '176', '410', '10:29', '12', '10.2'),
(44, 2, '6', '2018-05-08', '1 lb Green Extra Fancy', '1 lb Roasted Dark', '179', '440', '13:12', '12', '10'),
(46, 2, '6', '2018-05-08', '1 lb Green Extra Fancy', '1 lb Roasted Dark', '178', '440', '13:17', '12', '10'),
(47, 2, '6', '2018-05-09', '1 lb Green Extra Fancy', '1 lb Roasted Dark', '175', '440', '14:25', '12', '10'),
(48, 2, '6', '2018-05-14', '1 lb Green Prime', '1 lb Roasted Dark', '153', '440', '18:06', '15', '12.6'),
(49, 2, '6', '2018-05-14', '1 lb Green Prime', '1 lb Roasted Dark', '165', '440', '15:15', '15', '12.2'),
(50, 2, '6', '2018-05-14', '1 lb Green Prime', '1 lb Roasted Dark', '151', '440', '16:18', '15', '12.4'),
(51, 2, '6', '2018-05-14', '1 lb Green Prime', '1 lb Roasted Dark', '158', '440', '16:17', '15', '12.2'),
(52, 2, '6', '2018-05-14', '1 lb Green Prime', '1 lb Roasted Dark', '154', '440', '17:06', '15', '12.4'),
(53, 2, '6', '2018-05-14', '1 lb Green Prime', '1 lb Roasted Dark', '160', '440', '15:44', '15', '12.4'),
(54, 2, '6', '2018-05-14', '1 lb Green Prime', '1 lb Roasted Medium', '150', '412', '13:57', '15', '12.8'),
(55, 2, '6', '2018-05-14', '1 lb Green Prime', '1 lb Roasted Medium', '155', '412', '13:31', '15', '12.6'),
(56, 2, '6', '2018-05-14', '1 lb Green Prime', '1 lb Roasted Medium', '156', '412', '13:58', '15', '12.8'),
(57, 2, '6', '2018-05-14', '1 lb Green Prime', '1 lb Roasted Medium', '160', '412', '12:51', '15', '12.8'),
(58, 2, '6', '2018-05-15', '1 lb Green Prime', '1 lb Roasted Medium', '177', '412', '8:46', '10', '8.6'),
(59, 2, '6', '2018-05-15', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '174', '410', '11:33', '12', '10.2'),
(60, 1, '3', '2018-05-16', 'green coffee1', 'cherries1', '55', '5', 'dff', '255', '25'),
(61, 1, '3', '2018-05-16', 'green coffee1', 'cherries1', '25', '55', 'ff', '25', '5'),
(62, 2, '6', '2018-05-16', '1 lb Green Extra Fancy', '1 lb Dark XF', '165', '440', '15:32', '12', '10'),
(63, 1, '3', '2018-05-18', 'green coffee1', 'Cherries', '', '', '', '2', '3'),
(64, 1, '2', '2018-05-18', 'green coffee1', 'cascara1', '', '', '', '34', '34'),
(65, 2, '5', '2018-05-21', '1 lb Green Decaf', '1 lb Roasted Dark', '168', '440', '13.50', '12', '10'),
(66, 2, '5', '2018-05-21', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '180', '412', '11:23', '12', '10.2'),
(67, 2, '5', '2018-05-21', '1 lb Green Extra Fancy', '1 lb Roasted Dark', '173', '440', '13:23', '12', '10'),
(68, 2, '5', '2018-05-22', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '170', '412', '14:21', '12', '10.2'),
(69, 2, '5', '2018-05-24', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '170', '412', '13.58', '12', '10'),
(70, 2, '5', '2018-05-24', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '179', '412', '11:56', '12', '10'),
(71, 2, '5', '2018-05-24', '1 lb Green Decaf', '1 lb Roasted Dark', '149', '440', '17:56', '15', '12'),
(72, 2, '5', '2018-05-24', '1 lb Green Decaf', '1 lb Roasted Dark', '152', '440', '16:59', '15', '12'),
(73, 2, '5', '2018-05-25', '1 lb Green Prime', '1 lb Roasted Dark', '171', '440', '13:50', '12', '10'),
(74, 2, '5', '2018-05-25', '1 lb Green Prime', '1 lb Roasted Dark', '172', '440', '13:46', '12', '10'),
(75, 2, '5', '2018-05-25', '1 lb Green Prime', '1 lb Roasted Dark', '174', '440', '13:34', '12', '10'),
(76, 2, '5', '2018-05-25', '1 lb Green Prime', '1 lb Roasted Dark', '180', '440', '12.32', '12', '10'),
(77, 2, '5', '2018-05-25', '1 lb Green Prime', '1 lb Roasted Dark', '173', '440', '12.32', '12', '10'),
(78, 2, '5', '2018-05-29', '1 lb Green Prime', '1 lb Roasted Medium', '141', '440', '16:41', '15', '13'),
(79, 2, '5', '2018-05-29', '1 lb Green Prime', '1 lb Roasted Medium', '147', '440', '15:12', '15', '13'),
(80, 2, '5', '2018-05-29', '1 lb Green Prime', '1 lb Roasted Medium', '147', '440', '15:16', '15', '13'),
(81, 2, '5', '2018-05-29', '1 lb Green Prime', '1 lb Roasted Medium', '151', '440', '15:20', '15', '13'),
(82, 2, '5', '2018-05-29', '1 lb Green Peaberry', '1 lb Roasted Medium', '171', '408', '11:21', '9.8', '8.2'),
(83, 2, '5', '2018-05-29', '1 lb Green Extra Fancy', '1 lb Roasted Medium', '168', '412', '13:12', '12', '10'),
(84, 2, '5', '2018-05-29', '1 lb Green Extra Fancy', '1 lb Roasted Dark', '180', '440', '15:06', '12', '10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_taxamounts`
--

CREATE TABLE IF NOT EXISTS `tb_taxamounts` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `value` varchar(50) DEFAULT NULL,
  `cost` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tb_taxamounts`
--

INSERT INTO `tb_taxamounts` (`id`, `value`, `cost`) VALUES
(1, 'retail', '0.5'),
(2, 'wholescale', '0.04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_teamlead`
--

CREATE TABLE IF NOT EXISTS `tb_teamlead` (
  `teamlead_id` int(225) NOT NULL AUTO_INCREMENT,
  `teamlead_company` varchar(225) NOT NULL DEFAULT '0',
  `teamlead_name` varchar(225) DEFAULT NULL,
  `teamlead_email` varchar(225) DEFAULT NULL,
  `teamlead_password` varchar(225) DEFAULT NULL,
  `teamlead_phone` varchar(225) DEFAULT NULL,
  `teamlead_address` varchar(225) DEFAULT NULL,
  `deletestatus` int(128) NOT NULL DEFAULT '0' COMMENT '0: live 1: delete',
  PRIMARY KEY (`teamlead_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tb_teamlead`
--

INSERT INTO `tb_teamlead` (`teamlead_id`, `teamlead_company`, `teamlead_name`, `teamlead_email`, `teamlead_password`, `teamlead_phone`, `teamlead_address`, `deletestatus`) VALUES
(1, '1', 'teamlead', 'teamlead553@gmail.com', '123', '5343534', 'sdfsdsfds', 0),
(5, '1', 'testleader2', 'testleader2@gmail.com', '123', '5555555', 'erfggn', 0),
(6, '1', 'test leader', 'testleader@gmail.com', '123', '25656', 'yyfhf', 0),
(7, '1', 'ttttt', 'dssdfds', '123', '66666666', 'sdfas afdasdf', 1),
(8, '2', 'Primus', 'primuss194@gmail.com', 'pri00112', '8083547931', 'cc', 0),
(9, '2', 'Kat', 'kkkkk', 'kkk', '', '', 0),
(10, '1', 'test one leader', 'sdfssdfsdfsd333', '123', '733', 'sdfa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_unitprice`
--

CREATE TABLE IF NOT EXISTS `tb_unitprice` (
  `unit_id` int(225) NOT NULL AUTO_INCREMENT,
  `unit_company` int(225) NOT NULL DEFAULT '0',
  `unit_processing` varchar(225) NOT NULL DEFAULT '0',
  `unit_burlap` varchar(225) NOT NULL DEFAULT '0',
  `unit_plastic` varchar(225) NOT NULL DEFAULT '0',
  PRIMARY KEY (`unit_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tb_unitprice`
--

INSERT INTO `tb_unitprice` (`unit_id`, `unit_company`, `unit_processing`, `unit_burlap`, `unit_plastic`) VALUES
(1, 1, '0.35', '2.5', '2.0'),
(2, 2, '0.35', '2.5', '2.0');

-- --------------------------------------------------------

--
-- Table structure for table `tb_workers`
--

CREATE TABLE IF NOT EXISTS `tb_workers` (
  `farmer_id` int(128) NOT NULL AUTO_INCREMENT,
  `farmer_company` varchar(128) DEFAULT NULL,
  `farmer_teamleadid` varchar(128) DEFAULT NULL,
  `farmer_name` varchar(128) DEFAULT NULL,
  `farmer_email` varchar(128) DEFAULT NULL,
  `farmer_phone` varchar(128) DEFAULT NULL,
  `farmer_address` varchar(128) DEFAULT NULL,
  `deletestatus` int(128) NOT NULL DEFAULT '0' COMMENT '0: live 1: delete',
  PRIMARY KEY (`farmer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `tb_workers`
--

INSERT INTO `tb_workers` (`farmer_id`, `farmer_company`, `farmer_teamleadid`, `farmer_name`, `farmer_email`, `farmer_phone`, `farmer_address`, `deletestatus`) VALUES
(5, '1', '1', 'rrrr4444', 'ssssss122ss', '34324', 'sfsdadfa', 0),
(3, '1', '1', 'test24 farmer', 'test2us4er@gmail.com', '234', 'test address', 0),
(6, '1', '1', '44444', '4ewe', '23232423', 'sdfsd', 1),
(7, '1', '7', '66666666', '44444444444', '33333333', 'safsdfa', 0),
(8, '2', '8', 'Albert Andon', 'fffff', '44444', 'ddddd', 0),
(9, '1', '5', 'new owrk', 'newyourk@mgail.com', '22', 'sdf', 0),
(10, '1', '6', 'zhan worker', '', '', '', 0),
(11, '1', '6', 'you', 'ftyg', '55559', '', 0),
(12, '1', '6', 'tyujh', 'hsfh', '756456', 'aegae', 0),
(13, '1', '5', 'wd', 'sdf', '', 'sdf', 0),
(14, '1', '1', 'y', 'rrf@h', '555', 'rfg', 0),
(15, '1', '5', 'ttt', 'sdfsd', '', 'sdfsd', 0),
(16, '2', '8', 'Thomas Damarlin', '', '', '', 0),
(17, '1', '10', '656', 'sdf', '733', 'sdf', 1),
(18, '1', '1', 'yyyy', 'hh', '66', 'ggd', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
