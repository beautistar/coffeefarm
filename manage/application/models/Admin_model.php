<?php

class Admin_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

/*
* Joe Mow Admin Model - 77.104.168.141
*/ 
// to display all jobs on the first page
    
    
   
    
    function getMowerData($user_id){
        $this->db->select('user_id, user_firstname, user_lastname, user_email, user_available')
                 ->from('tb_user')
                 ->where('user_id', $user_id);
                 
        return $this->db->get()->result();
    }
    
    
    
    
    
      
    
    
    function getAllProviderInfo($adminID) {
        $this->db->select('*')
                 ->from('tb_provider')
                 ->where('adminID', $adminID)
                 ->order_by('proid', 'asc');
                 
        return $this->db->get()->result();
    } // end of getAllProviderInfo
    
    function getAllCustomerInfo($adminID) {
        $this->db->select('*')
                 ->from('tb_customer')
                 ->where('adminID', $adminID)
                 ->order_by('cusid', 'asc');
                 
        return $this->db->get()->result();
    }
    
    function getAllProductInfo() {
        $this->db->select('*')
                 ->from('tb_product')
                 ->order_by('itemid', 'asc');
                 
        return $this->db->get()->result(); 
    }
    
    function getAllServiceInfo() {
        $this->db->select('*')
                 ->from('tb_service')
                 ->order_by('serviceid', 'asc');
                 
        return $this->db->get()->result();
    }
    
    function getAllBroadmoorInfo($adminID) {
        $this->db->select('*')
                 ->from('tb_bm_product')
                 ->where('adminID', $adminID)
                 ->order_by('bm_proid', 'asc');
                 
        return $this->db->get()->result();
    }
    
    
    
    ///////////////////////////////////////////////////////
    // Provider ///////////////////////////////////////////
    ///////////////////////////////////////////////////////
    function getProviderInfo($proid){
        
        $result = $this->db->get_where('tb_provider',array('proid'=>$proid))->row();
        return $result;         
    }
    
    function getProviderEmailExist($proEmail) {
        $this->db->where('proEmail', $proEmail);
        return $this->db->get('tb_provider')->num_rows();
    }
    
    function updateProvider($data, $provider_url){
       
       $this->db->set('proProfileImageUrl', $provider_url); // provider profile image url
       $this->db->set('proFirstName', $data['proFirstName']);
       $this->db->set('proLastName', $data['proLastName']);
       $this->db->set('proEmail', $data['proEmail']);
       $this->db->set('proPassword', $data['proPassword']);
       $this->db->set('proCity', $data['proCity']);
       $this->db->set('proAddress', $data['proAddress']);
       $this->db->set('proCompany', $data['proCompany']);          
       
       $this->db->where('proid',$data['proid']);
       $this->db->update('tb_provider'); 
    }
      
    function updateSetup($data){

       $this->db->set('proServicePercent',     $data['proServicePercent']);
       $this->db->set('proSalary',             $data['proSalary']);
       $this->db->set('proProductSalePercent', $data['proProductSalePercent']);          
       
       $this->db->where('proid',$data['proid']);
       $this->db->update('tb_provider'); 
    }  
    // 
    function deleteProvider($proid){
        
        $this->db->where('proid', $proid);
        $this->db->delete('tb_provider');
    }
    
    // add new provider and get the proid from it
    // add the proid into available table
    /*function addProvider($data){
        
        $this->db->set('adminID',               $data['adminID']);            
        $this->db->set('proProfileImageUrl',    $data['proProfileImageUrl']);
        $this->db->set('proFirstName',          $data['proFirstName']);
        $this->db->set('proLastName',           $data['proLastName']);
        $this->db->set('proEmail',              $data['proEmail']);
        $this->db->set('proPassword',           $data['proPassword']);
        $this->db->set('proCity',               $data['proCity']);
        $this->db->set('proAddress',            $data['proAddress']);
        $this->db->set('proCompany',            $data['proCompany']);
        
        $this->db->insert('tb_provider');         
        $proid = $this->db->insert_id();
        
        // insert $proid into tb_provider_available table
        $this->db->set('proid', $proid);
        $this->db->insert('tb_provider_available');
    }*/
    
    ///////////////////////////////////////////////////////
    // Customer ///////////////////////////////////////////
    ///////////////////////////////////////////////////////
    function getCustomerInfo($cusid){
        
        $result = $this->db->get_where('tb_customer',array('cusid'=>$cusid))->row();
        return $result;         
    }
    
    function updateCustomer($data, $customer_url){
       
       $this->db->set('cusProfileImageUrl', $customer_url); // provider profile image url
       $this->db->set('cusFirstName',       $data['cusFirstName']);
       $this->db->set('cusLastName',        $data['cusLastName']);
       $this->db->set('cusEmail',           $data['cusEmail']);
       $this->db->set('cusPassword',        $data['cusPassword']);
       $this->db->set('cusLocation',        $data['cusLocation']);
       $this->db->set('cusMonthlyService',  $data['cusMonthlyService']);
       $this->db->set('cusSalaryAmount',    $data['cusSalaryAmount']);
       $this->db->set('cusExtra',           $data['cusExtra']);
       
       $this->db->where('cusid',$data['cusid']);
       $this->db->update('tb_customer'); 
    }
     
    function deleteCustomer($cusid){
        
        $this->db->where('cusid', $cusid);
        $this->db->delete('tb_customer');
    }
    
    function addCustomer($data){
        
        $this->db->set('adminID',            $data['adminID']);            
        $this->db->set('cusProfileImageUrl', $data['cusProfileImageUrl']);
        $this->db->set('cusFirstName',       $data['cusFirstName']);
        $this->db->set('cusLastName',        $data['cusLastName']);
        $this->db->set('cusEmail',           $data['cusEmail']);
        $this->db->set('cusPassword',        $data['cusPassword']);
        $this->db->set('cusLocation',        $data['cusLocation']);
        $this->db->set('cusMonthlyService',  $data['cusMonthlyService']);
        $this->db->set('cusSalaryAmount',    $data['cusSalaryAmount']);
        $this->db->set('cusExtra',           $data['cusExtra']);
        
        $this->db->insert('tb_customer');         
    }
    
    
    ///////////////////////////////////////////////////////
    // Product ///////////////////////////////////////////
    ///////////////////////////////////////////////////////
    function getProductInfo($itemid){
        
        $result = $this->db->get_where('tb_product',array('itemid'=>$itemid))->row();
        return $result;         
    }
    
    function getProductInfoByProid($proid){
        $result = $this->db->get_where('tb_product',array('proid'=>$proid))->result(); // get all product data by proid
        return $result;
    }
    
    function updateProduct($data, $item_url){
       
       $this->db->set('itemBrand',          $data['itemBrand']);
       $this->db->set('itemProduct',        $data['itemProduct']);
       $this->db->set('itemName',           $data['itemName']);
       $this->db->set('itemSize',           $data['itemSize']);
       $this->db->set('itemPrice',          $data['itemPrice']);
       $this->db->set('itemDescription',    $data['itemDescription']);
       $this->db->set('itemPictureUrl',     $item_url);
       $this->db->set('itemSaleStatus',     $data['itemSaleStatus']);
       
       $this->db->where('itemid',$data['itemid']);
       $this->db->update('tb_product'); 
    }
    
    function deleteProduct($itemid){
        
        $this->db->where('itemid', $itemid);
        $this->db->delete('tb_product');
    }
    
    function addProductConfirm($data, $item_url){
        
        $this->db->set('proid',            $data['proid']);            
        $this->db->set('itemBrand',        $data['itemBrand']);
        $this->db->set('itemProduct',      $data['itemProduct']);
        $this->db->set('itemName',         $data['itemName']);
        $this->db->set('itemSize',         $data['itemSize']);
        $this->db->set('itemPrice',        $data['itemPrice']);
        $this->db->set('itemDescription',  $data['itemDescription']);
        $this->db->set('itemPictureUrl',   $item_url);
        $this->db->set('itemInventoryNum', $data['itemInventoryNum']);
        $this->db->set('itemSaleStatus',   $data['itemSaleStatus']);
        
        $this->db->insert('tb_product');         
    }
    
    ///////////////////////////////////////////////////////
    // Service ////////////////////////////////////////////
    ///////////////////////////////////////////////////////
    function getServiceInfoByProid($proid) {
        $result = $this->db->get_where('tb_service',array('proid'=>$proid))->result();
        return $result;
    }
    
    function getServiceInfoByServiceID($serviceid) {
        $result = $this->db->get_where('tb_service',array('serviceid'=>$serviceid))->row();
        return $result;
    }
    
    function getProviderInfoByServiceID($serviceid) { // get proid using serviceid from tb_service and then get provider info using proid from tb_provider
        $proid = $this->db->select('proid')
                          ->from('tb_service')
                          ->where('serviceid', $serviceid)
                          ->get()
                          ->row();
        
        return $proid;
    }
    
    function getServiceInfoOfProvider($serviceid){
        $proid = $this->db->select('proid')
                          ->from('tb_service')
                          ->where('serviceid', $serviceid)
                          ->get()
                          ->row_array();
        
        $result = $this->db->get_where('tb_service',$proid)->result();
        return $result;
    }
	
	function validate_password($id,$password){
		 $this->db->select('COUNT(admin_id) as total');
		 $this->db->from('tb_admin'); 
		 $this->db->where('admin_id',$id);
		 $this->db->where('admin_password',$password); 
		 $result=$this->db->get();
		 $result=$result->row();
		 return $result;
	}
    
	public function remove_employee_by_store_id($employee_id,$store_id){
       $this->db->where('store_id',$store_id);
	   $this->db->where('employee_id',$employee_id);
	   $this->db->delete('tb_store_employees');
	   return true; 
	}	
	
	public function add_store_employee($store_id,$employee_name){
		$name=$this->split_name($employee_name);	
		$this->db->set('store_id',$store_id);            
        $this->db->set('employee_first_name',$name[0]);
		$this->db->set('employee_last_name',$name[1]);
		$insert_id=$this->db->insert('tb_store_employees'); 
		return $insert_id; 
	}
	
	public function split_name($name) {
	    $name = trim($name);
	    $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
	    $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
	    return array($first_name, $last_name);
	}
	
	public function assign_employee($employee_id,$order_id,$store_id){
		 $this->db->set('employee_id',$employee_id);
		 $this->db->where('order_id',$order_id);
		 $this->db->where('store_id',$store_id);
		 $result=$this->db->update('tb_orderhistory');
		 return $result;
	}
	
	public function change_status_by_order_id($order_id){
		$this->db->set('status',3);
		$this->db->set('fillfulledtime',date("Y-m-d H:i:s"));
		$this->db->where('order_id',$order_id);
		$result=$this->db->update('tb_orderhistory');
		return $result;
	}
	
    function updateService($data, $service_url){
       
       $this->db->set('proBeautyCategory',     $data['proBeautyCategory']);
       $this->db->set('proBeautySubcategory',  $data['proBeautySubcategory']);
       $this->db->set('proServicePrice',       $data['proServicePrice']);
       $this->db->set('proServicePictureUrl',  $service_url); // 
       $this->db->set('proServiceDescription', $data['proServiceDescription']);
       
       $this->db->where('serviceid',$data['serviceid']);
       $this->db->update('tb_service'); 
    }
    
    function deleteService($serviceid){
        
        $this->db->where('serviceid', $serviceid);
        $this->db->delete('tb_service');
    }
    
    function addServiceConfirm($data, $service_url){
        
        $this->db->set('proid',     $data['proid']);
        $this->db->set('proBeautyCategory',     $data['proBeautyCategory']);
        $this->db->set('proBeautySubcategory',  $data['proBeautySubcategory']);
        $this->db->set('proServicePrice',       $data['proServicePrice']);
        $this->db->set('proServicePictureUrl',  $service_url); // 
        $this->db->set('proServiceDescription', $data['proServiceDescription']);
       
        $this->db->insert('tb_service');
    }
    
    ///////////////////////////////////////////////////////
    // Braodmoor Product //////////////////////////////////
    ///////////////////////////////////////////////////////
    function BM_addProductConfirm($data){
        
        $this->db->set('adminID',            $data['adminID']);            
        $this->db->set('bm_proImageUrl',     $data['bm_proImageUrl']);            
        $this->db->set('bm_proName',         $data['bm_proName']);
        $this->db->set('bm_proInventoryNum', $data['bm_proInventoryNum']);
        $this->db->set('bm_proCategory',     $data['bm_proCategory']);
        $this->db->set('bm_proAdditional',   $data['bm_proAdditional']);
        
        $this->db->insert('tb_bm_product');
    }
    
    function BM_deleteProduct($bm_proid){
        
        $this->db->where('bm_proid', $bm_proid);
        $this->db->delete('tb_bm_product');
        $this->db->delete('tb_bm_product_detail');
    }
    
    function BM_getProductInfo($bm_proid){
        
        $result = $this->db->get_where('tb_bm_product',array('bm_proid'=>$bm_proid))->row();
        return $result;         
    }
    
    function BM_updateProduct($data, $item_url){
                    
        $this->db->set('bm_proImageUrl',     $item_url);            
        $this->db->set('bm_proName',         $data['bm_proName']);
        $this->db->set('bm_proInventoryNum', $data['bm_proInventoryNum']);
        $this->db->set('bm_proCategory',     $data['bm_proCategory']);
        $this->db->set('bm_proAdditional',   $data['bm_proAdditional']);
        
        $this->db->where('bm_proid',$data['bm_proid']);
        $this->db->update('tb_bm_product');
    }
    
    function BM_getDetailData($bm_proid) {
        $result = $this->db->get_where('tb_bm_product_detail',array('bm_proid'=>$bm_proid))->result();
        return $result;
    }
    
    function BM_deleteDetail($bm_detailID){
        
        $this->db->where('bm_detailID', $bm_detailID);
        $this->db->delete('tb_bm_product_detail');
    }
    
    function BM_updateProductDetail($data){
        
        $this->db->set('bm_proid',       $data['bm_proid']);
        $this->db->set('bm_proSize',     $data['bm_proSize']);
        $this->db->set('bm_proQuantity', $data['bm_proQuantity']);
        $this->db->set('bm_proPrice',    $data['bm_proPrice']);
        
        $this->db->insert('tb_bm_product_detail');
    }
    
    function BM_getOneDetailByID($bm_detailID) {
        $result = $this->db->get_where('tb_bm_product_detail',array('bm_detailID'=>$bm_detailID))->row();
        return $result;
    }
    
    function BM_editProductDetailConfirm($data){
        
        $this->db->set('bm_proSize',     $data['bm_proSize']);
        $this->db->set('bm_proQuantity', $data['bm_proQuantity']);
        $this->db->set('bm_proPrice',    $data['bm_proPrice']);
        
        $this->db->where('bm_detailID', $data['bm_detailID']);
        $this->db->update('tb_bm_product_detail');
    }
    
    /////////////////////////////////////////////////////////////////////
    // Employer
    /////////////////////////////////////////////////////////////////////
    function getEmployerInfo($adminID) {
        $this->db->select('*')
                 ->from('tb_admin')
                 ->where('adminID', $adminID);
                 
        return $this->db->get()->row();
    }
    
    function getAllEmployeeInfoOfAdmin($adminID){
        $this->db->select('*')
                 ->from('tb_em_employee')
                 ->where('adminID', $adminID)
                 ->order_by('em_id', 'asc');
                 
        return $this->db->get()->result();
    }
    
    function EM_getEmployeeInfo($em_id){
        
        $result = $this->db->get_where('tb_em_employee',array('em_id'=>$em_id))->row();
        return $result;         
    }
    
    function EM_updateEmployee($data, $photo_url){
                    
        $this->db->set('em_image',    $photo_url);            
        $this->db->set('em_name',     $data['em_name']);
        $this->db->set('em_gender',   $data['em_gender']);
        $this->db->set('em_email',    $data['em_email']);
        $this->db->set('em_password', $data['em_password']);
        $this->db->set('em_millennial', $data['em_millennial']);
        $this->db->set('em_givenbuck',  $data['em_givenbuck']);
        
        $this->db->where('em_id',$data['em_id']);
        $this->db->update('tb_em_employee');
    }
    
    // add new employee
    function EM_addEmployeeConfirm($data){
        
        $this->db->set('adminID',       $data['adminID']);            
        $this->db->set('em_image',      $data['em_image']);            
        $this->db->set('em_name',       $data['em_name']);
        $this->db->set('em_gender',     $data['em_gender']);
        $this->db->set('em_email',      $data['em_email']);
        $this->db->set('em_password',   $data['em_password']);
        $this->db->set('em_millennial', $data['em_millennial']);
        $this->db->set('em_givenbuck',  $data['em_givenbuck']);
        
        $this->db->insert('tb_em_employee');
    }
    
    // delete an employee
    function EM_deleteEmployee($em_id){
        
        $this->db->where('em_id', $em_id);
        $this->db->delete('tb_em_employee');
    }
    
    /////////////////////////////////////////////////////////////////////
    // JOB
    /////////////////////////////////////////////////////////////////////
    function getAllJobInfoOfAdmin($adminID){
        $this->db->select('*')
                 ->from('tb_em_job')
                 ->where('adminID', $adminID)
                 ->order_by('job_id', 'asc');
                 
        return $this->db->get()->result();
    }
    
    function EM_getJobInfo($job_id){
        
        $result = $this->db->get_where('tb_em_job',array('job_id'=>$job_id))->row();
        return $result;         
    }
    
    // update job data
    function EM_updateJob($data){
        
        $this->db->set('job_name',        $data['job_name']);
        $this->db->set('job_req',         $data['job_req']);
        $this->db->set('job_department',  $data['job_department']);
        $this->db->set('job_location',    $data['job_location']);
        $this->db->set('job_description', $data['job_description']);
        $this->db->set('job_empty',       $data['job_empty']);
        
        $this->db->where('job_id',$data['job_id']);
        $this->db->update('tb_em_job');
    }
    
    // add new job
    function EM_addJob($adminID, $data, $postdate){
        
        $this->db->set('adminID',         $adminID);
        $this->db->set('job_name',        $data['job_name']);
        $this->db->set('job_req',         $data['job_req']);
        $this->db->set('job_department',  $data['job_department']);
        $this->db->set('job_location',    $data['job_location']);
        $this->db->set('job_description', $data['job_description']);
        $this->db->set('job_postdate',    $postdate);
        $this->db->set('job_empty',       $data['job_empty']);
        
        $this->db->insert('tb_em_job');
    }
    
    // delete job
    function EM_deleteJob($job_id){
        
        $this->db->where('job_id', $job_id);
        $this->db->delete('tb_em_job');
    }
    
    /////////////////////////////////////////////////////////////////////
    // Announcement
    /////////////////////////////////////////////////////////////////////
    function getAllAnnounceInfoOfAdmin($adminID){
        $this->db->select('*')
                 ->from('tb_em_announce')
                 ->where('adminID', $adminID)
                 ->order_by('an_id', 'asc');
                 
        return $this->db->get()->result();
    }
    
    function EM_getAnnounceInfo($an_id){
        
        $result = $this->db->get_where('tb_em_announce',array('an_id'=>$an_id))->row();
        return $result;         
    }
    
    // update job data
    function EM_updateAnnounce($data, $photo_url){
        
        $this->db->set('an_title',        $data['an_title']);
        $this->db->set('an_audience',     $data['an_audience']);
        $this->db->set('an_subject',      $data['an_subject']);
        $this->db->set('an_description',  $data['an_description']);
        $this->db->set('an_callofaction', $data['an_callofaction']);
        $this->db->set('an_owneremail',   $data['an_owneremail']);
        $this->db->set('an_image',        $photo_url);
        
        $this->db->where('an_id',$data['an_id']);
        $this->db->update('tb_em_announce');
    }
    
    // add new job
    function EM_addAnnounce($data){
        
        $this->db->set('adminID',         $data['adminID']);
        $this->db->set('an_title',        $data['an_title']);
        $this->db->set('an_audience',     $data['an_audience']);
        $this->db->set('an_subject',      $data['an_subject']);
        $this->db->set('an_description',  $data['an_description']);
        $this->db->set('an_callofaction', $data['an_callofaction']);
        $this->db->set('an_owneremail',   $data['an_owneremail']);
        $this->db->set('an_image',        $data['an_image']);
        $this->db->set('an_postdate',     $data['an_postdate']);
        
        $this->db->insert('tb_em_announce');
    }
    
    // delete job
    function EM_deleteAnnounce($an_id){
        
        $this->db->where('an_id', $an_id);
        $this->db->delete('tb_em_announce');
    }
    
    
    
    ///==================== New Score==================
    ////////////////////////////////////////////////
    ////////////////////////////////////////////////
   function  addProvider($data){       
        $this->db->set('admin_email',        $data['admin_email']);
        $this->db->set('admin_phonenumber',  $data['admin_phonenumber']);
        $this->db->set('admin_storename',    $data['admin_storename']);
        $this->db->set('admin_managername',  $data['admin_managername']);
        $this->db->set('admin_photourl',     $data['admin_photourl']);
        $this->db->set('admin_address',      $data['admin_address']);
        $this->db->set('admin_password',     $data['admin_password']);
        $this->db->set('admin_latitude',           $data['latitude']);
        $this->db->set('admin_longitude',          $data['longitude']);
        
               
        $this->db->insert('tb_admin');
       
   }
   
    function getStoremanagerDataByType($superadmin_id){ //1-client, 2-mower
        $this->db->select('*')
                 ->from('tb_admin')
                 ->where('admin_id !=', $superadmin_id)
                 ->order_by('admin_id', 'asc');
                 
        return $this->db->get()->result();
    }
    
    function getDriversData(){
       $this->db->select('*')
                 ->from('tb_user')   
                 ->where('user_deletestatus', 2)             
                 ->order_by('user_id', 'asc');
                 
        return $this->db->get()->result();        
    }
     function getDriversData1(){
       $this->db->select('*')
                 ->from('tb_user')   
                 ->where('user_deletestatus', 0)             
                 ->order_by('user_id', 'asc');
                 
        return $this->db->get()->result();        
    }
    
    function getAdmininfo($adminid){
         $this->db->select('*')
                 ->from('tb_admin')     
                 ->where('admin_id', $adminid)           
                 ->order_by('admin_id', 'asc');
                 
        return $this->db->get()->result(); 
    }
    
    function getOrderinfo($orderid){
         $this->db->select('*')
                 ->from('tb_orderhistory')     
                 ->where('order_id', $orderid);  
        return $this->db->get()->result(); 
    }
    
    function getWorkerinfo($workerid){
        if($workerid!=0){
             $this->db->select('*')
                 ->from('tb_user')     
                 ->where('user_id', $workerid);  
             return $this->db->get()->result(); 
            
        }else{
            return array();
        }
        
    }
    
    function getStoreinfo($storeid){
         $this->db->select('*')
                 ->from('tb_admin')     
                 ->where('admin_id', $storeid);  
         return $this->db->get()->result();
        
    }
    
    
    function  updateAdmin($data){  
        $this->db->where('admin_id', $data['admin_id']);     
        $this->db->set('admin_email',        $data['admin_email']);
        $this->db->set('admin_phonenumber',  $data['admin_phonenumber']);
        $this->db->set('admin_storename',    $data['admin_storename']);
        $this->db->set('admin_managername',  $data['admin_managername']);
        $this->db->set('admin_photourl',     $data['admin_photourl']);
        $this->db->set('admin_address',      $data['admin_address']);
        $this->db->set('admin_password',     $data['admin_password']);
        $this->db->set('admin_latitude',           $data['latitude']);
        $this->db->set('admin_longitude',          $data['longitude']);
               
        $this->db->update('tb_admin');
       
   }
    
     function  updateAdmin1($data){  
        $this->db->where('admin_id', $data['admin_id']);     
        $this->db->set('admin_email',        $data['admin_email']);
        $this->db->set('admin_phonenumber',  $data['admin_phonenumber']);
        $this->db->set('admin_storename',    $data['admin_storename']);
        $this->db->set('admin_managername',  $data['admin_managername']);       
        $this->db->set('admin_address',      $data['admin_address']);
        $this->db->set('admin_password',     $data['admin_password']);
        $this->db->set('admin_latitude',           $data['latitude']);
        $this->db->set('admin_longitude',          $data['longitude']);
               
        $this->db->update('tb_admin');
       
   }
   
   function  createneworder($data){
       $storeid=$data['store_id'];
       $this->db->where('admin_id', $storeid);
       $storename=$this->db->get('tb_admin')->result()[0]->admin_storename;
       
       
        $this->db->set('store_id',          $data['store_id']);
        $this->db->set('buyer_name',        $data['buyername']);
        $this->db->set('buyer_address',     $data['address']);
        $this->db->set('buyer_phonenumber', $data['buyerphonenumber']);
        $this->db->set('buyer_email',       $data['bueremail']);  
        $this->db->set('product_id',    $data['product_id']);     
        $this->db->set('buyer_products',    $data['buyerproducts']);
        $this->db->set('buyer_cost',        $data['totalcost']);
        $this->db->set('start_time',        $data['createdtime']);
        $this->db->set('store_name',        $storename);
        $this->db->set('short_description', $data['short_description']);
        $this->db->set('latitude',        $data['latitude']);
        $this->db->set('longitude', $data['longitude']);
        $this->db->set('status', $data['status']);    
        $this->db->insert('tb_orderhistory');
        
        return $storename;
   }
   
  
  	
	function getEmployeesByStoreId($store_id){
		$query=$this->db->query("Select *
									From tb_store_employees
									Where store_id='$store_id'");
		
		$result=$query->result();
		return $result;						
	}  
    
    function getEmployeesNamesByIds($employee_id){
    	$query=$this->db->query("Select user_id, user_firstname, user_lastname
									From tb_user
									Where user_id='$employee_id'");
		
		$result=$query->result();
		return $result;	
    }
	
	function get_employee_name_by_employee_id($employee_id){
		$query=$this->db->query("Select employee_first_name, employee_last_name
									From tb_store_employees
									Where employee_id='$employee_id'");
		
		$result=$query->result();
		return $result;	
	}
    
    public function getAllOpenOrders($storeid,$status){
            if($status=='open'){
                $orderstatus=0;
            }elseif($status == 'pick-up'){
                $orderstatus =1;
            }elseif($status == 'close'){
                $orderstatus =2;
            }elseif($status=='accept'){
                $orderstatus =3;
            }elseif($status=='orderready'){
                $orderstatus =4;
            }
            if($storeid=='1'){
            $this->db->select('order_id, COUNT(order_id) as total');
             $this->db->where('status',$orderstatus);
            
             $this->db->group_by('status'); 
             $this->db->from('tb_orderhistory'); 
             $result=$this->db->get();
             $result=$result->row();
             return $result;
            }else{
                  $this->db->select('order_id, COUNT(order_id) as total');
             $this->db->where('status',$orderstatus);
             $this->db->where('store_id', $storeid);
             $this->db->group_by('status'); 
             $this->db->from('tb_orderhistory'); 
             $result=$this->db->get();
             $result=$result->row();
             return $result;
            }
           
        }
    
    
       
    
    function getAllJobsDataofstore($storeid,$status,$date,$orderid,$deliveryid){
       
        $jobinforarray = array();
        $result = array();
         //$orderstatus=null;
         //$this->session->set_userdata('status', $status);
        if($status=='all'){
            
                
                $this->db->select('*');
                $this->db->from('tb_orderhistory');
                if($date!=''){
                    $old_date_timestamp = strtotime($date);
                    $new_date = date('Y-m-d', $old_date_timestamp);
                   $this->db->like('tb_orderhistory.start_time', $new_date); 
                  }
                if($orderid!=''){
                     $this->db->where('tb_orderhistory.order_id', $orderid);
                }
				if($deliveryid!=''){
                     $this->db->where('tb_orderhistory.delivery_id', $deliveryid);
                }           
                $this->db->where('tb_orderhistory.store_id', $storeid); 
                $this->db->order_by("tb_orderhistory.order_id", "desc");
            // else{
                // $this->db->select('*');
                 // $this->db->from('tb_orderhistory');   
                 // $this->db->where('tb_orderhistory.store_id', $storeid);
                 // $this->db->order_by("tb_orderhistory.order_id", "desc");
            // }
        }
        else{
            //$orderstatus=null;
            //$status=$this->session->has_userdata('status');
            if($status=='open'){
                $orderstatus=0;
            }elseif($status == 'pick-up'){
                $orderstatus=1;
            }elseif($status == 'close'){
                $orderstatus=2;
            }elseif($status=='accept'){
                $orderstatus=3;
            }elseif($status=='orderready'){
                $orderstatus=4;
            }
           
            // if($date!=''){
                
                $this->db->select('*');
                 $this->db->from('tb_orderhistory');
                 if($date!=''){
                     $old_date_timestamp = strtotime($date);
                    $new_date = date('Y-m-d', $old_date_timestamp);
                     $this->db->like('tb_orderhistory.start_time', $new_date);
                 }
                if($orderid!=''){
                    $this->db->where('tb_orderhistory.order_id', $orderid);
                }
				if($deliveryid!=''){
                     $this->db->where('tb_orderhistory.delivery_id', $deliveryid);
                }      
                 $this->db->where('tb_orderhistory.store_id', $storeid);
                 $this->db->where('tb_orderhistory.status', $orderstatus);
                 $this->db->order_by("tb_orderhistory.order_id", "desc");
                 
            // }else{
                // $this->db->select('*');
                // $this->db->from('tb_orderhistory'); 
                // $this->db ->order_by("tb_orderhistory.order_id", "desc") ;        
                // $this->db ->where('tb_orderhistory.store_id', $storeid);
                 // $this->db->where('tb_orderhistory.status', $orderstatus) ;  
                  // $this->db->order_by("tb_orderhistory.order_id", "desc");
            // }
            
        }
        
        $w_query = $this->db->get();                
        if ($w_query->num_rows() > 0){
            $j = 1;
            foreach ($w_query->result() as $tmpInfo) {
              
               
                /*if($alarm_picture != ''){
                    $picture = '<img class="img-thumbnail" align="middle" onclick="image(this)" src="'.$alarm_picture.'" />';
                } else {
                    $picture = '';
                }
                */
                // to set alarm status
                $workstatus = $tmpInfo->status;
                if($workstatus == 0){
                	if($workstatus == 0 && $tmpInfo->employee_id!=''){
                		$status = '<font style="color: #FFFF00";>Open</font>';
						$no='<font style="padding: 6px 6px 6px 6px; background-color: #FFFF00; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	}else{
                		$status = '<font style="color: #00a65a";>Open</font>';
						$no='<font style="padding: 6px 6px 6px 6px; background-color: #00a65a; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	}           
                } elseif ($workstatus == 1){
                    $status = '<font style="color: #ca311e";>Picked up</font>';
					$no='<font style="padding: 6px 6px 6px 6px; background-color: #ca311e; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	
                } elseif ($workstatus == 2){ 
                    $status = '<font style="color: #da8c10";>Closed</font>';
					$no='<font style="padding: 6px 6px 6px 6px; background-color: #da8c10; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	
                }elseif ($workstatus == 3){ 
                    $status = '<font style="color: #00acd7";>Fulfilled</font>';
					$no='<font style="padding: 6px 6px 6px 6px; background-color: #00acd7; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	
                }elseif ($workstatus == 4){ 
                    $status = '<font style="color: #720377";>Order Ready</font>';
					$no='<font style="padding: 6px 6px 6px 6px; background-color: #720377; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	
                };
                
                
                $tb_item_detail ="detail";       
                $detail = '<a href="'.base_url().'index.php/admin/viewjobdetail/'.$tmpInfo->order_id.'">'.$tb_item_detail.'&nbsp;<i class="fa fa-commenting" aria-hidden="true"></i></a>';
                $delivery_detail = '<a href="'.base_url().'index.php/admin/viewdeliverydetail/'.$tmpInfo->delivery_id.'">Delivery Detail&nbsp;<i class="fa fa-commenting" aria-hidden="true"></i></a>';
               
                $admin_id=$this->session->userdata('login_adminID');
                if($admin_id==1){
                     $tmp = array(
                                'No'           => $no, 
                                'order_id'     => $tmpInfo->order_id,                              
                                'buyer_name'    => $tmpInfo->buyer_name,
                                'driver_name'     => $tmpInfo->worker_name,
                                'buyer_address'  => $tmpInfo->buyer_address,
                                'productname'          => $tmpInfo->buyer_products,
                                'cost' => $tmpInfo->buyer_cost,
                                'status'     => $status,
                                'detail'       => $detail,
                                'shop_name' => $tmpInfo->store_name
                    );
                    
                }else{
                     $tmp = array(
                                'No'           => $no,
                                'order_id'     => $tmpInfo->order_id,                                
                                'buyer_name'    => $tmpInfo->buyer_name,
                                'driver_name'     => $tmpInfo->worker_name,
                                'buyer_address'  => $tmpInfo->buyer_address,
                                'productname'          => $tmpInfo->buyer_products,
                                'cost' => $tmpInfo->buyer_cost,
                                'status'     => $status,
                                'detail'       => $detail,
                                'delivery_detail' => $delivery_detail,
                                'delivery_id'=> $tmpInfo->delivery_id
                    );
                }
                
                array_push($jobinforarray, $tmp);
                $j++;
            }              
            $result = $jobinforarray;                 
        } else {
            $result = array();
        }
        
        return $result;
       
    }
    
    
    function getAllJobsDataofstoreadmin($status,$date,$orderid){
       
        $jobinforarray = array();
        $result = array();
         //$orderstatus=null;
         //$this->session->set_userdata('status', $status);
        if($status=='all'){
                
               
                $this->db->select('*');
                 $this->db->from('tb_orderhistory') ;
                 if($date!=''){
                     $old_date_timestamp = strtotime($date);
                    $new_date = date('Y-m-d', $old_date_timestamp);
                     $this->db->like('tb_orderhistory.start_time', $new_date );
                 }
                 if($orderid!=''){
                     $this->db->where('tb_orderhistory.order_id', $orderid);
                 }
                 $this->db->order_by("tb_orderhistory.order_id", "desc");    
               
            
        }
        else{
            //$orderstatus=null;
            //$status=$this->session->has_userdata('status');
            if($status=='open'){
                $orderstatus=0;
            }elseif($status == 'pick-up'){
                $orderstatus=1;
            }elseif($status == 'close'){
                $orderstatus=2;
            }elseif($status=='accept'){
                $orderstatus=3;
            }elseif($status=='orderready'){
                $orderstatus=4;
            }
			
                
                $this->db->select('*');
                 $this->db->from('tb_orderhistory');
                 if($date!=''){
                     $old_date_timestamp = strtotime($date);
                    $new_date = date('Y-m-d', $old_date_timestamp);
                     $this->db->like('tb_orderhistory.start_time', $new_date);
                 }
                 if($orderid!=''){
                     $this->db->where('tb_orderhistory.order_id', $orderid);
                 }              
                 $this->db->where('tb_orderhistory.status', $orderstatus);
                  $this->db->order_by("tb_orderhistory.order_id", "desc");
                 
           
            
        }
        
        $w_query = $this->db->get();                
        if ($w_query->num_rows() > 0){
            $j = 1;
			//$no='';
            foreach ($w_query->result() as $tmpInfo) {
              
                
                /*if($alarm_picture != ''){
                    $picture = '<img class="img-thumbnail" align="middle" onclick="image(this)" src="'.$alarm_picture.'" />';
                } else {
                    $picture = '';
                }
                */
                // to set alarm status
                $workstatus = $tmpInfo->status;
				//$no='';
                if($workstatus == 0){
                    if($workstatus == 0 && $tmpInfo->employee_id!=''){
                		$status = '<font style="color: #FFFF00";>Open</font>';
						$no='<font style="padding: 6px 6px 6px 6px; background-color: #FFFF00; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	}else{
                		$status = '<font style="color: #00a65a";>Open</font>';
						$no='<font style="padding: 6px 6px 6px 6px; background-color: #00a65a; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	}
                } elseif ($workstatus == 1){
                    $status = '<font style="color: #ca311e";>Picked up</font>';
					$no='<font style="padding: 6px 6px 6px 6px; background-color: #ca311e; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	
                } elseif ($workstatus == 2){ 
                    $status = '<font style="color: #da8c10";>Closed</font>';
					$no='<font style="padding: 6px 6px 6px 6px; background-color: #da8c10; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	
                }elseif ($workstatus == 3){ 
                    $status = '<font style="color: #00acd7";>Fullfilled</font>';
					$no='<font style="padding: 6px 6px 6px 6px; background-color: #00acd7; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	
                }elseif ($workstatus == 4){ 
                    $status = '<font style="color: #720377";>Order Ready</font>';
					$no='<font style="padding: 6px 6px 6px 6px; background-color: #720377; border-radius: 4px; width: 30px; text-align: center; display: inline-block;" >'.$j.'</font>';
                	
                };
                
                $tb_item_detail ="detail";       
                $detail = '<a href="'.base_url().'index.php/admin/viewjobdetailsuperadminside/'.$tmpInfo->order_id.'">'.$tb_item_detail.'&nbsp;<i class="fa fa-commenting" aria-hidden="true"></i></a>';
                
               
                $admin_id=$this->session->userdata('login_adminID');
                if($admin_id==1){
                     $tmp = array(
                                'No'           => $no,
                                'order_id'     => $tmpInfo->order_id,                                
                                'buyer_name'    => $tmpInfo->buyer_name,
                                'driver_name'     => $tmpInfo->worker_name,
                                'buyer_address'  => $tmpInfo->buyer_address,
                                'productname'          => $tmpInfo->buyer_products,
                                'cost' => $tmpInfo->buyer_cost,
                                'status'     => $status,
                                'detail'       => $detail,
                                'shop_name' => $tmpInfo->store_name
                    );
                    
                }else{
                     $tmp = array(
                                'No'           => $no, 
                                'order_id'     => $tmpInfo->order_id,                               
                                'buyer_name'    => $tmpInfo->buyer_name,
                                'driver_name'     => $tmpInfo->worker_name,
                                'buyer_address'  => $tmpInfo->buyer_address,
                                'productname'          => $tmpInfo->buyer_products,
                                'cost' => $tmpInfo->buyer_cost,
                                'status'     => $status,
                                'detail'       => $detail,
                                'delivery_id' => $tmpInfo->delivery_id
                    );
                }
                
                array_push($jobinforarray, $tmp);
				//$no='';
                $j++;
            }              
            $result = $jobinforarray;                 
        } else {
            $result = array();
        }
        
        return $result;
       
    }
    
    
    function check_unique_delivery_id($delivery_id){
    	$this->db->select('delivery_id')
                 ->from('tb_orderhistory')
                 ->where('delivery_id', $delivery_id);
                 
        $count=$this->db->get()->num_rows();
		return $count;
		
		
    }
// ******************************************************
  
}
?>