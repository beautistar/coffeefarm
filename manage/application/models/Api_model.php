<?php      
 
define('VALUE_USER_AVAILABLE', 1);
define('VALUE_USER_BUSY', 2);
define('VALUE_USER_PENDING', 0);
define('VALUE_USER_BLOCK', 10);


define('VALUE_JOB_GOINGLIMIT', 10);


define('VALUE_JOB_PENDING', 1);
define('VALUE_JOB_PROCESSING', 3);
define('VALUE_JOB_ACCEPTED', 5);
define('VALUE_JOB_COMPLETED', 11);
define('VALUE_JOB_CANCELLED', 12);


define('MESSAGE_JOB_POSTED', 'A client posted a job near you!');
define('MESSAGE_JOB_CANCELLED', 'Job cancelled.');
define('MESSAGE_JOB_ALLOCATED', "Your job has allocated to ");
define('MESSAGE_JOB_STARTED', " has started the job.");
define('MESSAGE_JOB_FINISHED', " has finished the mowing job.");
define('MESSAGE_JOB_NEARMEAPPEARED', "There is a job near you.");
define('MESSAGE_GOT_FEEDBACK', "You have got feedback from ");

define("VALUE_SEARCH_RANGE", 10);


define('PRICE_PER_MINUTE', 110);
//set stripe ke

require (APPPATH .'third_party/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;
require_once( APPPATH . 'third_party/stripe-php/init.php');
\Stripe\Stripe::setApiKey('sk_test_BW3RP7Ba2e9QDZNlBEtwrJWb');

     
class Api_model extends CI_Model {
    
    function __construct(){
        parent::__construct();
        date_default_timezone_set('America/Chicago');
        $this->load->model('Mail');
    }
    
    
    //======================================= Coffee Farm======================================
    function AdminExist($adminemail, $adminpassword){
         $this->db->where('company_email', $adminemail);
         $cnt = $this->db->get('tb_company')->num_rows();
        
        if ($cnt == 0) {
            return 0;      // email not exist
        }else {
             $this->db->where('company_email', $adminemail);
             $this->db->where('company_password', $adminpassword);
            
             $cnt = $this->db->get('tb_company')->num_rows();
              if ($cnt == 0) {
                return 1;      // Incorrect password
            }else {               
                 return 2;    // correct
            }
        }   
    }
    
    function Employeeexist($adminemail, $adminpassword){
         $this->db->where('farmer_email', $adminemail);
         $cnt = $this->db->get('tb_workers')->num_rows();
        if ($cnt == 0) {
            return 0;      // email not exist
        }else {
             $this->db->where('farmer_email', $adminemail);
             $this->db->where('farmer_password', $adminpassword);
            
             $cnt = $this->db->get('tb_workers')->num_rows();
              if ($cnt == 0) {
                return 1;      // Incorrect password
            }else {               
                 return 2;    // correct
            }
        }   
    }
    
    function normaluserexist($adminemail, $adminpassword){
        $this->db->where('normaluser_email', $adminemail);
         $cnt = $this->db->get('tb_normaluser')->num_rows();
        if ($cnt == 0) {
            return 0;      // email not exist
        }else {
             $this->db->where('normaluser_email', $adminemail);
             $this->db->where('normaluser_password', $adminpassword);
            
             $cnt = $this->db->get('tb_normaluser')->num_rows();
              if ($cnt == 0) {
                return 1;      // Incorrect password
            }else {               
                 return 2;    // correct
            }
        } 
    }
    
    function teamleaderexist($email, $password){
     $this->db->where('teamlead_email', $email);
         $cnt = $this->db->get('tb_teamlead')->num_rows();
        if ($cnt == 0) {
            return 0;      // email not exist
        }else {
             $this->db->where('teamlead_email', $email);
             $this->db->where('teamlead_password', $password);
            
             $cnt = $this->db->get('tb_teamlead')->num_rows();
              if ($cnt == 0) {
                return 1;      // Incorrect password
            }else {               
                 return 2;    // correct
            }
        } 
    }
    
    function pickleaderexist($email, $password){
     $this->db->where('pickerlead_email', $email);
     $this->db->where('picker_type', 0);
         $cnt = $this->db->get('tb_pickerlead')->num_rows();
        if ($cnt == 0) {
            return 0;      // email not exist
        }else {
             $this->db->where('pickerlead_email', $email);
             $this->db->where('pickerlead_password', $password);
            
             $cnt = $this->db->get('tb_pickerlead')->num_rows();
              if ($cnt == 0) {
                return 1;      // Incorrect password
            }else {               
                 return 2;    // correct
            }
        } 
    }
    
    
    function AdminLogin($adminemail, $adminpassword){
        $this->db->where("company_email", $adminemail);
        $this->db->where("company_password", $adminpassword);       
        return $this->db->get('tb_company')->result();        
    }
    
    
    
    
    function EmployeeLogin($adminemail, $adminpassword){
        $this->db->where("farmer_email", $adminemail);
        $this->db->where("farmer_password", $adminpassword);
      
        return $this->db->get('tb_workers')->result();        
    }
    
    
    //=================== Team lead module================
    function Registerteamlead($companyid, $teamleademail, $teamleadname, $teamleadpassword, $teamleadphone,$teamleadaddress){
         $this->db->where("teamlead_company", $companyid);
         $this->db->where("teamlead_name", $teamleadname);   	 
   	 $farms= $this->db->get('tb_teamlead');
   	 $cnt=$farms->num_rows();
   	 if($cnt>0){
   		 return 0;
   	 }else{
	   	  $this->db->set("teamlead_company", $companyid);
	   	  $this->db->set("teamlead_name", $teamleadname);
	   	  $this->db->set("teamlead_email", $teamleademail);
	   	  $this->db->set("teamlead_password", $teamleadpassword);
	   	  $this->db->set("teamlead_phone", $teamleadphone);
	   	  $this->db->set("teamlead_address", $teamleadaddress);
	   	  $this->db->insert('tb_teamlead');
	   	  return 1;
   	 }
    }
    
    function updateteamlead($teamleadid, $companyid, $teamleademail, $teamleadname, $teamleadpassword , $teamleadphone,$teamleadaddress){
         $this->db->where("teamlead_company", $companyid);  
          $this->db->where("teamlead_id", $teamleadid);  	 
   	 $farms= $this->db->get('tb_teamlead');
   	 $cnt=$farms->num_rows();
   	 if($cnt==0){
   		 return 0;
   	 }else{
   	          $this->db->where("teamlead_company", $companyid);  
                  $this->db->where("teamlead_id", $teamleadid);  
	   	  $this->db->set("teamlead_company", $companyid);
	   	  $this->db->set("teamlead_name", $teamleadname);
	   	  $this->db->set("teamlead_email", $teamleademail);
	   	  $this->db->set("teamlead_password", $teamleadpassword);
	   	   $this->db->set("teamlead_phone", $teamleadphone);
	   	  $this->db->set("teamlead_address", $teamleadaddress);
	   	  $this->db->update('tb_teamlead');
	   	  return 1;
   	 }
    }
    function deleteteamlead($teamlead_id){
      $this->db->where("teamlead_id", $teamlead_id);
      $this->db->set("deletestatus",1);
      $this->db->update("tb_teamlead");      
    }
    
    
    function Registerteammember($companyid, $teamleadid, $membername, $memberemail, $memberphone,$memberaddress){
         $this->db->where("farmer_company", $companyid);
         $this->db->where("farmer_teamleadid", $teamleadid); 
         $this->db->where("farmer_name", $membername);  	 
   	 $farms= $this->db->get('tb_workers');
   	 $cnt=$farms->num_rows();
   	 if($cnt>0){
   		 return 0;
   	 }else{
	   	  $this->db->set("farmer_company", $companyid);
	   	  $this->db->set("farmer_teamleadid", $teamleadid);
	   	  $this->db->set("farmer_name", $membername);
	   	  $this->db->set("farmer_email", $memberemail);
	   	  $this->db->set("farmer_phone", $memberphone);
	   	  $this->db->set("farmer_address", $memberaddress);
	   	  $this->db->insert('tb_workers');
	   	  return 1;
   	 }
    }
    
   function Updateteammember($companyid, $teamleadid, $teammemberid,$membername, $memberemail, $memberphone,$memberaddress){
         $this->db->where("farmer_company", $companyid);  
          $this->db->where("farmer_teamleadid", $teamleadid);  	
          $this->db->where("farmer_id", $teammemberid);  
   	 $farms= $this->db->get('tb_workers');
   	 $cnt=$farms->num_rows();
   	 if($cnt==0){
   		 return 0;
   	 }else{
   	          $this->db->where("farmer_company", $companyid);  
                  $this->db->where("farmer_teamleadid", $teamleadid); 
                  $this->db->where("farmer_id", $teammemberid); 
	   	  $this->db->set("farmer_company", $companyid);
	   	  $this->db->set("farmer_teamleadid", $teamleadid);
	   	  $this->db->set("farmer_name", $membername);
	   	  $this->db->set("farmer_email", $memberemail);
	   	  $this->db->set("farmer_phone", $memberphone);
	   	  $this->db->set("farmer_address", $memberaddress);
	   	  $this->db->update('tb_workers');
	   	  return 1;
   	 }
   }
   
   
   function deleteteammember($teammemberid){
      $this->db->where("farmer_id", $teammemberid);
      $this->db->set("deletestatus", 1);
      $this->db->update('tb_workers');
   }
    
    
    //=========   Picker lead register module =============
    function Registerpickerlead($companyid, $pickleadername, $pickleaderemail, $pickleaderpassword,$pickleaderphone, $pickleaderaddress, $pickleadertype, $pickleaderrate, $leaderid){
         $this->db->where("pickerlead_company", $companyid);
         $this->db->where("pickerlead_name", $pickleadername);   	 
   	 $farms= $this->db->get('tb_pickerlead');
   	 $cnt=$farms->num_rows();
   	 if($cnt>0){
   		 return 0;
   	 }else{
	   	  $this->db->set("pickerlead_company", $companyid);
	   	  $this->db->set("pickerlead_name", $pickleadername);
	   	  $this->db->set("pickerlead_email", $pickleaderemail);
	   	  $this->db->set("pickerlead_address", $pickleaderaddress);
	   	  $this->db->set("pickerlead_phone", $pickleaderphone);
	   	  $this->db->set("pickerlead_password", $pickleaderpassword);
	   	  $this->db->set("picker_type", $pickleadertype);
	   	  $this->db->set("rate_lb", $pickleaderrate);
	   	  $this->db->set("leadidofpick", $leaderid);
	   	  $this->db->insert('tb_pickerlead');
	   	  return 1;
   	 }
    }
    
   function Updatepickerlead($leaderid,  $companyid, $pickleadername, $pickleaderemail, $pickleaderpassword, $pickleaderphone, $pickleaderaddress, $pickleadertype, $pickleaderrate){
         $this->db->where("pickerlead_company", $companyid);
         $this->db->where("pickerlead_id", $leaderid);   	 
   	 $farms= $this->db->get('tb_pickerlead');
   	 $cnt=$farms->num_rows();
   	 if($cnt==0){
   		 return 0;
   	 }else{
	   	  $this->db->where("pickerlead_company", $companyid);
	          $this->db->where("pickerlead_id", $leaderid);
	   	  $this->db->set("pickerlead_company", $companyid);
	   	  $this->db->set("pickerlead_name", $pickleadername);
	   	  $this->db->set("pickerlead_email", $pickleaderemail);
	   	  $this->db->set("pickerlead_phone", $pickleaderphone);
	   	  $this->db->set("pickerlead_address", $pickleaderaddress);
	   	  $this->db->set("pickerlead_password", $pickleaderpassword);
	   	  $this->db->set("picker_type", $pickleadertype);
	   	  $this->db->set("rate_lb", $pickleaderrate);
	   	  $this->db->update('tb_pickerlead');
	   	  return 1;
   	 }
  }
  
  function deletepickerlead($pickerleadid){
      $this->db->where("pickerlead_id", $pickerleadid);
      $this->db->set("deletestatus", 1);
      $this->db->update('tb_pickerlead');
  }
  
  function registerpickinglog($picking_farm, $picking_pickerleaderid, $picking_date, $picking_pickername, $picking_lbs, $picking_bags, $picking_company, $totallbs){
        $this->db->where("product_farm", $picking_farm);
        $this->db->where("product_name", "Cherries");
        $currentrow=$this->db->get("tb_productforproduction");
        $cnt=$currentrow->num_rows();
        if($cnt>0){
           $currentquantity=$currentrow->result()[0]->product_quantity;
           
           $this->db->where("product_farm", $picking_farm);
           $this->db->where("product_name", "Cherries");
           $this->db->set("product_quantity", $currentquantity+$totallbs);
           $this->db->update("tb_productforproduction");
        }
        
          $this->db->set("picking_farm", $picking_farm);
          $this->db->set("picking_pickerleaderid", $picking_pickerleaderid);
   	  $this->db->set("picking_date", $picking_date);
   	  $this->db->set("picking_pickername", $picking_pickername);
   	  $this->db->set("picking_lbs", $picking_lbs);
   	  $this->db->set("picking_bags", $picking_bags);
   	  $this->db->set("picking_company", $picking_company);
   	  $this->db->insert('tb_pickingandreceiving');
   	  
        
        
  }
  
  function updatepickinglog($pickinglog_id, $picking_farm, $picking_pickerleaderid, $picking_date, $picking_pickername, $picking_lbs, $picking_bags, $picking_company, $totallbs,$oldtotallbs){
        $this->db->where("product_farm", $picking_farm);
        $this->db->where("product_name", "Cherries");
        $currentrow=$this->db->get("tb_productforproduction");
        $cnt=$currentrow->num_rows();
        if($cnt>0){
           $currentquantity=$currentrow->result()[0]->product_quantity;
           
           $this->db->where("product_farm", $picking_farm);
           $this->db->where("product_name", "Cherries");
           $this->db->set("product_quantity", $currentquantity-$oldtotallbs+$totallbs);
           $this->db->update("tb_productforproduction");
        }
        
          $this->db->where("picking_id", $pickinglog_id);
          $this->db->set("picking_farm", $picking_farm);
          $this->db->set("picking_pickerleaderid", $picking_pickerleaderid);
   	  $this->db->set("picking_date", $picking_date);
   	  $this->db->set("picking_pickername", $picking_pickername);
   	  $this->db->set("picking_lbs", $picking_lbs);
   	  $this->db->set("picking_bags", $picking_bags);
   	  $this->db->set("picking_company", $picking_company);
   	  $this->db->update('tb_pickingandreceiving');
  }
  
  function deletepickinglog($companyid,$pickinglogid, $farmid, $totalamount){
        $this->db->where("product_farm", $farmid);
        $this->db->where("product_name", "Cherries");
        $currentrow=$this->db->get("tb_productforproduction");
        $cnt=$currentrow->num_rows();
        if($cnt>0){
           $currentquantity=$currentrow->result()[0]->product_quantity;
           
           $this->db->where("product_farm", $farmid);
           $this->db->where("product_name", "Cherries");
           $this->db->set("product_quantity", $currentquantity-$totalamount);
           $this->db->update("tb_productforproduction");
        }
        
        $this->db->where("picking_id", $pickinglogid);
        $this->db->delete("tb_pickingandreceiving");
  }
  
  //===================== receiving module =============
  function savenewreceiving($picking_company,$picking_farm,$picking_date,$picking_lbs,$picking_bags,$receiving_clientid,$receiving_quality,$receiving_comment){
          $this->db->set("picking_farm", $picking_farm);
          $this->db->set("picking_lbs", $picking_lbs);
          $this->db->set("pickingtype", 1);
   	  $this->db->set("picking_date", $picking_date);
   	  $this->db->set("picking_bags", $picking_bags);
   	  $this->db->set("receiving_clientid", $receiving_clientid);
   	  $this->db->set("receiving_quality", $receiving_quality);
   	  $this->db->set("picking_company", $picking_company);
   	  $this->db->set("receiving_comment", $receiving_comment);
   	  $this->db->insert('tb_pickingandreceiving');
   	   $insert_id = $this->db->insert_id();
   	  
   	  return $this->createreceiptpdf($insert_id);
  }
  
  function updatereceiving($id,$picking_company,$picking_farm,$picking_date,$picking_lbs,$picking_bags,$receiving_clientid,$receiving_quality,$receiving_comment){
          $this->db->where("picking_id", $id);
          $this->db->set("picking_farm", $picking_farm);
          $this->db->set("picking_lbs", $picking_lbs);
   	  $this->db->set("picking_date", $picking_date);
   	  $this->db->set("picking_bags", $picking_bags);
   	  $this->db->set("receiving_clientid", $receiving_clientid);
   	  $this->db->set("receiving_quality", $receiving_quality);
   	  $this->db->set("picking_company", $picking_company);
   	  $this->db->set("receiving_comment", $receiving_comment);
   	  $this->db->update('tb_pickingandreceiving');
   	  return $this->createreceiptpdf($id);
  }
  
  function createreceiptpdf($insert_id){
   $this->db->where("picking_id", $insert_id);
   $receivedlog=$this->db->get("tb_pickingandreceiving")->result()[0];
   
   $clientid=$receivedlog->receiving_clientid;
   $companyid=$receivedlog->picking_company;
   
   // get company name
     $this->db->where("id", $companyid);
     $companyname=$this->db->get("tb_company")->result()[0]->company_name;
   
   //======= client name==============
   $this->db->where("client_id", $clientid);
   $clientname=$this->db->get("tb_client")->result()[0]->client_name;
   
   //======= received date===========
   $receiveddate=$receivedlog->picking_date;   // should be changed to standard format
   $lbsofcherries=$receivedlog->picking_lbs;
   $bags=$receivedlog->picking_bags;
   
   $result = array('received_from' => $clientname,
                   'company_name' => $companyname,
                   'received_date' => date("m/d/Y", strtotime($receiveddate)),
                   'lbs_quantity' => $lbsofcherries,
                   'bag_quantity' => $bags);
   return $result;
  }
  
  
  
  //=================== Normal User register module ===================
  function Registernewnormaluser($companyid, $name, $email, $phone, $address, $password, $permission){
         $this->db->where("normaluser_company", $companyid);
         $this->db->where("normaluser_email", $email);   	 
   	 $farms= $this->db->get('tb_normaluser');
   	 $cnt=$farms->num_rows();
   	 if($cnt>0){
   		 return 0;
   	 }else{
	   	  $this->db->set("normaluser_company", $companyid);
	   	  $this->db->set("normaluser_name", $name);
	   	  $this->db->set("normaluser_email", $email);
	   	  $this->db->set("normaluser_phone", $phone);
	   	  $this->db->set("normaluser_address", $address);
	   	  $this->db->set("normaluser_password", $password);
	   	  $this->db->set("normaluser_permission", $permission);
	   	  $this->db->insert('tb_normaluser');
	   	  return 1;
   	 }
  }
  
  
  function Updatenormaluser($id, $companyid, $name, $email, $phone, $address, $password, $permission){
        $this->db->where("normaluser_company", $companyid);
         $this->db->where("id", $id);   	 
   	 $farms= $this->db->get('tb_normaluser');
   	 $cnt=$farms->num_rows();
   	 if($cnt==0){
   		 return 0;
   	 }else{
   	          $this->db->where("id", $id);  
	   	  $this->db->set("normaluser_company", $companyid);
	   	  $this->db->set("normaluser_name", $name);
	   	  $this->db->set("normaluser_email", $email);
	   	  $this->db->set("normaluser_phone", $phone);
	   	  $this->db->set("normaluser_address", $address);
	   	  $this->db->set("normaluser_password", $password);
	   	  $this->db->set("normaluser_permission", $permission);
	   	  $this->db->update('tb_normaluser');
	   	  return 1;
   	 }
  }
  
  function deletenormaluser($userid){
      $this->db->where("id", $userid);
      $this->db->delete('tb_normaluser');
  }
  
  
  
  //=============================== Processing module====
  function addnewprocessinglog($company_id,$processing_type,$receivingorpickinglog_id,$processing_date,$lbs_parchmentproduced,$parchment_date,$moisture,$processed_lbs,$method, $processing_farmorclientname,$burlap_bags,$grainpro_bags,$comment,$status){
          $this->db->set("company_id", $company_id);
          $this->db->set("processing_type", $processing_type);
	   	  $this->db->set("receivingorpickinglog_id", $receivingorpickinglog_id);
	   	  $this->db->set("processing_date", $processing_date);
	   	  $this->db->set("lbs_parchmentproduced", $lbs_parchmentproduced);
	   	  $this->db->set("parchment_date", $parchment_date);
	   	  $this->db->set("moisture", $moisture);
	   	  $this->db->set("processed_lbs", $processed_lbs);
	   	  
	   	   $this->db->set("method", $method);
	   	  
	   	 
	   	  $this->db->set("processing_farmorclientname", $processing_farmorclientname);
	   	  $this->db->set("burlap_bags", $burlap_bags);
	   	  $this->db->set("grainpro_bags", $grainpro_bags);
	   	  $this->db->set("comment", $comment);
	   	  $this->db->set("status", $status);
	   	  $this->db->insert('tb_processing');
	   	  
	   	  $insert_id = $this->db->insert_id();
	   	  
	   	
	   	  
	   	  
	   	  if($processing_type==0){  // if it's picking log 
	   	  
	   	  //===================== Cherries decrease in Inventory table==============
	   	    $this->db->where("picking_id",$receivingorpickinglog_id);
	   	    $farmid=$this->db->get("tb_pickingandreceiving")->result()[0]->picking_farm;
	   	    
	   	    $this->db->where("product_farm",$farmid);
	   	    $this->db->where("product_name","Cherries");	   	  
	   	    $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
	   	    
	   	    $this->db->where("product_farm",$farmid);
	   	    $this->db->where("product_name","Cherries");
	   	    
	   	    $totalusedquantity=$processed_lbs;
	   	   
	   	    $this->db->set("product_quantity",$currentquantity-$totalusedquantity);
	   	    $this->db->update("tb_productforproduction");
	   	    
	   	    //============= Parchment quantity increase in Inventory table=============
	   	    $this->db->where("product_farm",$farmid);
	   	    $this->db->where("product_name","Parchment");
	   	    $currentparchmentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
	   	    
	   	    $this->db->where("product_farm",$farmid);
	   	    $this->db->where("product_name","Parchment");
	   	    $this->db->set("product_quantity", $currentparchmentquantity+$lbs_parchmentproduced);
	   	    $this->db->update("tb_productforproduction");
	   	    
	   	    
	   	  }
	   	  //============= add used lbs in picking table=======
	   	    $this->db->where("picking_id",$receivingorpickinglog_id);
	   	    $currentusedlbs=$this->db->get("tb_pickingandreceiving")->result()[0]->used_lbscherries;
	   	    
	   	     $this->db->where("picking_id",$receivingorpickinglog_id);
	   	     $this->db->set("used_lbscherries",$currentusedlbs+$processed_lbs);
	   	     $this->db->update("tb_pickingandreceiving");
	   	     
	   	     if($status=="Closed" && $processingtype=1){    // this part
	   	     return $this->create_invoicepdffile($insert_id, $receivingorpickinglog_id);
	   	  }
  }
  
  
  function updateprocessinglog($processingid,$company_id,$processing_type,$receivingorpickinglog_id,$processing_date,$lbs_parchmentproduced,$parchment_date,$moisture,$processed_lbs,$method,$processing_farmorclientname,$burlap_bags,$grainpro_bags,$comment,$status){
                 
	   	  
	   	 
	   	  
	   	  
	   	   $this->db->where("processing_id",$processingid);
	   	    $currentlow=$this->db->get("tb_processing")->result()[0];
	   	    $previouslbsparchment=$currentlow->lbs_parchmentproduced;
	   	    $previouseprocessed_lbs=$currentlow->processed_lbs;
	   	    
	   	    
	   	  
	   	  
	   	  
	   	  if($processing_type==0){  // if it's picking log 
	   	  
	   	  //===================== Cherries decrease in Inventory table==============
	   	    $this->db->where("picking_id",$receivingorpickinglog_id);
	   	    $farmid=$this->db->get("tb_pickingandreceiving")->result()[0]->picking_farm;
	   	    
	   	    $this->db->where("product_farm",$farmid);
	   	    $this->db->where("product_name","Cherries");	   	  
	   	    $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
	   	    
	   	    $totalusedquantity=$processed_lbs;
	   	    
	   	   
	   	    
	   	    $this->db->where("product_farm",$farmid);
	   	    $this->db->where("product_name","Cherries");	
	   	    $this->db->set("product_quantity",$currentquantity-$totalusedquantity+$previouseprocessed_lbs);
	   	    $this->db->update("tb_productforproduction");
	   	    
	   	    //============= Parchment quantity increase in Inventory table=============
	   	    $this->db->where("product_farm",$farmid);
	   	    $this->db->where("product_name","Parchment");
	   	    $currentparchmentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
	   	    
	   	    $this->db->where("product_farm",$farmid);
	   	    $this->db->where("product_name","Parchment");
	   	    $this->db->set("product_quantity", $currentparchmentquantity+$lbs_parchmentproduced-$previouslbsparchment);
	   	    $this->db->update("tb_productforproduction");
	   	    
	   	    
	   	  }
	   	  //============= add used lbs in picking table=======
	   	    $this->db->where("picking_id",$receivingorpickinglog_id);
	   	    $currentusedlbs=$this->db->get("tb_pickingandreceiving")->result()[0]->used_lbscherries;
	   	    
	   	  //  echo $currentusedlbs;
	   	  //  echo $processed_lbs;
	   	  //  echo $previouseprocessed_lbs;
	   	    
	   	     $this->db->where("picking_id",$receivingorpickinglog_id);
	   	     $this->db->set("used_lbscherries",(int)$currentusedlbs+(int)$processed_lbs-(int)$previouseprocessed_lbs);
	   	     $this->db->update("tb_pickingandreceiving");
	   	     
	   	     
	   	  $this->db->where("processing_id", $processingid);
                  $this->db->set("company_id", $company_id);
                  $this->db->set("processing_type", $processing_type);
	   	  $this->db->set("receivingorpickinglog_id", $receivingorpickinglog_id);
	   	  $this->db->set("processing_date", $processing_date);
	   	  $this->db->set("lbs_parchmentproduced", $lbs_parchmentproduced);
	   	  $this->db->set("parchment_date", $parchment_date);
	   	  $this->db->set("moisture", $moisture);
	   	  $this->db->set("processed_lbs", $processed_lbs);
	   	  $this->db->set("method", $method);   
	   	  $this->db->set("processing_farmorclientname", $processing_farmorclientname);
	   	  $this->db->set("burlap_bags", $burlap_bags);
	   	  $this->db->set("grainpro_bags", $grainpro_bags);
	   	  $this->db->set("comment", $comment);
	   	  $this->db->set("status", $status);
	   	  $this->db->update('tb_processing');
	   	  
	   	  if($status=="Closed" && $processingtype=1){      // and this part
	   	      return $this->create_invoicepdffile($processingid, $receivingorpickinglog_id);
	   	  }
  }
  
  function create_invoicepdffile($processingid,  $receivingorpickinglog_id){         // create receipt part 
     $this->db->where("processing_id", $processingid);
     $processedlog=$this->db->get("tb_processing")->result()[0];
     
     $clientname=$processedlog->processing_farmorclientname;
     $invoicedate=$processedlog->parchment_date;      // should be changed to standard format
     $processingquantity=$processedlog->processed_lbs;
     $lbsparchment=$processedlog->lbs_parchmentproduced;
     //$lbscherries=$processedlog->
     $moisture=$processedlog->moisture;
     $method=$processedlog->method;
     $burlapbags=$processedlog->burlap_bags;
     $plasticbags=$processedlog->grainpro_bags;
     $processeddate=$processedlog->processing_date;
     
     
     $this->db->where("picking_id",$receivingorpickinglog_id);
     $received=$this->db->get("tb_pickingandreceiving")->result()[0];
     $totalreceived=$received->picking_lbs;
     $totawarklbags=$received->picking_bags;
     $clientid=$received->receiving_clientid;
     
          
     
     $companyid=$processedlog ->company_id;     
     $this->db->where("unit_company", $companyid);
     $units=$this->db->get("tb_unitprice")->result()[0];
     
     // get company name
     $this->db->where("id", $companyid);
     $companyname=$this->db->get("tb_company")->result()[0]->company_name;
     
     // unit column data
     $unitprocessing=$units->unit_processing;
     $unitblurap=$units->unit_burlap;
     $unitplasticbags=$units->unit_plastic;
     
     // amount column data
     $quantityamount=$unitprocessing * $totalreceived;
     $burlapamount=$unitblurap *$burlapbags;
     $plasticbags=$unitplasticbags * $plasticbags;
     $subtotal=$quantityamount+$burlapamount+$plasticbags;
     $tax= $subtotal*0.005;
     $feight="0.00";
     $paythigamount=$subtotal+$tax;
     
     $data = array('sold_to' => $clientname,
                   'company_name' => $companyname,
                   'invoice_date' => date("m/d/Y", strtotime($invoicedate)),
                   'total_quantity' => $totalreceived,
                   'burlap_quantity' => $burlapbags,
                   'plastic_bags_quantity' => $plasticbags,
                   'process_date' => $processeddate,
                   'bag_count' => $totawarklbags,
                   'processed_bag_count' => $processingquantity,
                   'returned_lbs_count' =>  $lbsparchment,
                   'moisture_percent' => $moisture,       // should be shown % => $moisture %
                   'recovery_percent' => round(100*$lbsparchment/$processingquantity , 2),  // let's keep this for now 
                   'unite_price_1' => $unitprocessing,
                   'unite_price_2' => $unitblurap,
                   'unite_price_3' =>  $unitplasticbags,
                   'amount_1' =>$quantityamount,
                   'amount_2' => $burlapamount,
                   'amount_3' => $plasticbags,
                   'sub_total' => $subtotal,
                   'tax' =>   $tax,
                   'feight' => $feight,
                   'pay_this_amount' => $paythigamount,                  
                   'method' => $method,                  
                   );
     
     return $data;
    
  }
  
  
  
  function deleteprocessing($processingid){
                    $this->db->where("processing_id",$processingid);
	   	    $currentlow=$this->db->get("tb_processing")->result()[0];
	   	    $previouslbsparchment=$currentlow->lbs_parchmentproduced;
	   	    $previouseprocessed_lbs=$currentlow->processed_lbs;
	   	    $processingtype=$currentlow->processing_type;
	   	    $receivedorpicked=$currentlow->receivingorpickinglog_id;
	   	    $totalusedquantity=$currentlow->processed_lbs;
	   	    if($processingtype==0){  // picked log
		   	    //===================== Cherries Increase in Inventory table==============
		   	    $this->db->where("picking_id",$receivedorpicked);
		   	    $farmid=$this->db->get("tb_pickingandreceiving")->result()[0]->picking_farm;
		   	    
		   	    $this->db->where("product_farm",$farmid);
		   	    $this->db->where("product_name","Cherries");	   	  
		   	    $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
		   	    
		   	    
		   	    
		   	   
		   	    
		   	    $this->db->where("product_farm",$farmid);
		   	    $this->db->where("product_name","Cherries");	
		   	    $this->db->set("product_quantity",$currentquantity+$totalusedquantity);
		   	    $this->db->update("tb_productforproduction");
		   	    
		   	    //============= Parchment quantity decrease in Inventory table=============
		   	    $this->db->where("product_farm",$farmid);
		   	    $this->db->where("product_name","Parchment");
		   	    $currentparchmentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
		   	    
		   	    $this->db->where("product_farm",$farmid);
		   	    $this->db->where("product_name","Parchment");
		   	    $this->db->set("product_quantity", $currentparchmentquantity-$previouslbsparchment);
		   	    $this->db->update("tb_productforproduction");
	   	    
	   	    }
	   	    
	   	     //============= add used lbs in picking table=======
	   	    $this->db->where("picking_id",$receivedorpicked);
	   	    $currentusedlbs=$this->db->get("tb_pickingandreceiving")->result()[0]->used_lbscherries;
	   	
	   	    
	   	     $this->db->where("picking_id",$receivedorpicked);
	   	     $this->db->set("used_lbscherries",(int)$currentusedlbs-(int)$previouseprocessed_lbs);
	   	     $this->db->update("tb_pickingandreceiving");
	   	     
	   	     
	   	     $this->db->where("processing_id", $processingid);
	   	     $this->db->delete("tb_processing");
  }
  
  
  
  
  
  
  
  
    
    
    function Registerfarm($companyid, $farmname){
   	 $this->db->where("company_id", $companyid);
   	 $this->db->where("farm_name", $farmname);
   	 $farms= $this->db->get('tb_farm');
   	 $cnt=$farms->num_rows();
   	 if($cnt>0){
   		 return 0;
   	 }else{
	   	  $this->db->set("company_id", $companyid);
	   	  $this->db->set("farm_name", $farmname);
	   	  $this->db->insert('tb_farm');
	   	  return 1;
   	 }
    
    }
    
    function deletefarm($farmid){
     $this->db->where("farm_id", $farmid);
     $this->db->delete('tb_farm');
    }
    
    function updatefarm($farmid,$framname){
	     $this->db->where("farm_id", $farmid);
	     $this->db->set("farm_name", $framname);
	      $this->db->update('tb_farm');
    }
    
    
    function Registernewfarmer($companyid, $farmsid, $username,$password, $address, $userphone, $useremail ){
                  $this->db->set("farmer_company", $companyid);
	   	  $this->db->set("farmer_farms", $farmsid);
	   	  $this->db->set("farmer_name", $username);
	   	  $this->db->set("farmer_password", $password);
	   	  $this->db->set("farmer_email", $useremail );
	   	  $this->db->set("farmer_phone", $userphone);
	   	  $this->db->set("farmer_address", $address);
	   	  $this->db->insert('tb_workers');
    }
    
    function updatefarmer($farmer_id ,$companyid, $farmsid, $username,$password, $address, $userphone, $useremail){
                  $this->db->where("farmer_id", $farmer_id);
                  $this->db->set("farmer_company", $companyid);
	   	  $this->db->set("farmer_farms", $farmsid);
	   	  $this->db->set("farmer_name", $username);
	   	  $this->db->set("farmer_password", $password);
	   	  $this->db->set("farmer_email", $useremail );
	   	  $this->db->set("farmer_phone", $userphone);
	   	  $this->db->set("farmer_address", $address);
	   	  $this->db->update('tb_workers');
    }
    
    function registerneworganic($organic_companyid,$organic_farmid, $organiclog_date, $organiclog_acitivty,$organiclog_productused, $organiclog_productquantity,$organiclog_refofbill, $assigned_teamleader,  $logstatus){
                 
                
    
                  $this->db->set("organic_companyid", $organic_companyid);
	   	  $this->db->set("organic_farmid", $organic_farmid);
	   	  $this->db->set("organiclog_date", $organiclog_date);
	   	  $this->db->set("organiclog_acitivty", $organiclog_acitivty);
	   	  $this->db->set("organiclog_productused", $organiclog_productused);
	   	  $this->db->set("organiclog_productquantity", $organiclog_productquantity);	   
	   	  $this->db->set("organiclog_refofbill", $organiclog_refofbill);
	   	  $this->db->set("organic_teamlead", $assigned_teamleader);
	   	  $this->db->set("organic_status", $logstatus);		 
	   	  $this->db->insert('tb_organiclog');
    }
    
    function updateorganic($organiclog_id, $organic_companyid,$organic_farmid, $organiclog_date, $organiclog_acitivty,$organiclog_productused, $organiclog_productquantity, $organiclog_refofbill, $assigned_teamleader,  $logstatus){
                  $this->db->where("organiclog_id", $organiclog_id);
	   	  $this->db->set("organic_farmid", $organic_farmid);
	   	  $this->db->set("organiclog_date", $organiclog_date);
	   	  $this->db->set("organiclog_acitivty", $organiclog_acitivty);
	   	  $this->db->set("organiclog_productused", $organiclog_productused);
	   	  $this->db->set("organiclog_productquantity", $organiclog_productquantity);	   
	   	  $this->db->set("organiclog_refofbill", $organiclog_refofbill);
	   	  $this->db->set("organic_teamlead", $assigned_teamleader);
	   	  $this->db->set("organic_status", $logstatus);		 
	   	  $this->db->update('tb_organiclog');
    }
    
   function registernewproduction($production_companyid, $production_farmid,  $production_date,  $production_product,$production_quantity, $production_roasted, $production_batch, $production_moisture, $production_millprocessed ){
                  $this->db->set("production_companyid", $production_companyid);
	   	  $this->db->set("production_farmid", $production_farmid);
	   	  $this->db->set("production_date", $production_date);
	   	  $this->db->set("production_product", $production_product);
	   	  $this->db->set("production_quantity", $production_quantity);
	   	  $this->db->set("production_batch", $production_batch);
	   	  $this->db->set("production_moisture", $production_moisture);	   
	   	  $this->db->set("production_millprocessed ", $production_millprocessed );
	   	  $this->db->set("production_roasted", $production_roasted );		 
	   	  $this->db->insert('tb_production');
	   	  
	   	  $this->db->where("product_company",$production_companyid);
	   	  $this->db->where("product_name",$production_product);
	   	  $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
	   	  $currentquantity1=$currentquantity+$production_quantity;	
	   	  
	   	 // print_r($currentquantity);
	   	  	   	// print_r($production_quantity);
	   	  	   	  	   	// print_r($currentquantity1);   	  
	   	  
	   	  
	   	   $this->db->where("product_name",$production_product);
	   	   $this->db->set("product_quantity",$currentquantity1);
	   	   $this->db->update("tb_productforproduction");
	   	   
	   	   if(strlen($production_roasted)>0){
	   	          $this->db->where("product_company",$production_companyid);
		   	  $this->db->where("product_name",$production_roasted);
		   	  $currentquantity2=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
		   	  $currentquantity3=$currentquantity2-$production_quantity;
		   	  
		   	   $this->db->where("product_name",$production_roasted);
		   	   $this->db->set("product_quantity",$currentquantity3);
		   	   $this->db->update("tb_productforproduction");
	   	  }
	   	  
   }
   
   function updateproduction($production_id, $production_companyid, $production_farmid,  $production_date,  $production_product,$production_quantity, $production_batch, $production_moisture, $production_millprocessed ){
      
       $this->db->where("production_id",$production_id);
       $currentquantity=$this->db->get("tb_production")->result()[0]->production_quantity;
       $currentproduct=$this->db->get("tb_production")->result()[0]->production_product;  // previouse product id
       
        $this->db->where("product_id",$currentproduct);
        $currentquantityinproducttable=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
        
        // decrease previouse product's quantity 
        $this->db->where("product_id",$currentproduct);
        $this->db->set("product_quantity", $currentquantityinproducttable-$currentquantity);
        $this->db->update("tb_productforproduction");
        
        // update production table
        
          $this->db->where("production_id",$production_id);
          $this->db->set("production_companyid", $production_companyid);
   	  $this->db->set("production_farmid", $production_farmid);
   	  $this->db->set("production_date", $production_date);
   	  $this->db->set("production_product", $production_product);
   	  $this->db->set("production_quantity", $production_quantity);
   	  $this->db->set("production_batch", $production_batch);
   	  $this->db->set("production_moisture", $production_moisture);	   
   	  $this->db->set("production_millprocessed ", $production_millprocessed );		 
   	  $this->db->update('tb_production');
   	  
   	  
   	  // increase new selected product's quantity in product for production table 
   	  
   	   $this->db->where("product_id",$production_product);
   	   $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
   	   $currentquantity=$currentquantity+$production_quantity;
   	  
   	   $this->db->where("product_id",$production_product);
   	   $this->db->set("product_quantity",$currentquantity);
   	   $this->db->update("tb_productforproduction");
   
   }
   
   
   function registernewincome($income_company, $income_date, $income_category,  $income_client,  $income_invoice,$income_amount, $income_taxstatus, $income_invicestatus, $income_productsold,$income_productquantity){
                  $this->db->set("income_company", $income_company);
	   	  $this->db->set("income_date", $income_date);
	   	  $this->db->set("income_category", $income_category);
	   	  $this->db->set("income_client", $income_client);
	   	  $this->db->set("income_invoice", $income_invoice);
	   	  $this->db->set("income_amount", $income_amount);
	   	  $this->db->set("income_taxstatus", $income_taxstatus);	   
	   	  $this->db->set("income_invicestatus", $income_invicestatus);
	   	  $this->db->set("income_productsold", $income_productsold);
	   	  $this->db->set("income_productquantity", $income_productquantity);
	   	  		
	   	  
	   	  if($income_taxstatus=="Yes"){
	   	    $taxconsts=$this->db->get("tb_taxamounts")->result();
	   	    $retailconst=$taxconsts[0]->cost;
	   	    $wholescaleconst=$taxconsts[1]->cost;
	   	    $this->db->set("income_wholescale", $income_amount*$wholescaleconst);
	   	    $this->db->set("income_retail", $income_amount*$retailconst);	   	    
	   	  }
	   	  
	   	  $this->db->insert('tb_income');
	   	  
	   	   $incomeproductsold=explode(",",$income_productsold);
	   	   $incomeproductquantity=explode(",",$income_productquantity);
	   	   $i=0;
	   	   //print_r("test==".sizeof($incomeproductsold));
	   	   
	   	   if(sizeof($incomeproductsold)>0 && strlen($incomeproductsold[0])>0){
	   	   
		   	   foreach($incomeproductsold as $oneproduct){
		   	     $this->db->where("product_company",$income_company);
		   	     $this->db->where("product_name",$oneproduct);
		   	     $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
		   	     
		   	     $this->db->where("product_company",$income_company);
		   	     $this->db->where("product_name",$oneproduct);
		   	     $this->db->set("product_quantity",$currentquantity-$incomeproductquantity[$i]);
		   	     $this->db->update("tb_productforproduction");
		   	     $i++;
		   	     
		   	   }
	   	   }
	   	   
	   	  
	   	   
	   	 
   }
   
   function registernewexpenese($expenses_company, $expenses_date, $expenses_invoice,  $expenses_provider,  $expenses_amount, $expenses_farm, $expenses_category){
    		  $this->db->set("expenses_company", $expenses_company);
	   	  $this->db->set("expenses_date", $expenses_date);
	   	  $this->db->set("expenses_invoice", $expenses_invoice);
	   	  $this->db->set("expenses_provider", $expenses_provider);
	   	  $this->db->set("expenses_amount", $expenses_amount);
	   	  $this->db->set("expenses_farm", $expenses_farm);
	   	  $this->db->set("expenses_category", $expenses_category);	   
	   	  $this->db->insert('tb_expensses');
   }
   
   function updateexpenese($expenses_id, $expenses_company, $expenses_date, $expenses_invoice,  $expenses_provider,  $expenses_amount, $expenses_farm, $expenses_category){
                  $this->db->where("expenses_id",$expenses_id);
    		  $this->db->set("expenses_company", $expenses_company);
	   	  $this->db->set("expenses_date", $expenses_date);
	   	  $this->db->set("expenses_invoice", $expenses_invoice);
	   	  $this->db->set("expenses_provider", $expenses_provider);
	   	  $this->db->set("expenses_amount", $expenses_amount);
	   	  $this->db->set("expenses_farm", $expenses_farm);
	   	  $this->db->set("expenses_category", $expenses_category);	   
	   	  $this->db->update('tb_expensses');
   }
   
   function registernewroastinglog($roast_company, $roast_farm,  $roast_date, $roast_typein,  $roast_typeout,  $roast_intemp, $roast_outtemp, $roast_duration, $roast_lbsin, $roast_lbsout){
   
                  if(strlen($roast_typein)>0){
                    $this->db->where("product_name",$roast_typein);
                    $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
                    
                    $this->db->where("product_name",$roast_typein);
                    $this->db->set("product_quantity",$currentquantity-$roast_lbsin);
                    $this->db->update("tb_productforproduction");
                    
                  }
                  
                  if(strlen($roast_typeout)>0){
                    $this->db->where("product_name",$roast_typeout);
                    $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
                    
                    $this->db->where("product_name",$roast_typeout);
                    $this->db->set("product_quantity",$currentquantity+$roast_lbsout);
                    $this->db->update("tb_productforproduction");
                  }
   
                  $this->db->set("roast_company", $roast_company);
                  $this->db->set("roast_farm", $roast_farm);
	   	  $this->db->set("roast_date", $roast_date);
	   	  $this->db->set("roast_typein", $roast_typein);
	   	  $this->db->set("roast_typeout", $roast_typeout);
	   	  $this->db->set("roast_intemp", $roast_intemp);
	   	  $this->db->set("roast_outtemp", $roast_outtemp);
	   	  $this->db->set("roast_duration", $roast_duration);
	   	  $this->db->set("roast_lbsin", $roast_lbsin);
	   	  $this->db->set("roast_lbsout", $roast_lbsout);	   
	   	  $this->db->insert('tb_roastlog');
   }
   
   function updateroastinglog($id, $roast_company, $roast_farm, $roast_date, $roast_typein,  $roast_typeout,  $roast_intemp, $roast_outtemp, $roast_duration, $roast_lbsin, $roast_lbsout){
                   $this->db->where("id",$id);
                  $this->db->set("roast_company", $roast_company);
                  $this->db->set("roast_farm", $roast_farm);
	   	  $this->db->set("roast_date", $roast_date);
	   	  $this->db->set("roast_typein", $roast_typein);
	   	  $this->db->set("roast_typeout", $roast_typeout);
	   	  $this->db->set("roast_intemp", $roast_intemp);
	   	  $this->db->set("roast_outtemp", $roast_outtemp);
	   	  $this->db->set("roast_duration", $roast_duration);
	   	  $this->db->set("roast_lbsin", $roast_lbsin);
	   	  $this->db->set("roast_lbsout", $roast_lbsout);	   
	   	  $this->db->update('tb_roastlog');
   }
   
    function setVerificationDetails($email)
    {
        //$this->db->query("update tbl_users SET ActivationCode = '".$activationCode."', ActivationPin = '".$pin."' where user_email = '".$email."'");
        $mail = Mail::setUpMail();        
        $mail->addAddress($email);
        $mail->isHTML(FALSE);
        $mail->Subject = "Verify code";
        $mail->Body = "Hi, Please verify your account using the following PIN code: ";

        if (! $mail->send()) {
            //echo "ERROR!" . $mail->ErrorInfo;
            return [
                KEY_RESULT=>RES_FAIL,
                KEY_RES_MESSAGE=>"Verification failed!"];
        }

        return [
            KEY_RESULT=>RES_SUCCESS, 
            KEY_RES_MESSAGE=>"Verification code successfully sent! Please check your email!",
            "pin"=>$pin];
    }
   
   
    
    
  
    //========================================================================================
    
    
    
    
    
    
    
     function getAdminModelKeys() {
        return [
            "id",
            "admin_firstname",
            "admin_lastname",
            "admin_email",
            "admin_dob",
            "admin_socialnumber",
            "storenumber",
            "admin_token",
            "admin_password"                  
        ];
    }
    
    function getEmployeeModelKeys() {
        return [
            "emp_id",
            "emp_firstname",
            "emp_lastname",
            "emp_dob",
            "emp_secnumber",
            "emp_storenumber",
            "emp_storeid", 
            "emp_token",
            "emp_email",
            "emp_password"                 
        ];
    }
    
     function getWorkModelKeys() {
        return [
            "work_id",
            "work_title",
            "work_startdate",
            "work_enddate",
            "work_comment",
            "worker_id",
            "status",
            "store_id"                  
        ];
    }
    function checkemailexist_admin($useremail){
         $this->db->where('admin_email', $useremail);
         $cnt = $this->db->get('tb_admin')->num_rows();
        if ($cnt == 0) {
            return 0;      // email not exist
        }else {
             return 1;    // email exist
        }        
   }
   function checkemailexist_worker($useremail){
         $this->db->where('emp_email', $useremail);
         $cnt = $this->db->get('tb_employee')->num_rows();
        if ($cnt == 0) {
            return 0;      // email not exist
        }else {
             return 1;    // email exist
        }        
    }



    
    function RegisternewAdmin($firstname, $lastname, $useremail, $userdob,$userpassword,$storenumber, $socialnumber ) {
       
        $this->db->set("admin_firstname", $firstname);
        $this->db->set("admin_lastname", $lastname);
        $this->db->set("admin_email", $useremail);
        $this->db->set("admin_dob", $userdob);
        $this->db->set("admin_socialnumber", $socialnumber);
        $this->db->set("storenumber", $storenumber);
      //  $this->db->set("admin_token", $token);
        $this->db->set("admin_password", $userpassword);
         
        $this->db->insert('tb_admin');   
          
        $user_id = $this->db->insert_id();
        return $this->db->where("id", $user_id)->get("tb_admin")->row();
        //return $user_id;
        
    }
    
    
    function RegisternewEmployee($firstname, $lastname, $useremail, $userdob,$userpassword,$storenumber, $socialnumber ,$storeid) {
       
        $this->db->set("emp_firstname", $firstname);
        $this->db->set("emp_lastname", $lastname);
        $this->db->set("emp_email", $useremail);
        $this->db->set("emp_dob", $userdob);
        $this->db->set("emp_secnumber", $socialnumber);
        $this->db->set("emp_storenumber", $storenumber);
       // $this->db->set("emp_token", $token);
        $this->db->set("emp_storeid",$storeid);
        $this->db->set("emp_password", $userpassword);
         
        $this->db->insert('tb_employee');   
          
        $user_id = $this->db->insert_id();
        return $this->db->where("emp_id", $user_id)->get("tb_employee")->row();
        //return $user_id;
        
    }
    
    function AddnewWork($worktitle, $workstartdate, $workenddate, $workcomment, $worker_id, $store_id){
        $this->db->set("work_title", $worktitle);
        $this->db->set("work_startdate", $workstartdate);
        $this->db->set("work_enddate", $workenddate);
        $this->db->set("work_comment", $workcomment);
        $this->db->set("worker_id", $worker_id);
        $this->db->set("store_id", $store_id);
        $this->db->set("status","0");
         $this->db->insert('tb_work');            
        
    }
    
     function updateAdmin($user) {

        $userModelKeys = $this->getAdminModelKeys();
        foreach ($userModelKeys as $key){
             if (array_key_exists($key, $user) && $key != "id") {
                  $this->db->set($key, $user[$key]);
             }           
        }
         
        $this->db->where('id', $user['id']);
        $this->db->update('tb_admin');
        return $user['id'];
    }
    
    
    function updateEmployee($user) {

        $userModelKeys = $this->getEmployeeModelKeys();
        foreach ($userModelKeys as $key){
             if (array_key_exists($key, $user) && $key != "id") {
                  $this->db->set($key, $user[$key]);
             }           
        }
         
        $this->db->where('emp_id', $user['emp_id']);
        $this->db->update('tb_employee');
        return $user['emp_id'];
    }
    
    function updatetokenAdmin($userid, $token){
         $this->db->where("id", $userid);            
        $this->db->set("admin_token", $token);         
        $this->db->update("tb_admin");        
    }
    function updatetokenEmployee($userid, $token){
         $this->db->where("emp_id", $userid);            
        $this->db->set("emp_token", $token);         
        $this->db->update("tb_employee");        
    }
    
    function getAllemployeesbystoreid($storeid){
        $this->db->where("emp_storeid", $storeid);
        return $this->db->get("tb_employee")->result();
    }
    
    function getAllworksbyworkerid($workerid){
        $this->db->where("worker_id", $workerid);
        return $this->db->order_by("work_id", "DESC")->get("tb_work")->result();
    }
    function getAllstores(){
        return $this->db->get("tb_admin")->result();
    }
    
    
    
    //===========%%%%%%%%%%%%%%%%%%%%%%%%%%%%================================
    
    
    
    
    
    
   


   
   
     
    
}
     
?>