<!--<div class="row">           
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">  
        
            <li ><h3>Add New Store</h3></li> 
            
        </ol>
    </div>
</div>-->
<div class="row" style = "padding-top: 0px;" style="background-color:#e0ad00;">
    <div id="breadcrumb" class="col-md-12" style="background-color:#e0ad00;">
        <ol>
            <h4><font color="white">Order Detail</font><a href="<?php echo $this->config->base_url(); ?>index.php/admin/store_main" type="button" class="pull-right"><img style="width: 60px; height: 45px; margin-top: -11px;" src="<?php echo $this->config->base_url(); ?>uploadfiles/images/image.png" /></a></h4>
             
        </ol>
    </div>
</div>
<br>



<div class="row container-fluid">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
          <?php echo validation_errors(); ?>
          

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id = "main-form" onsubmit="return confirm('Are you sure to delete this order?');" action = "<?php echo base_url();?>index.php/Admin/deleteorder" method="POST" enctype="multipart/form-data">
             <br>
             <label for="username"   style="text-align: center; width: 100%; color:red; font-size: 30px;">Products Information</label>
            <!----- products ---->
            <table  width="100%">
            
            	    <tr  width="100%">
            	    	<input type="hidden" name="order_id" id="order_id" value="<?php echo $admin_info[0]->order_id; ?>" />
                          <td style="padding: 5px 10px 5px 5px;">
                               <div class="form-group">
                                  <label for="idnumber">Product Ids</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->product_id?>" required>
                                </div>
                          </td>
                   </tr>
                    <tr  width="100%">
                          <td style="padding: 5px 10px 5px 5px;">
                               <div class="form-group">
                                  <label for="idnumber">Products</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->buyer_products ?>" required>
                                </div>
                          </td>
                   </tr>
                   <tr>
                          <td style="padding: 5px 5px 5px 5px;">
                                <div class="form-group">
                                  <label for="idnumber">Cost (USD)</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->buyer_cost ?>" required>
                                </div>
                          </td>
                        
                     </tr>
                     <tr>
                          <td style="padding: 5px 5px 5px 5px;">
                                <div class="form-group">
                                  <label for="idnumber">Short Description</label>
                                  <input type="text" readOnly = true class="form-control " name = "shortdescription" value = "<?= $admin_info[0]->short_description ?>" required>
                                </div>
                          </td>
                        
                     </tr>
                     
                    <tr>
                    	<td style="padding: 5px 5px 5px 5px;">
                    		<?php if($admin_info[0]->status=='0'){ ?>
                    		<div class="form-group">
                    			<label>Fullfilled By</label>
                    			<select  name="store_employees" id="store_employees" <?php if($admin_info[0]->employee_id!=''){ echo "disabled"; } ?> style="height: 33px; width: 140px; margin: 0px 7px; padding-left: 5px;">
                    		<?php foreach ($employees_store as $key ) { if($key->employee_id!=''){ ?>
                			<option value="<?php echo $key->employee_id; ?>" <?php if($admin_info[0]->employee_id!='' && $admin_info[0]->employee_id==$key->employee_id){ echo "SELECTED"; } ?> ><?php echo $key->employee_first_name. " ". $key->employee_last_name; ?></option>
							<?php	} } ?>
                    	</select>
                    		<a onclick="assignEmployee()" type="button" id="confirm" class="btn btn-primary" style="display:<?php if($admin_info[0]->employee_id!=''){ echo "none"; } ?> ">Confirm</a>
                    		<a onclick="changeStatus()" type="button" id="fullfilled" class="btn btn-success" style="margin-left: 80px" <?php if($admin_info[0]->employee_id==''){ echo "disabled" ; } ?>>Fullfilled</a>
                    		<?php }elseif($admin_info[0]->status!='0' && $admin_info[0]->employee_id!='' && !empty($employee_name)){ ?>
                    		
                                <!-- <div class="form-group"> -->
                                  <label for="idnumber">Employee Name</label> 
                                  <input type="text" readOnly = true class="form-control" name = "employee_name" value = "<?php echo $employee_name[0]->employee_first_name. " ". $employee_name[0]->employee_last_name; ?>" required>
                                <!-- </div> -->
                         
                    		<?php } ?>
                    		</div>
                    		 
                    		 
                    		 <div class="form-group">
                    		 	
                    		 </div>
                    		 
                    	</td>
                    </tr>
                   
                </table>
             
                <!-- Provider -->
                <table width="100%">
                    <tr width="100%">
                        <td width="50%">
                             <div class="box-body">
                             <input type="hidden" name = "proid" value = "<?= $admin_info[0]->order_id ?>">
                             <div width="100%" class="form-group">                            
                             <label for="username"   style="text-align: center; width: 100%; color:red; font-size: 30px;">Buyer Infomation</label>
                            </div>
                  
                            <div class="form-group">
                            
                              <label for="username">Buyer Name</label>
                              <input type="text" readOnly = true class="form-control " name = "storename" value = "<?= $admin_info[0]->buyer_name ?>" required>
                             
                            </div>
                            
                            <div class="form-group">                                                                                                                        
                              <label for="username">Buyer Address</label>
                              <input type="text" readOnly = true class="form-control " name = "managername" value = "<?= $admin_info[0]->buyer_address ?>" required>
                            </div>
                            <div class="form-group">
                              <label for="idnumber">Buyer Phoneumber</label>
                              <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->buyer_phonenumber ?>" required>
                            </div>
                            
                            <div class="form-group">
                              <label for="idnumber">Buyer Email</label>
                              <input type="text" readOnly = true class="form-control " name = "email" value = "<?= $admin_info[0]->buyer_email ?>" required>
                            </div>
                        </td>
                        
                         <td width="50%">
                              <div class="form-group">                            
                                <label for="username"   style="text-align: center; width: 100%; color:red; font-size: 30px;">Delivery Infomation</label>
                             </div>
                              <div class="form-group">
                              <label for="type">Deliver Name</label>
                              <input type="text" readOnly = true class="form-control "  name = "password"  value = "<?= $admin_info[0]->worker_name;?>" >
                            </div>  
                            
                        <?php
                            if($admin_info[0]->worker_id==0){
                        ?>
                              <div class="form-group">                                                                                                                        
                                 <label for="username">Driver Address</label>
                                 <input type="text" readOnly = true class="form-control " name = "managername" value = "" required>
                             </div>
                             <div class="form-group">
                              <label for="idnumber">Driver Phoneumber</label>
                              <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "" required>
                            </div>
                             <div class="form-group">
                              <label for="idnumber">Driver Email</label>
                              <input type="text" readOnly = true class="form-control " name = "email" value = "" required>
                            </div>
                        <?php
                            } else {
                        ?>
                              
                              <div class="form-group">                                                                                                                        
                                  <label for="username">Driver Address</label>
                                  <input type="text" readOnly = true class="form-control " name = "managername" value = "<?=$workerinfo[0]->user_address  ?>" required>
                              </div>
                              <div class="form-group">
                                  <label for="idnumber">Driver Phoneumber</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $workerinfo[0]->user_phonenumber ?>" required>
                            </div>
                             <div class="form-group">
                              <label for="idnumber">Driver Email</label>
                              <input type="text" readOnly = true class="form-control " name = "email" value = "<?= $workerinfo[0]->user_email ?>" required>
                            </div>
                              <?php
                        }
                        ?>
                            
                            
                            
                           
                         </td>
                    </tr>
                     
                </table>
                
                
                
                
                
                
                
                
                                       
                     <label for="username"   style="text-align: center; width: 100%; color:red; font-size: 30px;">Proccessed Date and Time</label>
              
                <table  width="100%">
                    <tr  width="100%">
                          <td style="padding: 5px 10px 5px 5px;">
                               <div class="form-group">
                                  <label for="idnumber">Open Date and Time</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->start_time ?>" required>
                                </div>
                          </td>
                          <td style="padding: 5px 5px 5px 5px;">
                                <div class="form-group">
                                  <label for="idnumber">Fillfulled Date and Time</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->fillfulledtime ?>" required>
                                </div>
                          </td>
                          <td style="padding: 5px 5px 5px 5px;">
                                <div class="form-group">
                                  <label for="idnumber">Accepeted Date and Time</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->accept_time ?>" required>
                                </div>
                          </td>
                          
                          <td style="padding: 5px 5px 5px 5px;">
                               <div class="form-group">
                                  <label for="idnumber">Picked Up Date and Time</label>
                                  <input type="text"  readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->picked_time ?>" required>
                                </div>
                          </td>                          
                          
                          <td style="padding: 5px 5px 5px 5px;">
                                <div class="form-group">
                                  <label for="idnumber">Closed Date and Time</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->closed_time ?>" required>
                                </div>
                          </td>
                     </tr>
                </table>
                
              <div class="box-footer">
                    <button type="submit" class="btn btn-danger input-lg col-lg-12" >Delete Order</button>
                    <!-- <br><br>
                     <a style="margin-top: 10px;" href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary input-lg col-lg-12" >Back</a> -->
              </div>
                 
            
            
                      
            </form>
          </div>
     </div>
</div>

<script type="text/javascript">
//$('#fullfilled').attr("disabled","disabled");
	function assignEmployee(){
		var order_id = $("#order_id").val();
		 console.log(order_id);
		 
		 var employee_id=$("select#store_employees option").filter(":selected").val();
		 var admin_id="<?php echo $this->session->userdata('login_adminID'); ?>";
		  $.ajax({
            type: 'POST',
            data: {order_id: order_id,
            employee_id: employee_id},
            url: "<?php echo base_url();?>index.php/admin/assignEmployee",
            dataType: 'json',
            success: function(JSONObject) {
            	console.log(JSONObject);
             	if(JSONObject.result!='no'){
             		console.log('done');
             		$('#store_employees').attr("disabled","disabled");
             		//location.reload(true);
             		//$('#confirm').attr("disabled","disabled");
             		$('#confirm').css({'display':'none'});
             		$('#fullfilled').removeAttr("disabled");
             	}else{
             		console.log('not assigned');
             	}
            }
        });
	}
	
	function changeStatus(){
		var order_id = $("#order_id").val();
		  $.ajax({
            type: 'POST',
            data: {order_id: order_id},
            url: "<?php echo base_url();?>index.php/admin/changeStatus",
            dataType: 'json',
            success: function(JSONObject) {
            	console.log(JSONObject);
             	if(JSONObject.result!='no'){
             		
             		console.log('done');
             		location.reload(true);
             	}else{
             		console.log('not assigned');
             	}
            }
        });
	}
</script>
<!--<script type="text/javascript">
    $("#main-form").validate({
        rules: {
            proFirstName: "required",
            proLastName: "required",
            proEmail: "required",
            proPassword: "required",
            
            proAddress: "required",
            proCompany: "required"
        }
    });
    

</script>-->

<!--<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#proProfileImage_11')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>-->

<?php
        if($this->session->flashdata('message')){
        ?>
        <script>
            alert('<?=$this->session->flashdata('message')?>');
        </script>
        <?php
        }
?>   