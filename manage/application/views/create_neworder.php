<!--<div class="row">           
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">  
        
            <li ><h3>Add New Store</h3></li> 
            
        </ol>
    </div>
</div>-->

<div class="row" style = "padding-top: 0px;" style="background-color:#e0ad00;">
    <div id="breadcrumb" class="col-md-12" style="background-color:#e0ad00;">
        <ol>
            <h4><font color="white">Create New Order</font></h4> 
        </ol>
    </div>
</div>
<br>



<div class="row container-fluid">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
          <?php echo validation_errors(); ?>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id = "main-form" action = "<?php echo base_url();?>index.php/Admin/createNeworderredirect" method="POST" enctype="multipart/form-data">
                <!-- Provider -->
                <div class="box-body">
                   <input type="hidden" name = "proid" value = "">

               
                
                <div class="form-group">
                  <label for="username">Buyer Name</label>
                  <input type="text" class="form-control " name = "buyername" value = "" required>
                </div>
                
                <div class="form-group">                                                                                                                        
                  <label for="username">Address</label>
                  <input type="text" class="form-control " name = "address" value = "" required>
                </div>
                <div class="form-group">
                  <label for="idnumber">Phone Number</label>
                  <input type="text" class="form-control " name = "buyerphonenumber" value = "" required>
                </div>
                
                <div class="form-group">
                  <label for="idnumber">Email</label>
                  <input type="text" class="form-control " name = "bueremail" value = "" required>
                </div>
                <div class="form-group">
                  <label for="type">Product Ids</label>
                  <input type="text" class="form-control "  name = "buyerproductsids" value = "" required>
                </div>
                
                <div class="form-group">
                  <label for="type">Products</label>
                  <input type="text" class="form-control "  name = "buyerproducts" value = "" required>
                </div>
               
                
                <!--<div class="form-group">
                  <label for="password">City</label>
                  <select class="form-control" id="proCity" name="proCity">
                    <option>San Francisco</option>
                    <option>New York</option>
                    <option>Chicago</option>
                    <option>Denver</option>
                  </select>
                </div>-->
                
                <div class="form-group">
                  <label for="password">Total Cost(USD)</label>
                  <input type="text" class="form-control "  name = "totalcost" value = "" required>
                </div>
                <div class="form-group">
                  <label for="password">Special Instruction (Optional)</label>
                  <input type="text" class="form-control "  name = "shortdescription" value = "" >
                </div>
                
               <!-- <div class="form-group">
                  <label for="password">Company</label>
                  <input type="text" class="form-control "  name = "proCompany" value = "" required>
                </div> -->  <!-- End of Provider info -->
                
              <div class="box-footer">
                <button type="submit" class="btn btn-primary input-lg col-lg-12" >Create New Order</button>
              </div>
            </form>
          </div>
     </div>
</div>


<script type="text/javascript">
    $("#main-form").validate({
        rules: {
            proFirstName: "required",
            proLastName: "required",
            proEmail: "required",
            proPassword: "required",
            proCity: "required",
            proAddress: "required",
            proCompany: "required"
        }
    });
    

</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#proProfileImage_11')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<?php
        if($this->session->flashdata('message')){
        ?>
        <script>
            alert('<?=$this->session->flashdata('message')?>');
        </script>
        <?php
        }
?>  




<!--
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var geocoder = new google.maps.Geocoder();

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus(str) {
  document.getElementById('markerStatus').innerHTML = str;
}

function updateMarkerPosition(latLng) {
  document.getElementById('info').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
}

function updateMarkerAddress(str) {
  document.getElementById('address').innerHTML = str;
}

function initialize() {
  var latLng = new google.maps.LatLng(-34.397, 150.644);
  var map = new google.maps.Map(document.getElementById('mapCanvas'), {
    zoom: 8,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var marker = new google.maps.Marker({
    position: latLng,
    title: 'Point A',
    map: map,
    draggable: true
  });

  // Update current position info.
  updateMarkerPosition(latLng);
  geocodePosition(latLng);

  // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress('Dragging...');
  });

  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerStatus('Dragging...');
    updateMarkerPosition(marker.getPosition());
  });

  google.maps.event.addListener(marker, 'dragend', function() {
    updateMarkerStatus('Drag ended');
    geocodePosition(marker.getPosition());
  });
}

// Onload handler to fire off the app.
google.maps.event.addDomListener(window, 'load', initialize);
</script>
</head>


<body>
  <style>
  #mapCanvas {
    width: 1500px;
    height: 400px;
    float: left;
  }
  #infoPanel {
    float: left;
    margin-left: 10px;
  }
  #infoPanel div {
    margin-bottom: 5px;
  }
  </style>

  <div id="mapCanvas"></div>
  <div id="infoPanel">
    <b>Marker status:</b>
    <div id="markerStatus"><i>Click and drag the marker.</i></div>
    <b>Current position:</b>
    <div id="info"></div>
    <b>Closest matching address:</b>
    <div id="address"></div>
  </div>
</body>
</html> -->