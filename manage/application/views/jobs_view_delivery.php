<!--container title part-->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">  
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>application/views/sub-admin/Ajax_timer_script/get_alarm_script.js"></script>



<div class="row" style = "padding-top: 0px;" style="background-color:#e0ad00;">
    <div id="breadcrumb" class="col-md-12" style="background-color:#e0ad00;">
        <ol>
            <h4><font color="white">Order History by Delivery</font></h4> 
        </ol>
    </div>
</div>
<br>
    
<div class="box box-primary">  
  <div class = "box-body">
    <div class="row-fluid table-responsive">                
   <table class="table table-bordered table-striped table-hover table-heading table-datatable content-fluid" id="datatable-1">
         <thead>
              <tr>
              	  <th id="select_box" style="text-align: center; display: none; ">Select</th>
                  <th style="text-align: center; ">No</th>  
                  <th style="text-align: center; ">Order Id</th>  
                  <th style="text-align: center; ">Buyer Name</th>              
                  <th style="text-align: center; ">Driver Name</th>                  
                  <th style="text-align: center; ">Address</th>     
                   <!-- <th style="text-align: center; ">Product Name</th>           -->
                  <th style="text-align: center; ">Cost</th>    
                  <th style="text-align: center; ">Status</th>              
                 
                  <?php if($this->session->userdata('login_adminID')==1){?>
                      <th style="text-align: center; ">Shop Name</th>
                  <?php } ?>
                   <th style="text-align: center; ">Details</th>
              </tr>
         </thead>
         	
         	
         
          <tbody style="text-align:center;" id="tbodyid">
          	<?php foreach($orderdata as $key=>$val){?>
          		<tr>
          			<th style="text-align: center; "><?php echo $val['No']; ?></th>
          			<th style="text-align: center; "><?php echo $val['order_id']; ?></th>
          			<th style="text-align: center; "><?php echo $val['buyer_name']; ?></th>
          			<th style="text-align: center; "><?php echo $val['driver_name']; ?></th>
          			<th style="text-align: center; "><?php echo $val['buyer_address']; ?></th>
          			<th style="text-align: center; "><?php echo $val['cost']; ?></th>
          			<th style="text-align: center; "><?php echo $val['status']; ?></th>
          			<th style="text-align: center; "><a href="<?php echo $this->config->base_url().'index.php/admin/viewjobdetail/'.$val['order_id']?>">Order Detail&nbsp;<i class="fa fa-commenting" aria-hidden="true"></i></a></th>
          		</tr>
          		
          <?php } ?>		
          </tbody>
     </table>
     <div class="go_submit pull-right" style="display: none;"> 
      <div class="col-lg-4"><input type="text" placeholder="Enter Delivery Id" class="form-control" name="delivery_id" id="delivery_id" value=""></div>
      <div class="col-lg-4"><input type="submit" class="btn btn-primary" value="GO" /></div></div>
    
     
    <!-- <input type="text" placeholder="Enter Delivery Id" class="delivery_id" />
     <button>Go</button>-->
     </div>
     <!--<div class="box-footer">
        <a type="button" class="btn btn-primary col-md-12" href="<?php echo base_url();?>index.php/Admin/EM_addEmployee" >Add New  Employee</a>                   
     </div>-->
     
     </div>
  </div>
</div>



      