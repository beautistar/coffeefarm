<!--<div class="row">           
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">  
        
            <li ><h3>Add New Store</h3></li> 
            
        </ol>
    </div>
</div>-->

<div class="row" style = "padding-top: 0px;" style="background-color:<?php if($this->session->userdata('login_adminID')==1){ echo "#e0ad00"; }else{ echo "#77bb55"; } ?>;">
    <div id="breadcrumb" class="col-md-12" style="background-color:<?php if($this->session->userdata('login_adminID')==1){ echo "#e0ad00"; }else{ echo "#77bb55"; } ?>;">
  
        <ol>
            <h4><font color="white">Order Detail</font></h4> 
        </ol>
    </div>
</div>
<br>



<div class="row container-fluid">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
          <?php echo validation_errors(); ?>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id = "main-form" onsubmit="return confirm('Are you sure to delete this order?');" action = "<?php echo base_url();?>index.php/Admin/deleteorderfromsuperadmin" method="POST" enctype="multipart/form-data">
            
            
            <!--- Products --->
            <br>
             <label for="username"   style="text-align: center; width: 100%; color:red; font-size: 30px;">Products Infomation</label>
            <table  width="100%">
            
            	    <tr  width="100%">
                          <td style="padding: 5px 10px 5px 5px;">
                               <div class="form-group">
                                  <label for="idnumber">Product Ids</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->product_id?>" required>
                                </div>
                          </td>
                   </tr>
                    <tr  width="100%">
                          <td style="padding: 5px 10px 5px 5px;">
                               <div class="form-group">
                                  <label for="idnumber">Products</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->buyer_products ?>" required>
                                </div>
                          </td>
                   </tr>
                   <tr>
                          <td style="padding: 5px 5px 5px 5px;">
                                <div class="form-group">
                                  <label for="idnumber">Cost (USD)</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->buyer_cost ?>" required>
                                </div>
                          </td>
                        
                     </tr>
                     <tr>
                          <td style="padding: 5px 5px 5px 5px;">
                                <div class="form-group">
                                  <label for="idnumber">Short Description</label>
                                  <input type="text" readOnly = true class="form-control " name = "shortdescription" value = "<?= $admin_info[0]->short_description ?>" required>
                                </div>
                          </td>
                        
                     </tr>
                </table>
            
            
                <!-- Provider -->
                <table width="100%">
                    <tr width="100%">
                         <td width="30%">
                             <div class="box-body">
                             <input type="hidden" name = "proid" value = "<?= $store_info[0]->admin_id ?>">
                             <div width="100%" class="form-group">                            
                             <label for="username"   style="text-align: center; width: 100%; color:red; font-size: 30px;">Store Infomation</label>
                            </div>
                  
                            <div class="form-group">
                            
                              <label for="username">Store Name</label>
                              <input type="text" readOnly = true class="form-control " name = "storename" value = "<?= $store_info[0]->admin_storename ?>" required>
                             
                            </div>
                            
                            <div class="form-group">                                                                                                                        
                              <label for="username">Store Address</label>
                              <input type="text" readOnly = true class="form-control " name = "managername" value = "<?= $store_info[0]->admin_address ?>" required>
                            </div>
                            <div class="form-group">
                              <label for="idnumber">Store Phoneumber</label>
                              <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $store_info[0]->admin_phonenumber ?>" required>
                            </div>
                            
                            <div class="form-group">
                              <label for="idnumber">Store Email</label>
                              <input type="text" readOnly = true class="form-control " name = "email" value = "<?= $store_info[0]->admin_email ?>" required>
                            </div>
                        </td>
                        <td width="30%">
                             <div class="box-body">
                             <input type="hidden" name = "proid" value = "<?= $admin_info[0]->order_id ?>">
                             <div width="100%" class="form-group">                            
                             <label for="username"   style="text-align: center; width: 100%; color:red; font-size: 30px;">Buyer Infomation</label>
                            </div>
                  
                            <div class="form-group">
                            
                              <label for="username">Buyer Name</label>
                              <input type="text" readOnly = true class="form-control " name = "storename" value = "<?= $admin_info[0]->buyer_name ?>" required>
                             
                            </div>
                            
                            <div class="form-group">                                                                                                                        
                              <label for="username">Buyer Address</label>
                              <input type="text" readOnly = true class="form-control " name = "managername" value = "<?= $admin_info[0]->buyer_address ?>" required>
                            </div>
                            <div class="form-group">
                              <label for="idnumber">Buyer Phoneumber</label>
                              <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->buyer_phonenumber ?>" required>
                            </div>
                            
                            <div class="form-group">
                              <label for="idnumber">Buyer Email</label>
                              <input type="text" readOnly = true class="form-control " name = "email" value = "<?= $admin_info[0]->buyer_email ?>" required>
                            </div>
                        </td>
                        
                         <td width="30%">
                              <div class="form-group">                            
                                <label for="username"   style="text-align: center; width: 100%; color:red; font-size: 30px;">Delivery Infomation</label>
                             </div>
                              <div class="form-group">
                              <label for="type">Deliver Name</label>
                              <input type="text" readOnly = true class="form-control "  name = "password"  value = "<?= $admin_info[0]->worker_name;?>" >
                            </div>  
                            
                        <?php
                            if($admin_info[0]->worker_id==0){
                        ?>
                              <div class="form-group">                                                                                                                        
                                 <label for="username">Driver Address</label>
                                 <input type="text" readOnly = true class="form-control " name = "managername" value = "" required>
                             </div>
                             <div class="form-group">
                              <label for="idnumber">Driver Phoneumber</label>
                              <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "" required>
                            </div>
                             <div class="form-group">
                              <label for="idnumber">Driver Email</label>
                              <input type="text" readOnly = true class="form-control " name = "email" value = "" required>
                            </div>
                        <?php
                            } else {
                        ?>
                              
                              <div class="form-group">                                                                                                                        
                                  <label for="username">Driver Address</label>
                                  <input type="text" readOnly = true class="form-control " name = "managername" value = "<?=$workerinfo[0]->user_address  ?>" required>
                              </div>
                              <div class="form-group">
                                  <label for="idnumber">Driver Phoneumber</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $workerinfo[0]->user_phonenumber ?>" required>
                            </div>
                             <div class="form-group">
                              <label for="idnumber">Driver Email</label>
                              <input type="text" readOnly = true class="form-control " name = "email" value = "<?= $workerinfo[0]->user_email ?>" required>
                            </div>
                              <?php
                        }
                        ?>
                            
                            
                            
                           
                         </td>
                    </tr>
                     
                </table>
                
                
                
                
                
                
                
                
                                       
                     <label for="username"   style="text-align: center; width: 100%; color:red; font-size: 30px;">Proccessed Date and Time</label>
              
                <table  width="100%">
                    <tr  width="100%">
                          <td style="padding: 5px 10px 5px 5px;">
                               <div class="form-group">
                                  <label for="idnumber">Open Date and Time</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->start_time ?>" required>
                                </div>
                          </td>
                          <td style="padding: 5px 5px 5px 5px;">
                                <div class="form-group">
                                  <label for="idnumber">Accepeted Date and Time</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->accept_time ?>" required>
                                </div>
                          </td>
                          
                          <td style="padding: 5px 5px 5px 5px;">
                               <div class="form-group">
                                  <label for="idnumber">Picked Up Date and Time</label>
                                  <input type="text"  readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->picked_time ?>" required>
                                </div>
                          </td>                          
                          
                          <td style="padding: 5px 5px 5px 5px;">
                                <div class="form-group">
                                  <label for="idnumber">Closed Date and Time</label>
                                  <input type="text" readOnly = true class="form-control " name = "phonenumber" value = "<?= $admin_info[0]->closed_time ?>" required>
                                </div>
                          </td>
                     </tr>
                </table>
                
              <div class="box-footer">
                    <button type="submit" class="btn btn-danger input-lg col-lg-12" >Delete Order</button>
              </div>
                   
            
            
                      
            </form>
          </div>
     </div>
</div>


<!--<script type="text/javascript">
    $("#main-form").validate({
        rules: {
            proFirstName: "required",
            proLastName: "required",
            proEmail: "required",
            proPassword: "required",
            
            proAddress: "required",
            proCompany: "required"
        }
    });
    

</script>-->

<!--<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#proProfileImage_11')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>-->

<?php
        if($this->session->flashdata('message')){
        ?>
        <script>
            alert('<?=$this->session->flashdata('message')?>');
        </script>
        <?php
        }
?>   