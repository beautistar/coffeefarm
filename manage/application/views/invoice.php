<html !DOCTYPE>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
<!--    <link href="<?php echo base_url(); ?>skins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>skins/dist/css/sb-admin-2.css" rel="stylesheet" type="text/css">                              
    <link href="<?php echo base_url(); ?>skins/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
    <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/receipt-style.css">    

</head>
<body>

    <div class="wrapper">
            <div class="container">
                <div class="header text-center">
                    <ul>
                        <li><h2><label class="text-left" ><?= $company_name; ?> 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         Invoice</label></h2></li>
                        
                    </ul>
                    
                </div>
                <form class="formsec">
                    
                    <div class="form-group">
                        <br>
                        <label>82-5966 Mamalahoa Hwy</label>                                                
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Captain Cook, HI 96704 
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                (808)238-5005</label>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="sold">                        
                        <h4>SOLD TO: <?= $sold_to; ?></h4>                        
                    </div>
                    
                    <div class="sold">                        
                        <h4>Invoice Date: <?= $invoice_date; ?></h4>                        
                    </div>
                    <!--<div class="sold">                        
                        <h4>SHIPPED TO: AAAAA</h4>                        
                    </div> -->
                    <div class="sold">                        
                        <h4>Tax: 0.5</h4>                        
                    </div>
                    
                    <br>
                </form>
                 <div class="table-responsive">          
                      <table class="table">
                        <thead>
                          <tr>
                            <th>QUANTITY</th>
                            <th>Description</th>
                            <th>Unit Price</th>
                            <th>Amount</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><?= $total_quantity; ?><br><br><br><br><br><br><br><br><br></td>
                            <td><?= $process_date; ?> WetMill processing (<?= $bag_count; ?> bags, <?= $processed_bag_count; ?> processed)<br>
                                Returned <?= $returned_lbs_count; ?> lbs of parchment<br>
                                <?= $moisture_percent; ?>%moisture<br>
                                <?= $method; ?><br>
                                Recovery <?= $recovery_percent; ?>% <br><br><br><br><br><br><br>
                            </td>
                            <td><?= $unite_price_1; ?></td>
                            <td><?= $amount_1; ?></td>
                          </tr>
                          
                          <tr>
                            <td><?= $burlap_quantity; ?></td>
                            <td>Burlap</td>
                            <td><?= $unite_price_2; ?></td>
                            <td><?= $amount_2; ?></td>
                          </tr>
                          
                          <tr>
                            <td><?= $plastic_bags_quantity; ?></td>
                            <td>Plastic bags</td>
                            <td><?= $unite_price_3; ?></td>
                            <td><?= $amount_3; ?></td>
                          </tr>
                          
                          <tr>
                            <td colspan="2" rowspan="3"></td>
                            <td rowspan="3">                            
                            <b>SUB TOTAL</b><br><br>
                            <b>TAX</b><br><br>
                            <b>FEIGHT</b></td>
                            <td><?= $sub_total; ?></td>
                          </tr>
                          
                          <tr>
                            <td><?= $tax; ?></td>
                          </tr>
                          <tr>
                            <td><?= $feight; ?></td>
                          </tr> 
                          
                          <tr>
                            <td style="border: none;" colspan="3">
                            <div style="width:50%;float:left;">
                                <b>DiRECT ALL INQUIRIES TO:</b>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <b>MAKE ALL CHECKES PAYABLE TO :</b>
                            <p>Jean<p>
                            <p>(808) 238-5005<p>
                            <p> jean@halatreecoffee.com<p>
                            </div>                   
                            </td>
                            <td>
                                <p>PAY THIS AMOUNT</p>
                                <b><?= $pay_this_amount; ?></b>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="thanku">
                        <h3>THANK YOU FOR YOUR BUSINESS!</h3>
                    </div>
            </div>
    </div>
</body>
</html>