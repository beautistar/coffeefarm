<html !DOCTYPE>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
<!--    <link href="<?php echo base_url(); ?>skins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>skins/dist/css/sb-admin-2.css" rel="stylesheet" type="text/css">                              
    <link href="<?php echo base_url(); ?>skins/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
    <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/receipt-style.css">    

</head>
<body>

    <div class="wrapper">
            <div class="container">
                <div class="header text-center">
                    <ul>
                        <li><h2><label class="text-left" ><?= $company_name; ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         Receipt</label></h2></li>
                        
                    </ul>
                    
                </div>
                <form class="formsec">
                    <div class="form-group">
                        <label>82-5966 Mamalahoa Hwy</label>                                                
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Captain Cook, HI 96704 
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                (808)238-5005</label>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="sold">
                        <h4>Received From: <?= $received_from; ?></h4>
                        <!--<h4>Received From: $received_from</h4>-->
                        
                    </div>
                    
                    <div class="sold">
                        <h4>Received Date: <?= $received_date; ?></h4>
                        <!--<h4>Received Date: 2018-05-30</h4>-->
                        
                    </div>
                    
                    <br>
                </form>
                 <div class="table-responsive">          
                      <table class="table">
                        <thead>
                          <tr>
                            <th>QUANTITY</th>
                            <th>Description</th>
                            <th>Unit Price</th>
                            <th>Amount</th>
                          </tr>                             
                        </thead>
                        <tbody>
                          <tr>
                            <td><?= $lbs_quantity; ?><br><br><?= $bag_quantity; ?><br><br><br><br><br><br><br></td>
                            <td>Lbs of cherries<br><br># of bags<br><br><br><br><br><br><br></td>
                            <td></td>
                            <td></td>
                          </tr>                          
                                                    
                          <tr>
                            <td colspan="2" rowspan="3"></td>
                            <td rowspan="3">                            
                            <b>SUB TOTAL</b><br>
                            <b>TAX</b><br>
                            <b>FEIGHT</b></td>
                            <td></td>
                          </tr>
                          
                          <tr>
                            <td></td>
                          </tr>
                          <tr>
                            <td></td>
                          </tr>                          
                          
                          <tr>
                            <td style="border: none;" colspan="3">
                            <div style="width:50%;float:left;">
                                <b>DiRECT ALL INQUIRIES TO:</b>
                                <p>Jean<p>
                                <p>(808) 238-5005<p>
                                <p> jean@halatreecoffee.com<p>
                            </div>                            
                            </td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="thanku">
                        <h3>THANK YOU FOR YOUR BUSINESS!</h3>
                    </div>
            </div>
    </div>
</body>
</html>