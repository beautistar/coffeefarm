<!--container title part-->


<div class="row" style = "padding-top: 0px;" style="background-color:#e0ad00;">
    <div id="breadcrumb" class="col-md-12" style="background-color:#e0ad00;">
        <ol>
            <h4><font color="white">Order History</font></h4> 
        </ol>
    </div>
</div>
<div id="wrapper">
    
<!-- <div id="page-wrapper" class="gray-bg"> -->
<div class="wrapper wrapper-content date_search">
        <div class="row">
            <div class="col-lg-3">
                        <div class="ibox ibox-cutome ibox-cutome-1 float-e-margins">
                            <div class="ibox-content" style="background: transparent;color: #fff;">
                                <h1 class="no-margins" id="open"></h1>
                       
                            </div>
                        <div class="ibox-title" style="background: transparent;border: none;color: #fff;min-height: initial;padding: 0 8px 0 8px;">
                                <h5>Open Orders</h5>
                            </div><div class="view-detail-link"><a onclick="myFunction('open')">View Details <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>    </a></div></div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox ibox-cutome ibox-cutome-3 float-e-margins">
                            <div class="ibox-content" style="background: transparent;color: #fff;">
                                <h1 class="no-margins" id="accept"></h1>
                       
                            </div>
                        <div class="ibox-title" style="background: transparent;border: none;color: #fff;min-height: initial;padding: 0 8px 0 8px;">
                                <h5>Fulfilled Orders</h5>
                            </div><div class="view-detail-link"><a onclick="myFunction('accept')">View Details <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>    </a></div></div>
                    </div>
                    
                    <div class="col-lg-3">
                        <div class="ibox ibox-cutome ibox-cutome-4 float-e-margins">
                            <div class="ibox-content" style="background: transparent;color: #fff;">
                                <h1 class="no-margins" id="pick-up"></h1>
                       
                            </div>
                        <div class="ibox-title" style="background: transparent;border: none;color: #fff;min-height: initial;padding: 0 8px 0 8px;">
                                <h5>Picked Up Orders</h5>
                            </div><div class="view-detail-link"><a onclick="myFunction('pick-up')">View Details <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>    </a></div></div>
                    </div>
                    
                    <div class="col-lg-3">
                        <div class="ibox ibox-cutome ibox-cutome-2 float-e-margins">
                            <div class="ibox-content" style="background: transparent;color: #fff;">
                                <h1 class="no-margins" id="close"></h1>
                       
                            </div>
                        <div class="ibox-title" style="background: transparent;border: none;color: #fff;min-height: initial;padding: 0 8px 0 8px;">
                                <h5>Closed Orders</h5>
                            </div><div class="view-detail-link"><a onclick="myFunction('close')">View Details <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>    </a></div></div>
                    </div>
                    
                    
                    
                    <!-- <div class="col-lg-3">
                        <div class="ibox ibox-cutome ibox-cutome-2 float-e-margins">
                            <div class="ibox-title" style="background: #fff;">
                                <h1 class="no-margins" id="close"></h1>
                                
                            </div>
                            <div class="ibox-content" style="background: #fff;">
                                <h5>Close Orders</h5>
                                <a onclick="myFunction('close')">View Details</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title" style="background: #fff;">
                                <h5>Accept Orders</h5>
                            </div>
                            <div class="ibox-content" style="background: #fff;">
                                <h1 class="no-margins" id="accept"></h1>
                                <a onclick="myFunction('accept')">View Details</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title" style="background: #fff;">
                                <h5>Pick up Orders</h5>
                            </div>
                            <div class="ibox-content" style="background: #fff;">
                                <h1 class="no-margins" id="pick-up"></h1>
                                
                                <a onclick="myFunction('pick-up')">View Details</a>
                            </div>
                        </div>
                    </div> -->
                    
                    <!-- <div class="col-sm-3"> -->
<form action="<?php echo base_url();?>index.php/Admin/search" method="post">
        <div class="form-group col-lg-12">
            <div class="col-lg-4"><div class="input-group date" id="datetimepicker1">
                <input type="text" class="form-control" placeholder="To Date" name="todate" id="todate" value="<?php if($this->session->userdata('date')!==""){ echo "".$this->session->userdata('date'); } ?>">
                <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
                </span>
  </div>
            </div>
            

            <div class="col-lg-4"><input type="text" placeholder="Enter Order Id" class="form-control" name="order_id" id="order_id" value="<?php if($this->session->userdata('order_id')!==""){ echo "".$this->session->userdata('order_id'); } ?>"></div>
               <div class="col-lg-4"><button type="submit" class="btn btn-primary">Search</button></div></div>
           <!-- </div> -->
            
            </form>
        </div>
       
    <!-- </div> -->
</div>
</div>
<div class="box box-primary">  
  <div class = "box-body">
    <div class="row-fluid table-responsive">                
     <table class="table table-bordered table-striped table-hover table-heading table-datatable content-fluid" id="datatable-1">
         <thead>
              <tr>
                  <th style="text-align: center; ">No</th>
                  <th style="text-align: center; ">Order Id</th>
                  <th style="text-align: center; ">Store Name</th>
                  <th style="text-align: center; ">Buyer Name</th>
                  <th style="text-align: center; ">Driver Name</th>
                  <th style="text-align: center; ">Address</th>     
                   <th style="text-align: center; ">Product Name</th>          
                  <th style="text-align: center; ">Cost</th>   
                  <th style="text-align: center; ">Status</th>   
                  <th style="text-align: center; ">Details</th>
              </tr>
         </thead>
        
         <tbody style="text-align:center;" id="tbodyid">
          </tbody>
     </table>
     
     <!--<div class="box-footer">
        <a type="button" class="btn btn-primary col-md-12" href="<?php echo base_url();?>index.php/Admin/EM_addEmployee" >Add New  Employee</a>                   
     </div>-->
     
     </div>
  </div>
</div>



<script type="text/javascript">
    
    function deleteJob(job_id){
        var r;
        r = confirm("Are you sure to delete this Job?");
        
        if (r == true) {     
            
            location.href = "<?php echo base_url();?>"+"index.php/admin/deleteJob/" + job_id;                
        }
    }
</script> 
      
<script language="javascript" type="text/javascript">
   var status="all";
     
   /* updateAlarmCount();*/
//     
    $('#datetimepicker1').datetimepicker({
            sideBySide: true,
            format : 'YYYY/MM/DD',
        });
//setInterval(myFunction, 10000);
var interval=null;
    jQuery(document).ready(function () {
        //status="all";
        setInterval(function(){ myFunction(status); },10000);
        //setInterval(myFunction, 10000);
        //console.log("here");
       //interval= setInterval(myFunction, 10000);
       
        myFunction(status);
        changeCount();
        setInterval(changeCount, 10000);
       // myFunction();
       /* setInterval(function(){ updateAlarmCount(); },3000);*/
    });
    function myFunction(statusa){
        var date=null;
        status=statusa;
        console.log(status);
        var date=document.getElementById('todate').value;
        var orderid=document.getElementById('order_id').value;
        console.log(date);
        console.log(orderid);
        $.ajax({
            type: 'POST',
            data: {id: "",
            status: status,
            date: date,
            orderid: orderid},
            url: "<?php echo base_url();?>index.php/admin/getAlarmsForFamilyIDAdmin",
            dataType: 'json',
            success: function(JSONObject) {
                // setTimeout(function(){myFunction();}, 10000);
                 // clean all table data
                
               
                var admin_id="<?php echo $this->session->userdata('login_adminID'); ?>";
                console.log(admin_id);
                if(admin_id==1){
                    $("#tbodyid").empty();
                     function createTableRow(cells) {
                    var tds = cells.map(function (cellContent) {
                        return '<td>' + cellContent + '</td>';
                    }).join('');
                    return '<tr>' + tds + '</tr>';
                }
                    var alarmsTableRow = function (ignore, index) {
                    return createTableRow([
                            JSONObject[index].No,
                            JSONObject[index].order_id,
                            JSONObject[index].shop_name,
                            JSONObject[index].buyer_name,
                            JSONObject[index].driver_name,
                            JSONObject[index].buyer_address,
                            JSONObject[index].productname,                      
                            JSONObject[index].cost,
                            JSONObject[index].status,
                            //JSONObject[index].shop_name,
                            JSONObject[index].detail,
                            
                        ]);
                    };
                    $('#tbodyid').append(
                    $.map(JSONObject, alarmsTableRow)
                );
                }else{
                    $("#tbodyid").empty();
                     function createTableRow(cells) {
                    
                    var tds = cells.map(function (cellContent) {
                        return '<td>' + cellContent + '</td>';
                    }).join('');
                    return '<tr>' + tds + '</tr>';
                }
                    var alarmsTableRow = function (ignore, index) {
                    return createTableRow([
                        JSONObject[index].No,
                        JSONObject[index].order_id,
                        JSONObject[index].buyer_name,
                        JSONObject[index].driver_name,
                        JSONObject[index].buyer_address,
                        JSONObject[index].productname,                      
                        JSONObject[index].cost,
                        JSONObject[index].status,
                        JSONObject[index].detail,
                        // JSONObject[index].shop_name,
                    ]);
                };
                $('#tbodyid').append(
                    $.map(JSONObject, alarmsTableRow)
                );
                }
                
                
                
            }
        });
    } // end of myFunction
    
    
    function changeCount(){
        $.ajax({
            type: 'POST',
            data: {id: "",
            status: status},
            url: "<?php echo base_url();?>index.php/admin/getcount",
            dataType: 'json',
            success: function(JSONObject) {
                // setTimeout(function(){myFunction();}, 10000);
                console.log(JSONObject);
                if(JSONObject.status_accept!=null){
                    document.getElementById("accept").innerHTML = JSONObject.status_accept.total;
                }else{
                    document.getElementById("accept").innerHTML = "0";
                }
                if(JSONObject.status_open!=null){
                    document.getElementById("open").innerHTML = JSONObject.status_open.total;
                }else{
                    document.getElementById("open").innerHTML = "0";
                }
                if(JSONObject.status_close!=null){
                    document.getElementById("close").innerHTML = JSONObject.status_close.total;
                }else{
                    document.getElementById("close").innerHTML = "0";
                }
                
                if(JSONObject.status_pickup!=null){
                    document.getElementById("pick-up").innerHTML = JSONObject.status_pickup.total;
                }else{
                    document.getElementById("pick-up").innerHTML = "0";
                }
            }
        });
    }
    
   // function getOrderHistoryByStatus(status){
           // $.ajax({
            // type: 'POST',
            // data: {status: status},
            // url: "<?php //echo base_url();?>index.php/admin/getAllJobsDataByStatus",
            // dataType: 'json',
            // success: function(JSONObject) {
                // clearInterval(interval); 
                // // setTimeout(function(){myFunction();}, 10000);
                // $("#tbodyid").empty(); // clean all table data
//                 
                // function createTableRow(cells) {
                    // var tds = cells.map(function (cellContent) {
                        // return '<td>' + cellContent + '</td>';
                    // }).join('');
                    // return '<tr>' + tds + '</tr>';
                // }
//                 
                // var alarmsTableRow = function (ignore, index) {
                    // return createTableRow([
                        // JSONObject[index].No,
                        // JSONObject[index].buyer_name,
                        // JSONObject[index].driver_name,
                        // JSONObject[index].buyer_address,
                        // JSONObject[index].productname,                      
                        // JSONObject[index].cost,
                        // JSONObject[index].status,
                        // JSONObject[index].detail,
                    // ]);
                // };
//                 
                // $('#tbodyid').append(
                    // $.map(JSONObject, alarmsTableRow)
                // );
            // }
        // });
   // }
    
</script>