<!--<div class="row">           
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">  
        
            <li ><h3>Add New Store</h3></li> 
            
        </ol>
    </div>
</div>-->
<div class="row" style = "padding-top: 0px;" style="background-color:<?php if($this->session->userdata('login_adminID')==1){ echo "#e0ad00"; }else{ echo "#77bb55"; } ?>;">
    <div id="breadcrumb" class="col-md-12" style="background-color:<?php if($this->session->userdata('login_adminID')==1){ echo "#e0ad00"; }else{ echo "#77bb55"; } ?>;">
 
        <ol>
            <h4><font color="white">Delivery Profile</font></h4> 
        </ol>
    </div>
</div>
<br>



<div class="row container-fluid">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
          <?php echo validation_errors(); ?>

            <!-- /.box-header -->
            <!-- form start -->
          <!--  <form role="form" id = "main-form"  method="POST" enctype="multipart/form-data">  -->
                <!-- Provider -->
                <div class="box-body">
                   <input type="hidden" name = "proid" value = "">

                <div class="form-group">
                 
                  
                   <?php
                            if($workerinfo[0]->user_photourl){
                        ?>
                        
                            <img class="img-thumbnail" style="margin-top: 1px; margin-bottom: 1px; margin-left: 1px; margin-right: 1px;" onclick="image(this)" src="<?= $workerinfo[0]->user_photourl ?>" />
                      
                        <?php
                            } else {
                        ?>
                       
                            <img class="img-thumbnail" style="margin: 1px;" src="<?php echo base_url();?>skins/images/photo.png" alt="">
                          
                        <?php
                        }
                        ?>
                  
                  
                
                </div>
                
                <div class="form-group">
                  <label for="username">First Name</label>
                  <input type="text" class="form-control " name = "storename" value = "<?= $workerinfo[0]->user_firstname ?>" readonly>
                </div>
                
                <div class="form-group">                                                                                                                        
                  <label for="username">Last Name</label>
                  <input type="text" class="form-control " name = "managername" value = "<?= $workerinfo[0]->user_lastname ?>" readonly>
                </div>
                <div class="form-group">
                  <label for="idnumber">Phone Number</label>
                  <input type="text" class="form-control " name = "phonenumber" value = "<?= $workerinfo[0]->user_phonenumber ?>" readonly>
                </div>
                
                <div class="form-group">
                  <label for="idnumber">Email</label>
                  <input type="text" class="form-control " name = "email" value = "<?= $workerinfo[0]->user_email ?>" readonly>
                </div>
                
                <div class="form-group">
                  <label for="type">Password</label>
                  <input type="text" class="form-control "  name = "password" value = "<?= $workerinfo[0]->user_password ?>" readonly>
                </div>
                
                <!--<div class="form-group">
                  <label for="password">City</label>
                  <select class="form-control" id="proCity" name="proCity">
                    <option>San Francisco</option>
                    <option>New York</option>
                    <option>Chicago</option>
                    <option>Denver</option>
                  </select>
                </div>-->
                
                <div class="form-group">
                  <label for="password">Address</label>
                  <input type="text" class="form-control "  name = "address" value = "<?= $workerinfo[0]->user_address ?>" readonly>
                </div>
                
                <div class="form-group">
                  <label for="password">Driver's License No</label>
                  <input type="text" class="form-control "  name = "address" value = "<?= $workerinfo[0]->licenseno ?>" readonly>
                </div>
                <div class="form-group">
                  <label for="password">Issued By State</label>
                  <input type="text" class="form-control "  name = "address" value = "<?= $workerinfo[0]->issuedbystate ?>" readonly>
                </div>
                <div class="form-group">
                  <label for="password">Expiry Date</label>
                  <input type="text" class="form-control "  name = "address" value = "<?= $workerinfo[0]->expirydate ?>" readonly>
                </div>
                <div class="form-group">
                  <label for="password">DOB</label>
                  <input type="text" class="form-control "  name = "address" value = "<?= $workerinfo[0]->dob ?>" readonly>
                </div>
                <div class="form-group">
                  <label for="password">Car Insured By:</label>
                  <input type="text" class="form-control "  name = "address" value = "<?= $workerinfo[0]->carinsuredby ?>" readonly>
                </div>
                <div class="form-group">
                  <label for="password">Car Expiry Date:</label>
                  <input type="text" class="form-control "  name = "address" value = "<?= $workerinfo[0]->carexpireydate ?>" readonly>
                </div>
                
                <div class="form-group">
                
                    <?php
                        if($workerinfo[0]->typesomethinghere==1){
                    ?>                          
                        <img class="img-thumbnail" style="margin: 1px;" src="<?php echo base_url();?>skins/images/checkbox1.png" alt="">
                        <label>Type something here... Consent to National Criminal Background check (includes criminal background check, sex offender check)</label>
                  
                    <?php
                        } else {
                    ?>                         
                        <img class="img-thumbnail" style="margin: 1px;" src="<?php echo base_url();?>skins/images/empty.png" alt="">  
                        <label>test</label>                          
                    <?php
                    }
                    ?>
                </div>
                <div class="form-group">
                    <?php
                        if($workerinfo[0]->consent==1){
                    ?>                          
                        <img class="img-thumbnail" style="margin: 1px;" src="<?php echo base_url();?>skins/images/checkbox1.png" alt="">
                        <label>Consent to obtain Motor Vehicle Record Consent to drug test</label>
                  
                    <?php
                        } else {
                    ?>                         
                        <img class="img-thumbnail" style="margin: 1px;" src="<?php echo base_url();?>skins/images/empty.png" alt="">  
                        <label>Consent to obtain Motor Vehicle Record Consent to drug test</label>                          
                    <?php
                    }
                    ?>
                </div>
              
                
               <!-- <div class="form-group">
                  <label for="password">Company</label>
                  <input type="text" class="form-control "  name = "proCompany" value = "" required>
                </div> -->  <!-- End of Provider info -->
                
                  <?php
                            if($workerinfo[0]->user_deletestatus==0){
                        ?>
                        <div class="box-footer">
                            <button type=warning class="btn btn-primary input-lg col-lg-12" onclick="updateApprove(<?= $workerinfo[0]->user_id ?>)" >Approve User</button>
                        </div>
                        <div class="box-footer">
                            <button type=reset class="btn btn-primary input-lg col-lg-12" onclick="updateReject(<?= $workerinfo[0]->user_id ?>)">Reject User</button>
                        </div>
                    <?php
                        } else {
                    ?>
                    <div class="box-footer">
                        <button type=submit class="btn btn-primary input-lg col-lg-12" onclick="deleteUserfromadmin(<?= $workerinfo[0]->user_id ?>)" >Delete User</button>
                     </div>
                     <?php
                        }
                    ?>
                    
                    
              
                
              
            <!--</form> -->
          </div>
     </div>
</div>


<script type="text/javascript">
    $("#main-form").validate({
        rules: {
            proFirstName: "required",
            proLastName: "required",
            proEmail: "required",
            proPassword: "required",
            proCity: "required",
            proAddress: "required",
            proCompany: "required"
        }
    });
           
   

</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#proProfileImage_11')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<?php
        if($this->session->flashdata('message')){
        ?>
        <script>
            alert('<?=$this->session->flashdata('message')?>');
        </script>
        <?php
        }
?>   

<script type="text/javascript">
    function image(img) {
        var src = img.src;
        window.open(src, "width=200,height=100");
    }

    function updateApprove(user_id){   
      var r;
        r = confirm("Are you sure to approve this user?");
        
        if (r == true) {     
            
            location.href = "<?php echo base_url();?>"+"index.php/admin/approveUser/" + user_id;    
        }            
       
    }
    
    function updateReject(user_id){
        var r;
        r = confirm("Are you sure to reject this user?");
        
        if (r == true) {     
            
            location.href = "<?php echo base_url();?>"+"index.php/admin/rejectUser/" + user_id ;                
        }
    }
    
    function deleteUserfromadmin(user_id){
      var r;
        r = confirm("Are you sure to delete this user?");
        
        if (r == true) {     
            
            location.href = "<?php echo base_url();?>"+"index.php/admin/deleteuserfromAdmin/" + user_id ;                
        }
    }
    
    
</script>