<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{
    
    function __construct(){
        
        parent::__construct();
		
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->helper('url');      
        $this->load->library('session');
        $this->load->database();
        $this->load->model('admin_model'); 
        date_default_timezone_set('America/Chicago'); 
        
    }
    
    private function doUrlDecode($p_text){
         
        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = trim($p_text);
        return $p_text; 
     }
    
    function createKey(){        
        $time = explode(' ', microtime());
        $NameTime = substr($time[0], 2, 5);        
        $NameRand = mt_rand(0, 0xffff);
        $key = $NameRand.$NameTime; 
        $key = $key * 10 / 10;
        $len = strlen($key);        
        if($len < 16){
            for ($i = 0; $i < 16-$len; $i++)
              $key .= mt_rand(0,9);
        }
        //echo $key."-----".strlen($key);
        return $key;
    }
    
    
    function sessionCheck(){
      
        if (!$this->session->userdata('is_login')){
            redirect("/Admin/index");
        }
    }    
    
    function index(){
        //$this->load->view('login_view');
        $this->load->view('invoice');
    }
    
    function logout(){
        
        $this->session->sess_destroy();
        redirect('/Admin/index');        
    }
    
/*
* Joe Mow Admin controller - 77.104.168.141
*/ 

// login confirm called by login_view form
    function loginConfirm() {
    
        $admin_email    = $this->input->post('admin_email');
        $admin_password = $this->input->post('admin_password');
       
        $adminData = $this->db->where('admin_email', $admin_email)->where('admin_deletestatus',0)->get('tb_admin')->row();
      
        if (!empty($adminData)) { // if there is result, then compare password
         
        
            if ($admin_password == $adminData->admin_password) { // if the tmpPwd == admin password
                // register adminID into Session variable
                $this->session->set_userdata(array('is_login' => true, 'login_adminID' => $adminData->admin_id));
               	
				
                if($adminData->admin_id == 1){            
                redirect('/admin/main');
                   // redirect('/admin/store_main');
                }else{
                    
                    redirect('/admin/store_main');
                }
                return;
            } else { // wrong password entered
                $this->session->set_flashdata('message',"Wrong password. Please try again.");
                redirect('/Admin/index');
            }    
        } else { // no such email
            $this->session->set_flashdata('message',"No such Email.");
            redirect('/Admin/index');
        }
        
    } //end of loginConfirm
     function forgot(){
            $this->load->view('forgotpassword');
    }
    
    function sendverifycode(){
        
         $email    = $this->input->post('admin_email');
        
         $adminData = $this->db->where('admin_email', $email)->get('tb_admin')->row();
          if (!empty($adminData)) { // if there is result, then compare password
               if($adminData->admin_deletestatus==0){
                      $this->sendforgotverifycode($email);
               } else{
                   echo "This account was be removed by Admin." ;
                   exit;
               }
          } else{
              echo "This is invalid mail.";
              exit; 
          }               
    }
    
    function sendforgotverifycode($email){
        $verificationcode=$this->generateRandomString();  
                
        $this->db->where('admin_email',$email);
        $this->db->set('admin_forgotcode',$verificationcode); 
        $this->db->update('tb_admin'); 
        
        $subject="From BOLTR.com" ;
        $content = "Your verification code is   ".$verificationcode;      
         
        $from = "BOLTR.com";           
        $hearders = "Mime-Version:1.0";
        $hearders .= "Content-Type : text/html;charset=UTF-8";
        $hearders .= "From: " . $from;
        
        mail($email, $subject, $content, $hearders);  
        
        $this->load->view('forgot_verifyview');
    }
    
    function generateRandomString() {
        $length = 5;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    function verifyConfirm(){
        $email  = $this->input->post('admin_email');
        $password= $this->input->post('admin_password');
        $verificationcode=$this->input->post('admin_verificationcode');
        
          $adminData = $this->db->where('admin_email', $email)->get('tb_admin')->row();
          if (!empty($adminData)) { // if there is result, then compare password
               if($adminData->admin_deletestatus==0){
                      if($adminData->admin_forgotcode == $verificationcode){
                           $this->db->where('admin_email',$email);
                            $this->db->set('admin_password',$password); 
                            $this->db->update('tb_admin'); 
                            
                            $this->index();                          
                      } else {
                          echo "Invalid Verification code";
                      }
               } else{
                   echo "This account was be removed by Admin." ;
                   exit;
               }
          } else{
              echo "This is invalid mail.";
              exit; 
          }    
        
        
        
    }

    /*
    * Jobs
    */
    // to display all jobs data after login success
    function main(){        
        
        $this->sessionCheck();
        $storeid = $this->session->userdata('login_adminID');  
        $order_id=$this->session->userdata("order_id");
      
        $date=$this->session->userdata("date");
      	$stauts="all";
        
        $jobs_data = $this->admin_model->getAllJobsDataofstoreadmin($stauts,$date,$order_id); // get all jobs data
        
        $this->load->view('main_header_view');
        $this->load->view('main_container_view');
        $this->load->view('jobs_view', array('jobs_data'=>$jobs_data));
        $this->load->view('main_footer_view');
    }   
    
    function deleteJob($job_id){
        
        $this->db->where('job_id', $job_id);
        $this->db->delete('tb_jobs');
        
        redirect('/admin/main');
    }
    
    /*
    * Clients
    */
   
    
    
    
    /*
    * Mower Part
    */
    
    
   
    
    // update mower status from tb_user table
    function updateMowerAvailable($user_id, $user_available){

        $tmp_available = $this->db->where('user_id', $user_id)->get('tb_user')->row_array()['user_available'];
        
        $this->db->set('user_available', $user_available);
       
        $this->db->where('user_id',$user_id);
        $this->db->update('tb_user');
        
        // to send push notification to user
        if ($tmp_available == 0 && $user_available == 1){
            $this->sendPushNotification($user_id);    
        }
        
        
        redirect('/admin/mowers');
    }
    
   
   
    
    
    function sendPushNotification($shopname){

        $user_data = $this->db->where('user_deletestatus', 2)->get('tb_user');
        $usersnumrow=$user_data->num_rows();
         // echo ($usersnumrow);
        foreach ($user_data->result() as $user) {
        
            $token = $user->token;   
           // echo ($token);           
           
            
            $message_title = "New Alert";//$title;  
            $message_content = "Created New Order by ".$shopname;
            
            $url = "https://fcm.googleapis.com/fcm/send";
    //        api_key available in google-services.json

            $api_key = "AIzaSyCfEgr0jMv0X1kNoHphkUR8xP0HLM_uido";

            $notification["to"] = $token;
            $notification["priority"] = "high" ;
                           
            $notification['notification'] = array(
                                    "body" => $message_content,
                                    "title" => $message_title,
                                    "sound" => "notisound.mp3",
                                    
                                    );

            $headers = array(
                            'Content-Type:application/json',
                            'Authorization:key='.$api_key
                            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($notification));

            $result = curl_exec($ch);

            if ($result === FALSE) {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);
        }
        return;
    }
    
    
    //========================= New Scope ======================================
    ///////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
     function addstore(){
         $this->sessionCheck(); 
        
        $this->load->view('main_header_view');
        $this->load->view('main_container_view');
        $this->load->view('addStore_view');
        $this->load->view('main_footer_view');
    }
    
    function addProvider(){
        
        $this->sessionCheck();   
        $this->load->helper('url');   
       
            
        $config['upload_path'] = './uploadfiles/images/';  //upload directory 
        $config['allowed_types'] = 'gif|jpg|png';           
        $config['max_size'] = '1024000';                         
           
        $this->upload->initialize($config);//important part!!!
        $this->load->library('upload', $config);
        $data0 =  $this->input->post('proProfileImage');
        $storename = $this->input->post('storename');
        $managername = $this->input->post('managername');
        $phonenumber = $this->input->post('phonenumber');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $address = $this->input->post('address');        
        
         $adminData = $this->db->where('admin_email', $email)->get('tb_admin')->row();      
        
        if ($adminData > 0) { // if there is result, then compare email
    
            $this->session->set_flashdata('message',"Email already exist.");    
            redirect('/Admin/addstore');
        } else { // no such email
        
             $adminData1 = $this->db->where('admin_phonenumber', $phonenumber)->get('tb_admin')->row();    
             if ($adminData1 > 0) { // if there is result, then compare phone number        
                    $this->session->set_flashdata('message',"Phone Number already exist.");    
                    redirect('/Admin/addstore');
                } else { 
                     
                    if(!($this->upload->do_upload('proProfileImage'))){               
                     
                        $error = $this->upload->display_errors();
                        redirect('/Admin/main');
                                
                    } else {
                    
                        $data = array('upload_data'=>$this->upload->data());

                       
                        
                        $upload_path = "uploadfiles/images/";        
                        $upload_url = base_url()."uploadfiles/images/";   
                        // Upload file                
                        $image_name = $managername."_photo";            
                        $w_uploadConfig = array(
                                'upload_path' => $upload_path,
                                'upload_url' => $upload_url,
                                'allowed_types' => "*",
                                'overwrite' => TRUE,
                                'max_size' => 10000000,
                                'max_width' => 4000,
                                'max_height' => 3000,
                                'file_name' => $image_name.intval(microtime(true) * 10)
                            );       
                        $this->load->library('upload', $w_uploadConfig);   
                                 
                        if ($this->upload->do_upload('proProfileImage')) {                
                             $profile_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
                            //result['photo_url']  = $photo_url;                  
                        } else {
                           //  $this->doRespond(105, $result);
                            $profile_url = base_url().'uploadfiles/provider/'.$data['upload_data']['file_name'];
                             //turn;
                        }
                        
                        $locationObject = $this->getAddress($address); 
		        $object1 = $locationObject->results;
		        if (sizeof($object1 ) > 0) {
		                $coordinates = $object1[0]->geometry->location;                       
                    
	                        $this->admin_model->addProvider(array(
	                            
	                            'admin_photourl'    =>$profile_url,
	                            'admin_storename'   =>$storename, 
	                            'admin_managername' =>$managername,
	                            'admin_email'        =>$email,
	                            'admin_phonenumber'  =>$phonenumber,
	                            'admin_password'     =>$password,
	                            'admin_address'      =>$address,
	                            'latitude'           =>$coordinates->lat,
	                            'longitude'          =>$coordinates->lng 
	                           
	                        ));
	                        $content="Congratlations!
	                        Your BOLTR.com account was be created." ;
	                        $this->sendEmail($email, $content);

                       		 redirect('Admin/addstore');
                        }else{
                     	   echo ("Incorrect Address");
                        }
                    }
                }
        }     
      
        
    }
    
    

  // to display clients out of tb_users table
    function clients(){        
         
        $this->sessionCheck();
                    
        $clients_data = $this->admin_model->getStoremanagerDataByType(1); // get all clients-user_type==1
        
        $this->load->view('main_header_view');
        $this->load->view('main_container_view');
        $this->load->view('clients_view', array('clients_data'=>$clients_data));
        $this->load->view('main_footer_view');
    }
    
    function deleteClient($user_id){
        $this->db->where('admin_id', $user_id);
        $this->db->delete('tb_admin'); 
        
        $this->db->where('store_id', $user_id);
        $this->db->delete('tb_orderhistory');
               
        redirect('/admin/clients');
    }
    
    // to display mower workers out of tb_users table
    function deliveries(){        
         
        $this->sessionCheck();
                    
        $mowers_data = $this->admin_model->getDriversData(); // get all mowers-user_type==2
        
        $this->load->view('main_header_view');
        $this->load->view('main_container_view');
        $this->load->view('mowers_view', array('mowers_data'=>$mowers_data));
        $this->load->view('main_footer_view');
    }
    function pendingdeliveries(){        
         
        $this->sessionCheck();
                    
        $mowers_data = $this->admin_model->getDriversData1(); // get all mowers-user_type==0
        
        $this->load->view('main_header_view');
        $this->load->view('main_container_view');
        $this->load->view('pendingdelivery_view', array('mowers_data'=>$mowers_data));
        $this->load->view('main_footer_view');
    }
     function deleteDelivery($user_id){
        
        $this->db->where('user_id', $user_id);
        $this->db->delete('tb_user');
        
        redirect('/admin/deliveries');
    }
    
    //================== Sub admin side =======================
    
     function store_main(){    
        $this->sessionCheck();
       	
       	//print_r($_POST);
	    /* $jobs_data = $this->admin_model->getAllJobsDataofstore($this->session->userdata('login_adminID')); // get all jobs data
        $this->load->view('main_header_view');
        $this->load->view('main_container_view_store');
        $this->load->view('jobs_view_storeside', array('jobs_data'=>$jobs_data));
        $this->load->view('main_footer_view');*/
       	
       	if($this->session->userdata("count")==0){
       		$this->session->set_userdata("count",1);
       	}else{
       		$this->session->unset_userdata("date");
			$this->session->unset_userdata("order_id");
			$this->session->unset_userdata("delivery_id");
       	}
        $this->load->view('main_header_view');
        $this->load->view('main_container_view_store');
        $this->load->view('jobs_view_storeside');       
        $this->load->view('main_footer_view');
         
     }
    
    
    
    function getcount(){
        $storeid = $this->session->userdata('login_adminID');
        $result_open = $this->admin_model->getAllOpenOrders($storeid,"open");
        $result_close= $this->admin_model->getAllOpenOrders($storeid,"close");
        $result_accept= $this->admin_model->getAllOpenOrders($storeid,"accept");
        $result_pickup= $this->admin_model->getAllOpenOrders($storeid,"pick-up");
		$result_orderready= $this->admin_model->getAllOpenOrders($storeid,"orderready");
        
        $data['status_open']=$result_open;
        $data['status_close']=$result_close;
        $data['status_accept']=$result_accept;
        $data['status_pickup']=$result_pickup;
		$data['status_orderready']=$result_orderready;
        
        $someJSON = json_encode($data);
        echo $someJSON;
     }
  
   function getAlarmsForFamilyID(){    
        $family_id = $_POST['id'];
        $status=$this->input->post('status');   
        $storeid = $this->session->userdata('login_adminID');  
        $date=$this->input->post('date'); 
        $orderid=$this->input->post('orderid'); 
		$deliveryid=$this->input->post('deliveryid');             
        $result = $this->admin_model->getAllJobsDataofstore($storeid,$status,$date,$orderid,$deliveryid);
		
        $someJSON = json_encode($result);
        echo $someJSON;
    }
     function getAlarmsForFamilyIDAdmin(){    
        $family_id = $_POST['id'];
        $status=$this->input->post('status');   
        $storeid = $this->session->userdata('login_adminID');  
        $date=$this->input->post('date');
        $orderid=$this->input->post('orderid');              
        $result = $this->admin_model->getAllJobsDataofstoreadmin($status,$date,$orderid);
        $someJSON = json_encode($result);
        echo $someJSON;
    }
    function viewjobdetail($orderid){       
         $this->sessionCheck(); 
        $orderdetaildata = $this->admin_model->getOrderinfo($orderid); // get all clients-user_type==1   
        
        $this->db->where("order_id", $orderid);
        $driverid=$this->db->get("tb_orderhistory")->result()[0]->worker_id;         
        $workerdetaildata=$this->admin_model->getWorkerinfo($driverid);
 		      
      	$storeid = $this->session->userdata('login_adminID'); 
        $employees_store=$this->admin_model->getEmployeesByStoreId($storeid);
        $employee_name=$this->admin_model->get_employee_name_by_employee_id($orderdetaildata[0]->employee_id);
        $this->load->view('main_header_view');
        $this->load->view('main_container_view_store');
        $this->load->view('orderdetail_storeside', array('admin_info'=>$orderdetaildata, 'workerinfo'=>$workerdetaildata,'employees_store'=>$employees_store,'employee_name'=>$employee_name));
        $this->load->view('main_footer_view'); 
    }
	
	public function assignEmployee(){
		$employee_id=$this->input->post('employee_id');
		$order_id=$this->input->post('order_id');
		$storeid = $this->session->userdata('login_adminID'); 
		$result=$this->admin_model->assign_employee($employee_id,$order_id,$storeid);
		if($result){
			echo json_encode($result);
		}else{
			$result='no';
			echo json_encode($result);
		}
	}
	
	function changeStatus(){
		$order_id=$this->input->post('order_id');
		$result=$this->admin_model->change_status_by_order_id($order_id);
		if($result){
			echo json_encode($result);
		}else{
			$result='no';
			echo json_encode($result);
		}
	}
    
	function viewdeliverydetail($delivery_id){
		       
        $this->sessionCheck(); 
        $storeid = $this->session->userdata('login_adminID');
        $orderdata = $this->admin_model->getAllJobsDataofstore($storeid,"all","","",$delivery_id); // get all clients-user_type==1   
       
        
        ///$driverid=$this->db->get("tb_orderhistory")->result()[0]->worker_id;         
        //$workerdetaildata=$this->admin_model->getWorkerinfo($driverid);
       
      
        $data=array(
        'orderdata'=>$orderdata
		);
                  
        $this->load->view('main_header_view');
        $this->load->view('main_container_view_store');
        $this->load->view('jobs_view_delivery',$data);       
        $this->load->view('main_footer_view');
    }
	
     function viewjobdetailsuperadminside($orderid){       
         $this->sessionCheck(); 
        $orderdetaildata = $this->admin_model->getOrderinfo($orderid); // get all clients-user_type==1  
        
        $this->db->where("order_id", $orderid);
        $driverid=$this->db->get("tb_orderhistory")->result()[0]->worker_id;
        $this->db->where("order_id", $orderid);
        $storeid= $this->db->get("tb_orderhistory")->result()[0]->store_id;
       
        $workerdetaildata=$this->admin_model->getWorkerinfo($driverid);
        $storedetaildata= $this->admin_model->getStoreinfo($storeid);
       
      
        
                  
        $this->load->view('main_header_view');
        $this->load->view('main_container_view');
        $this->load->view('orderdetail', array('admin_info'=>$orderdetaildata, 'workerinfo'=>$workerdetaildata, 'store_info'=>$storedetaildata));
        $this->load->view('main_footer_view'); 
    }
    
    function deleteorder(){
        $orderid = $this->input->post('proid');
        $this->db->where('order_id', $orderid);
        $this->db->delete('tb_orderhistory');
         redirect('Admin/store_main'); 
        
        
    }
    
    function deleteorderfromsuperadmin(){
        $orderid = $this->input->post('proid');
        $this->db->where('order_id', $orderid);
        $this->db->delete('tb_orderhistory');
        redirect('Admin/main'); 
    }
    
   
    
    
    
    function editstoreprofile(){
        $this->sessionCheck();                    
        $loginadminid = $this->session->userdata('login_adminID');
        $admindata = $this->admin_model->getAdmininfo($loginadminid); // get all clients-user_type==1             
        $employees_store=$this->admin_model->getEmployeesByStoreId($loginadminid);
		// $index=0;
		// foreach ($employees_store as $key) {
			// $employee_record=$this->admin_model->getEmployeesNamesByIds($key->user_id);
			// if(!empty($employee_record[0])){
				// $employees_store[$index]->employee_first_name=$employee_record[0]->user_firstname;
				// $employees_store[$index]->employee_last_name=$employee_record[0]->user_lastname;
				// $employees_store[$index]->employee_id=$employee_record[0]->user_id;
			// }
			// $index++;
		// }
        $this->load->view('main_header_view');
        $this->load->view('main_container_view_store');
        $this->load->view('edit_storeprofile', array('admin_info'=>$admindata,'employees_store'=>$employees_store));
        $this->load->view('main_footer_view'); 
    }
    
	
	public function validatePassword(){
		$admin_id=$this->input->post('admin_id');
		$password=$this->input->post('password');
		
		$result=$this->admin_model->validate_password($admin_id,$password);
		echo json_encode($result);
		
	}
	
	public function removeEmployee(){
		$employee_id=$this->input->get('id');
		$loginadminid = $this->session->userdata('login_adminID');//this is also store id
		
		$result=$this->admin_model->remove_employee_by_store_id($employee_id,$loginadminid);
		if($result){
			redirect('Admin/editstoreprofile');
		}
	}
	
	public function addEmployeeToStore(){
		$employee_name=$this->input->get('employee_name');
		$loginadminid = $this->session->userdata('login_adminID');//this is also store id
		
		$result=$this->admin_model->add_store_employee($loginadminid,$employee_name);
		//echo json_encode($employee_name);
		if($result){
			echo json_encode($result);
			redirect('Admin/editstoreprofile');
		}else{
			$result='no';
			echo json_encode($result);
			redirect('Admin/editstoreprofile');
		}
		
	}
	
    function updateStore(){
        $this->sessionCheck();   
        $this->load->helper('url');   
       
            
        $config['upload_path'] = './uploadfiles/images/';  //upload directory 
        $config['allowed_types'] = 'gif|jpg|png';           
        $config['max_size'] = '1024000';                         
           
        $this->upload->initialize($config);//important part!!!
        $this->load->library('upload', $config);
        $data0 =  $this->input->post('proProfileImage');
        $storename = $this->input->post('storename');
        $managername = $this->input->post('managername');
        $phonenumber = $this->input->post('phonenumber');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $address = $this->input->post('address');        
                   
        if(!($this->upload->do_upload('proProfileImage'))){ 
        
              $locationObject = $this->getAddress($address); 
              $object1 = $locationObject->results;
	     if (sizeof($object1 ) > 0) {
		      $coordinates = $object1[0]->geometry->location;   

	             $this->admin_model->updateAdmin1(array(
	                'admin_id'          => $this->session->userdata('login_adminID'),                
	              
	                'admin_storename'   =>$storename, 
	                'admin_managername' =>$managername,
	                'admin_email'        =>$email,
	                'admin_phonenumber'  =>$phonenumber,
	                'admin_password'     =>$password,
	                'admin_address'      =>$address,
	                'latitude'           =>$coordinates->lat,
		        'longitude'          =>$coordinates->lng 
	               
	            ));

              		redirect('Admin/editstoreprofile');     
            }else{
              echo("Incorrect Address");
            }            
        } else {
        
            $data = array('upload_data'=>$this->upload->data());

           
            
            $upload_path = "uploadfiles/images/";        
            $upload_url = base_url()."uploadfiles/images/";   
            // Upload file                
            $image_name = $username."_photo";            
            $w_uploadConfig = array(
                    'upload_path' => $upload_path,
                    'upload_url' => $upload_url,
                    'allowed_types' => "*",
                    'overwrite' => TRUE,
                    'max_size' => 10000000,
                    'max_width' => 4000,
                    'max_height' => 3000,
                    'file_name' => $image_name.intval(microtime(true) * 10)
                );       
            $this->load->library('upload', $w_uploadConfig);   
                     
            if ($this->upload->do_upload('proProfileImage')) {                
                 $profile_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
                //result['photo_url']  = $photo_url;                  
            } else {
               //  $this->doRespond(105, $result);
                $profile_url = base_url().'uploadfiles/provider/'.$data['upload_data']['file_name'];
                 //turn;
            }
            
             $locationObject = $this->getAddress($address); 
	     $object1 = $locationObject->results;
             if (sizeof($object1 ) > 0) {
		    $coordinates = $object1[0]->geometry->location;  
            
        
	            $this->admin_model->updateAdmin(array(
	                'admin_id'          => $this->session->userdata('login_adminID'),                 
	                'admin_photourl'    =>$profile_url,
	                'admin_storename'   =>$storename, 
	                'admin_managername' =>$managername,
	                'admin_email'        =>$email,
	                'admin_phonenumber'  =>$phonenumber,
	                'admin_password'     =>$password,
	                'admin_address'      =>$address,
	                'latitude'           =>$coordinates->lat,
	                'longitude'          =>$coordinates->lng 
	               
	            ));
	            redirect('Admin/editstoreprofile');
            }else{
                echo("Incorrect Address");
            }
        }     
        
    }
    
    
    function createneworder(){
        $this->sessionCheck();   
        $this->load->view('main_header_view');
        $this->load->view('main_container_view_store');
        $this->load->view('create_neworder');
        $this->load->view('main_footer_view');        
    }
    
    function createNeworderredirect(){
    	
        $this->sessionCheck();   
        $this->load->helper('url'); 
        
        $buyername = $this->input->post('buyername');
        $address = $this->input->post('address');
        $buyerphonenumber = $this->input->post('buyerphonenumber');
        $bueremail = $this->input->post('bueremail');
        $buyerproductsid = $this->input->post('buyerproductsids');
        $buyerproducts = $this->input->post('buyerproducts');
        $totalcost = $this->input->post('totalcost');
        $shortdescription= $this->input->post('shortdescription'); 
          
        $locationObject = $this->getAddress($address); 
        $object1 = $locationObject->results;
        if (sizeof($object1 ) > 0) {
                $coordinates = $object1[0]->geometry->location;
              //  print_r(coordinates);
	        $this->load->helper('date');
	
	       // $format = "%Y-%m-%d %H:%i";
	       // $currenttime= @mdate($format);  
	       
	        $cur_time = time();
	         
	         $dateY = date("Y", $cur_time);
	         $dateM = date("m", $cur_time);
	         $dateD=date("d", $cur_time);
	         $timehour=date("H", $cur_time);
	         $timeminute=date("i", $cur_time);
	         $currenttime=$dateY."-".$dateM."-".$dateD." ".$timehour.":".$timeminute;    
	                    
	        $storename= $this->admin_model->createneworder(array( 
	            'store_id'        =>$this->session->userdata('login_adminID'),          
	            'buyername'        =>$buyername,
	            'address'          =>$address, 
	            'buyerphonenumber' =>$buyerphonenumber,
	            'bueremail'        =>$bueremail,
	            'product_id'       => $buyerproductsid,
	            'buyerproducts'    =>$buyerproducts,
	            'totalcost'        =>$totalcost,
	            'createdtime'      =>$currenttime,
	            'short_description'=>$shortdescription,
	            'latitude'         => $coordinates->lat,
	            'longitude'        => $coordinates->lng,
	            'status'=> 0,
	            //'latitude'         => "123",
	            //'longitude'        => "456"
	           
	        ));
	        
	       // echo ($storename);
	        
	       // $this->sendPushNotification($storename);
	
	        redirect('Admin/createneworder');
        
        }else{
           echo ("Incorrect Address");        
        }      
            
        
    }
    
    
    
    function getAddress($address){
    
        $address = str_replace(", ","+",$address);
	$address = str_replace(" ","+",$address);
	$url="https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyCKY6_Nrez6G7bBFxbMY0o_fpPmnp2fUGE";        
	$json = file_get_contents($url);
        $data=json_decode($json);    
        
        //print_r($data);
        
        return $data; 
     }
    
    
    
    
    function gotoDetailpage($workerid){   
        $deliverydetaildata= $this->admin_model->getWorkerinfo($workerid);  
                  
        $this->load->view('main_header_view');
        $this->load->view('main_container_view');
        $this->load->view('viewdelivery_profile', array('workerinfo'=>$deliverydetaildata));
        $this->load->view('main_footer_view'); 
        
    }
    
    function approveUser($userid){
          // echo ($userid."  "."approve user");
           $this->db->select('*')
                 ->from('tb_user')
                 ->where('user_id', $userid);
           $email=$this->db->get()->result()[0]->user_email ;
          $content="Congratelations !
          Your BOLTR.com account was be approved." ;
        //  echo($email);
          
          $this->sendEmail($email, $content);
           $this->db->where('user_id', $userid);   
         $this->db->set('user_deletestatus',2);
         $this->db->update('tb_user');
          redirect('Admin/pendingdeliveries') ;
    }
    
    function rejectUser ($userid){
         $this->db->select('*')
                 ->from('tb_user')
                 ->where('user_id', $userid);
           $email=$this->db->get()->result()[0]->user_email ;
          $content="Your BOLTR.com account was be rejected. 
           Please resubmit your infomations" ;
        //  echo($email);
          
          $this->sendEmail($email, $content);
          redirect('Admin/pendingdeliveries') ;
    }
    
    function deleteuserfromAdmin($userid){
          $this->db->where('user_id', $userid);   
         $this->db->set('user_deletestatus',1);
         $this->db->update('tb_user');
         redirect('Admin/deliveries') ;
       
    }
    
    function sendEmail($useremail, $content){   
        $to = $useremail;
        $subject = "From BOLTR.com";
       
         
        $from = "BOLTR.com";           
        $hearders = "Mime-Version:1.0";
        $hearders .= "Content-Type : text/html;charset=UTF-8";
        $hearders .= "From: " . $from;
        
        mail($to, $subject, $content, $hearders);
    }
    
    public function search(){
    	print_r($_POST);
      	$date=$this->input->post('todate');
      	$order_id=$this->input->post('order_id');
     	$delivery_id=$this->input->post('delivery_id');
     	
     	$count=0;
     	$this->session->set_userdata("count",$count);
     	
       	$this->session->set_userdata("date",$date);
       	$this->session->set_userdata("order_id",$order_id);
       	$this->session->set_userdata("delivery_id",$delivery_id);
		
		//redirect('/admin/store_main'); 
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function submit_checkbox(){
		$check_form=$this->input->post("check_form");
		if($check_form=="open"){
			
			$data=array();
			$this->load->helper('date');
		    $cur_time = time();
		    $dateY = date("Y", $cur_time);
		    $dateM = date("m", $cur_time);
		    $dateD=date("d", $cur_time);
		    $timehour=date("H", $cur_time);
		    $timeminute=date("i", $cur_time);
		    $currenttime=$dateY."-".$dateM."-".$dateD." ".$timehour.":".$timeminute; 
		
			foreach( $_POST as $key=>$stuff ) {
	       			$childarray=array();
					$childarray['status']=3;
					$childarray['order_id']=$key;
					$childarray['fillfulledtime']=$currenttime;
					array_push($data,$childarray);
	       		
				
			}
			if(count($data)<=2){
				$this->session->set_flashdata('message',"No Order Selected");
				redirect($_SERVER['HTTP_REFERER']);
			}
			$this->db->update_batch('tb_orderhistory', $data, 'order_id');
			redirect($_SERVER['HTTP_REFERER']);
		}
		else{
			//check unique delivery id
			$delivery_id=$this->input->post("delivery_id_text");
			if($delivery_id!=""){
				$delivery_count = $this->admin_model->check_unique_delivery_id($delivery_id);
				if($delivery_count>0){
					$this->session->set_flashdata('message',"Unique Delivery Id Required.");
				 	redirect($_SERVER['HTTP_REFERER']);
				}
				
				$data=array();
				$this->load->helper('date');
		    	$cur_time = time();
		    	$dateY = date("Y", $cur_time);
		    	$dateM = date("m", $cur_time);
		    	$dateD=date("d", $cur_time);
		    	$timehour=date("H", $cur_time);
		    	$timeminute=date("i", $cur_time);
		    	$currenttime=$dateY."-".$dateM."-".$dateD." ".$timehour.":".$timeminute; 
				foreach( $_POST as $key=>$stuff ) {
	       			$childarray=array();
					$childarray['delivery_id']=$this->input->post("delivery_id_text");
					$childarray['status']=4;
					$childarray['order_id']=$key;
					//$childarray['fillfulledtime']=$currenttime;
					array_push($data,$childarray);
				}
				
				if(count($data)<=2){
					$this->session->set_flashdata('message',"No Order Selected");
				 	redirect($_SERVER['HTTP_REFERER']);
				}
				$this->db->update_batch('tb_orderhistory', $data, 'order_id');
				//send push notification
				$storeid=$this->session->userdata('login_adminID');
	       		$this->db->where('admin_id', $storeid);
	       		$storename=$this->db->get('tb_admin')->result()[0]->admin_storename;
				$this->sendPushNotificationDelivery($storename); 
				redirect($_SERVER['HTTP_REFERER']);
			}
			else{
				 $this->session->set_flashdata('message',"Delivery Id Required.");
				 redirect($_SERVER['HTTP_REFERER']);
			}
		
		
		}
	
	}

	function sendPushNotificationDelivery($shopname){

        $user_data = $this->db->where('user_available', 1)->get('tb_user');
        $usersnumrow=$user_data->num_rows();
         // echo ($usersnumrow);
        foreach ($user_data->result() as $user) {
        
            $token = $user->token;   
           // echo ($token);           
           
            
            $message_title = "New Alert";//$title;  
            $message_content = $shopname." Created new delivery";
            
            $url = "https://fcm.googleapis.com/fcm/send";
    //        api_key available in google-services.json

            $api_key = "AIzaSyCfEgr0jMv0X1kNoHphkUR8xP0HLM_uido";

            $notification["to"] = $token;
            $notification["priority"] = "high" ;
                           
            $notification['notification'] = array(
                                    "body" => $message_content,
                                    "title" => $message_title,
                                    "sound" => "notisound.mp3",
                                    
                                    );

            $headers = array(
                            'Content-Type:application/json',
                            'Authorization:key='.$api_key
                            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($notification));

            $result = curl_exec($ch);

            if ($result === FALSE) {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);
        }
        return;
    }
    
    
}
                         

            
        