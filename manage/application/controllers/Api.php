<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

include("./vendor/autoload.php");

class Api extends CI_Controller{
    
                
    function __construct(){
        
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Api_model');
        $this->load->library('session');        
        date_default_timezone_set('America/Chicago');  
    }  
     
     private function doRespond($p_result){   
         
         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
     }

     private function doRespondSuccess($result){
         $result['message'] = "success";
         $this->doRespond($result);
     }
     
     private function doRespondFail($message) {
         $result = array();
         $result['message'] = $message;
         $this->doRespond($result);
                  
     }
     
     
     private function doUrlDecode($p_text){
         
        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text); 
        $p_text = str_replace('%20',' ', $p_text); 
        $p_text = trim($p_text);
        return $p_text; 
     }
     
     //============================ Coffee Farm===========================
      function Login(){
         $result=array();
         
         $email=$_POST["email"];
         $password=$_POST["password"];
         
         $usertype=0;
         if( isset($_POST['usertype']) )
	{
	     $usertype= $_POST["usertype"];
	     //echo $usertype;
	}
        
         
         $email=$this->doUrlDecode($email);
         $password=$this->doUrlDecode($password);
         if($usertype==0){     // company manager
            $result1=  $this->Api_model->AdminExist($email, $password);
           if($result1==0){  // not company email
                $this->doRespondFail("There is no registered email as company manager");           
              	 
	           
           }else if($result1==1){ //incorrect password
           
               $this->doRespondFail("Incorrect Password");
               
           }else if($result1==2){
             
               $companydata=$this->Api_model->AdminLogin($email, $password);
               $result['account']=$companydata;
               
                $companyid=$companydata[0]->id;
               	$this->db->where('company_id', $companyid);         
	         $farms = $this->db->get('tb_farm')->result();
	         $result['farmsofcompany']=$farms;
	         
	         $this->db->where('organic_companyid',$companyid);         
	         $activity= $this->db->get('tb_activity')->result();
	         $result['activity']=$activity;
	         
	         $this->db->where('company_id',$companyid);         
	         $pro= $this->db->get('tb_productfororganiclog')->result();
	         $result['productforlog']=$pro;
	         
	         $this->db->where('product_company',$companyid);         
	         $product= $this->db->get('tb_productforproduction')->result();
	         $result['productforproduction']=$product;
	         
	         $this->db->where('category_company',$companyid);         
	         $category= $this->db->get('tb_category')->result();
	         $result['category']=$category;
	         
	         $this->db->where('category_company',$companyid);         
	         $categoryexpense= $this->db->get('tb_categoryforexpeneses')->result();
	         $result['categoryforexpense']=$categoryexpense;
	         
	         $this->db->where('provider_company',$companyid);         
	         $providers= $this->db->get('tb_provider')->result();
	         $result['providers']=$providers;
	         
	         $this->db->where('client_company',$companyid);         
	         $clients= $this->db->get('tb_client')->result();
	         $result['clients']=$clients;
	         
	          $this->db->where('teamlead_company',$companyid);         
	         $teamleads= $this->db->get('tb_teamlead')->result();
	         $result['teamleads']=$teamleads;
	         
                $this->doRespondSuccess($result);
           }
         }else if ($usertype==1){  // team leader
             $result2=  $this->Api_model->teamleaderexist($email, $password);
	           if($result2==0){
	                $this->doRespondFail("Email not exist!");
	           }else if($result2==1){
	               $this->doRespondFail("Incorrect Password");
	           }else{
	             
	              $this->db->where('teamlead_email', $email);
                      $this->db->where('teamlead_password', $password);
            
                       $userdata= $this->db->get('tb_teamlead')->result();
                       $result['userdata']=$userdata;
                       
                       //================== Company data===================
                        $companyid=$userdata[0]->teamlead_company;
                	 $this->db->where('company_id', $companyid);         
		         $farms = $this->db->get('tb_farm')->result();
		         $result['farmsofcompany']=$farms;
		         
		         $this->db->where('organic_companyid',$companyid);         
		         $activity= $this->db->get('tb_activity')->result();
		         $result['activity']=$activity;
		         
		         $this->db->where('company_id',$companyid);         
		         $pro= $this->db->get('tb_productfororganiclog')->result();
		         $result['productforlog']=$pro;
		         
		         $this->db->where('product_company',$companyid);         
		         $product= $this->db->get('tb_productforproduction')->result();
		         $result['productforproduction']=$product;
		         
		         $this->db->where('category_company',$companyid);         
		         $category= $this->db->get('tb_category')->result();
		         $result['category']=$category;
		         
		         $this->db->where('category_company',$companyid);         
		         $categoryexpense= $this->db->get('tb_categoryforexpeneses')->result();
		         $result['categoryforexpense']=$categoryexpense;
		         
		         $this->db->where('provider_company',$companyid);         
		         $providers= $this->db->get('tb_provider')->result();
		         $result['providers']=$providers;
		         
		         $this->db->where('client_company',$companyid);         
		         $clients= $this->db->get('tb_client')->result();
		         $result['clients']=$clients;
		         
		          $this->db->where('teamlead_company',$companyid);         
		         $teamleads= $this->db->get('tb_teamlead')->result();
		         $result['teamleads']=$teamleads;
	                       
	                       
			         
		         $this->doRespondSuccess($result);
	      }
         }else if ($usertype==2){  // picker leader
                $result2=  $this->Api_model->pickleaderexist($email, $password);
	           if($result2==0){
	                $this->doRespondFail("Email not exist!");
	           }else if($result2==1){
	               $this->doRespondFail("Incorrect Password");
	           }else{
	             
	              $this->db->where('pickerlead_email', $email);
                      $this->db->where('pickerlead_password', $password);
            
                       $userdata= $this->db->get('tb_pickerlead')->result();
                       $result['userdata']=$userdata;
                       
                         $pickleaderid=$userdata[0]->pickerlead_id;
	                 $this->db->where('leadidofpick',$pickleaderid);         
		         $teammembers= $this->db->get('tb_pickerlead')->result();
		         $result['teammembers']=$teammembers;
                       
                       //================== Company data===================
                        $companyid=$userdata[0]->pickerlead_company;
                	 $this->db->where('company_id', $companyid);         
		         $farms = $this->db->get('tb_farm')->result();
		         $result['farmsofcompany']=$farms;
		         
		         $this->db->where('organic_companyid',$companyid);         
		         $activity= $this->db->get('tb_activity')->result();
		         $result['activity']=$activity;
		         
		         $this->db->where('company_id',$companyid);         
		         $pro= $this->db->get('tb_productfororganiclog')->result();
		         $result['productforlog']=$pro;
		         
		         $this->db->where('product_company',$companyid);         
		         $product= $this->db->get('tb_productforproduction')->result();
		         $result['productforproduction']=$product;
		         
		         $this->db->where('category_company',$companyid);         
		         $category= $this->db->get('tb_category')->result();
		         $result['category']=$category;
		         
		         $this->db->where('category_company',$companyid);         
		         $categoryexpense= $this->db->get('tb_categoryforexpeneses')->result();
		         $result['categoryforexpense']=$categoryexpense;
		         
		         $this->db->where('provider_company',$companyid);         
		         $providers= $this->db->get('tb_provider')->result();
		         $result['providers']=$providers;
		         
		         $this->db->where('client_company',$companyid);         
		         $clients= $this->db->get('tb_client')->result();
		         $result['clients']=$clients;
		         
		         $this->db->where('teamlead_company',$companyid);         
		         $teamleads= $this->db->get('tb_teamlead')->result();
		         $result['teamleads']=$teamleads;
		         
		         
	                       
	                       
			         
		         $this->doRespondSuccess($result);
	      }
         }else if ($usertype==3){  // normal user
           
             $result2=  $this->Api_model->normaluserexist($email, $password);
	           if($result2==0){
	                $this->doRespondFail("Email not exist!");
	           }else if($result2==1){
	               $this->doRespondFail("Incorrect Password");
	           }else{
	             
	              $this->db->where('normaluser_email', $email);
                      $this->db->where('normaluser_password', $password);
            
                       $userdata= $this->db->get('tb_normaluser')->result();
                       $result['userdata']=$userdata;
                       
                       //================== Company data===================
                        $companyid=$userdata[0]->normaluser_company;
                	 $this->db->where('company_id', $companyid);         
		         $farms = $this->db->get('tb_farm')->result();
		         $result['farmsofcompany']=$farms;
		         
		         $this->db->where('organic_companyid',$companyid);         
		         $activity= $this->db->get('tb_activity')->result();
		         $result['activity']=$activity;
		         
		         $this->db->where('company_id',$companyid);         
		         $pro= $this->db->get('tb_productfororganiclog')->result();
		         $result['productforlog']=$pro;
		         
		         $this->db->where('product_company',$companyid);         
		         $product= $this->db->get('tb_productforproduction')->result();
		         $result['productforproduction']=$product;
		         
		         $this->db->where('category_company',$companyid);         
		         $category= $this->db->get('tb_category')->result();
		         $result['category']=$category;
		         
		         $this->db->where('category_company',$companyid);         
		         $categoryexpense= $this->db->get('tb_categoryforexpeneses')->result();
		         $result['categoryforexpense']=$categoryexpense;
		         
		         $this->db->where('provider_company',$companyid);         
		         $providers= $this->db->get('tb_provider')->result();
		         $result['providers']=$providers;
		         
		         $this->db->where('client_company',$companyid);         
		         $clients= $this->db->get('tb_client')->result();
		         $result['clients']=$clients;
		         
		          $this->db->where('teamlead_company',$companyid);         
		         $teamleads= $this->db->get('tb_teamlead')->result();
		         $result['teamleads']=$teamleads;
	                       
	                       
			         
		         $this->doRespondSuccess($result);
	      }
         }
         
         
         
        
         
        
     }
     
     function registernewfarm(){
     	$result=array();
         
         $companyid=$_POST["company_id"];
         $farmname=$_POST["farmname"];
        
         
         $companyid=$this->doUrlDecode($companyid);
         $farmname=$this->doUrlDecode($farmname);
         $result2=  $this->Api_model->Registerfarm($companyid, $farmname);
         if($result2==0){
       		  $this->doRespondFail("This farm name already exist in your company.");
         }else{
       		  $this->doRespondSuccess($result);
         }
         
     }
     
     function deletefarm(){
         $result=array();
         
         $farmid=$_POST["farm_id"];    
         $farmid=$this->doUrlDecode($farmid);         
         $this->Api_model->deletefarm($farmid);
          $this->doRespondSuccess($result);
     }
     
     function updatefarm(){
     	$result=array();
         
         $farmid=$_POST["farm_id"];   
         $framname=$_POST["farm_name"];  
         $farmid=$this->doUrlDecode($farmid); 
         $framname=$this->doUrlDecode($framname);        
         $this->Api_model->updatefarm($farmid,$framname);
          $this->doRespondSuccess($result);
     }
     
     //=============================== Team lead register module =======================
     function registernewteamlead(){
         $result=array();
          
         $companyid=$_POST["company_id"];
         $teamleadname=$_POST["teamleadname"];
         $teamleademail=$_POST["teamleademail"];
         $teamleadpassword=$_POST["teamleadpassword"];
         $teamleadphone=$_POST["teamleadphone"];
         $teamleadaddress=$_POST["teamleadaddress"];
        
         
         $companyid=$this->doUrlDecode($companyid);
         $teamleademail=$this->doUrlDecode($teamleademail);
         $teamleadname=$this->doUrlDecode($teamleadname);
         $teamleadpassword=$this->doUrlDecode($teamleadpassword);
         $teamleadphone=$this->doUrlDecode($teamleadphone);
         $teamleadaddress=$this->doUrlDecode($teamleadaddress);
         
         $result2=  $this->Api_model->Registerteamlead($companyid, $teamleademail, $teamleadname, $teamleadpassword, $teamleadphone,$teamleadaddress );
         if($result2==0){
       		  $this->doRespondFail("This team lead name already exist in your company.");
         }else{
       		  $this->doRespondSuccess($result);
         }
     }
     
     function updateteamlead(){
         $result=array();
         $teamleadid=$_POST["teamlead_id"];          
         $companyid=$_POST["company_id"];
         $teamleadname=$_POST["teamleadname"];
         $teamleademail=$_POST["teamleademail"];
         $teamleadpassword=$_POST["teamleadpassword"];
         $teamleadphone=$_POST["teamleadphone"];
         $teamleadaddress=$_POST["teamleadaddress"];
        
        
         $teamleadid=$this->doUrlDecode($teamleadid);
      
         $companyid=$this->doUrlDecode($companyid);
         $teamleademail=$this->doUrlDecode($teamleademail);
         $teamleadname=$this->doUrlDecode($teamleadname);
         $teamleadpassword=$this->doUrlDecode($teamleadpassword);
         $teamleadphone=$this->doUrlDecode($teamleadphone);
         $teamleadaddress=$this->doUrlDecode($teamleadaddress);
         
         $result2=  $this->Api_model->updateteamlead($teamleadid, $companyid, $teamleademail, $teamleadname, $teamleadpassword, $teamleadphone,$teamleadaddress);
         if($result2==0){
       		  $this->doRespondFail("This team lead does not exist in your company.");
         }else{
       		  $this->doRespondSuccess($result);
         }
     }
     
     function deleteteamlead($teamlead_id){
          $result=array();
           $this->Api_model->deleteteamlead($teamlead_id);
          $this->doRespondSuccess($result);
    }
    
    function getallteamleads(){
         $result=array();              
         $companyid=$_POST["company_id"];   
         $startdate="All";
         $enddate="All"; 
         
        
         if( isset($_POST['daterange']) )
	{
	     $daterange= $_POST["daterange"];
	     
	}     
            
         $companyid=$this->doUrlDecode($companyid);
         
         $this->db->where("teamlead_company", $companyid);
         $this->db->where("deletestatus", 0);       	 
   	 $teamleads= $this->db->get('tb_teamlead')->result();
   	 
   	 $teamleadarry=array();
   	 
   	 
   	 foreach($teamleads as $teamlead){
   	    $teamleadname=$teamlead->teamlead_name;
   	    $teamleadid=$teamlead->teamlead_id;
   	    
   	    
   	    
   	    $amount=0;
   	    
   	    // All oganic logs assigned to this team lead
   	    $this->db->where('organic_teamlead', $teamleadid);
   	    if($daterange=="All"){
   	    }else{
	   	 $daterange1=explode("_",$daterange);
	         $startdate=$daterange1[0];
	         $enddate=$daterange1[1];
	         
	         if($startdate>$enddate){
	              $tempdate=$enddate;
	              $enddate=$startdate;
	              $startdate=$tempdate;	            
	         }         
	               
	        $this->db->where('organiclog_date>=', $startdate);
	        $this->db->where('organiclog_date<=',$enddate);
		
            }
            
            
            
   	    $alloganiclogs=$this->db->get('tb_organiclog')->result(); 
   	    
   	    foreach($alloganiclogs as $oganiclog) {
   	    
   	       // get all workers and times for the selected oganic log 
   	       $includedworkers=$oganiclog->organic_workers;
   	       $workinghours=$oganiclog->organic_times;
   	       
   	       $workerArray = explode(',', $includedworkers);
   	       $timeArray = explode(',', $workinghours);
   	       
   	       $teamleaderposition=0;
   	       
   	       foreach($workerArray as $worker){
   	          if($worker==$teamleadname){  // if there is team leader name in the assigned workers array
   	             $amount+=$timeArray[$teamleaderposition] ;
   	             
   	             
   	          }
   	          $teamleaderposition++; 
   	       }
   	    }
   	   
   	    $oneteamlead= array('totalhours'      =>    $amount,
   	                 'teamlead_id'     =>    $teamlead->teamlead_id,
   	                 'teamlead_company'=>    $teamlead->teamlead_company,
   	                 'teamlead_name'   =>    $teamlead->teamlead_name,
   	                 'teamlead_email'  =>    $teamlead->teamlead_email,
   	                 'teamlead_password'=>   $teamlead->teamlead_password,
   	                 'teamlead_phone'  =>    $teamlead->teamlead_phone,
   	                 'teamlead_address'=>    $teamlead->teamlead_address
   	    );
   	    array_push($teamleadarry,$oneteamlead);
   	 }
   	 
   	 
   	 $result['employees']=$teamleadarry;
   	 $this->doRespondSuccess($result);
    }
    
    function addteamembers(){
        $result=array();
          
         $companyid=$_POST["company_id"];
         $teamleadid=$_POST["teamleadid"];
         $membername=$_POST["membername"];
         $memberemail=$_POST["memberemail"];
         $memberphone=$_POST["memberphone"];
         $memberaddress=$_POST["memberaddress"];
        
         
         $companyid=$this->doUrlDecode($companyid);
         $teamleadid=$this->doUrlDecode($teamleadid);
         $membername=$this->doUrlDecode($membername);
         $memberemail=$this->doUrlDecode($memberemail);
         $memberphone=$this->doUrlDecode($memberphone);
         $memberaddress=$this->doUrlDecode($memberaddress);
         
         $result2=  $this->Api_model->Registerteammember($companyid, $teamleadid, $membername, $memberemail, $memberphone,$memberaddress);
         if($result2==0){
       		  $this->doRespondFail("This worker name already exist in your company.");
         }else{
       		  $this->doRespondSuccess($result);
         }
    }
    
    function updateteammember(){
         $result=array();
          
         $companyid=$_POST["company_id"];
         $teamleadid=$_POST["teamleadid"];
         $teammemberid=$_POST["teammemberid"];
         $membername=$_POST["membername"];
         $memberemail=$_POST["memberemail"];
         $memberphone=$_POST["memberphone"];
         $memberaddress=$_POST["memberaddress"];
        
         
         $companyid=$this->doUrlDecode($companyid);
         $teamleadid=$this->doUrlDecode($teamleadid);
         $membername=$this->doUrlDecode($membername);
         $memberemail=$this->doUrlDecode($memberemail);
         $memberphone=$this->doUrlDecode($memberphone);
         $memberaddress=$this->doUrlDecode($memberaddress);
         
         $result2=  $this->Api_model->Updateteammember($companyid, $teamleadid, $teammemberid,$membername, $memberemail, $memberphone,$memberaddress);
         if($result2==0){
       		  $this->doRespondFail("This worker deoes not exist in your company.");
         }else{
       		  $this->doRespondSuccess($result);
         }
    }
    
    function deleteteammember($teammemberid){
          $result=array();
           $this->Api_model->deleteteammember($teammemberid);
          $this->doRespondSuccess($result);
    }
    
    function getallteammember(){
      $result=array();
      
       $teamleaderid=$_POST["teamleaderid"];
       $daterange=$_POST["daterange"];
      
      $this->db->where('farmer_teamleadid',$teamleaderid);
      $this->db->where('deletestatus',0);
      $memebers=$this->db->get("tb_workers")->result();
      
       //===== get team lead name=============================================
       $this->db->where('teamlead_id',$teamleaderid);
       $teamleadername=$this->db->get("tb_teamlead")->result()[0]->teamlead_name;
       // echo $teamleadername;
       $teamleaderamount=0;
       // =========get team leader's amount======
       // All oganic logs assigned to this team lead
		$this->db->where('organic_teamlead', $teamleaderid);
		if($daterange=="All"){
		}else{
		 $daterange1=explode("_",$daterange);
		 $startdate=$daterange1[0];
		 $enddate=$daterange1[1];
		 
		 if($startdate>$enddate){
		      $tempdate=$enddate;
		      $enddate=$startdate;
		      $startdate=$tempdate;	            
		 }         
		       
		$this->db->where('organiclog_date>=', $startdate);
		$this->db->where('organiclog_date<=',$enddate);
		
		}
            
            
   	        $alloganiclogs=$this->db->get('tb_organiclog')->result(); 
   	    
   	        foreach($alloganiclogs as $oganiclog) {
   	    
	   	       // get all workers and times for the selected oganic log 
	   	       $includedworkers=$oganiclog->organic_workers;
	   	       $workinghours=$oganiclog->organic_times;
	   	       
	   	       $workerArray = explode(',', $includedworkers);
	   	       $timeArray = explode(',', $workinghours);
	   	       
	   	       $teamleaderposition=0;
	   	       
	   	       foreach($workerArray as $worker){
	   	          if($worker==$teamleadername){  // if there is team leader name in the assigned workers array
	   	         
	   	             $teamleaderamount+=$timeArray[$teamleaderposition] ;
	   	            
	   	          }
	   	           $teamleaderposition++; 
	   	       }
   	        }
   	       // echo $teamleaderamount;
       
       
       
      //======================= get members amount=============================
      
       $teammembersarry=array();
   	 
       foreach($memebers as $member){
		$teammembername=$member->farmer_name;
		$teammemberid=$member->farmer_id;
		$teamleadid=$member->farmer_teamleadid;
		
		$amount=0;
		
		// All oganic logs assigned to this team lead
		$this->db->where('organic_teamlead', $teamleadid);
		if($daterange=="All"){
		}else{
		 $daterange1=explode("_",$daterange);
		 $startdate=$daterange1[0];
		 $enddate=$daterange1[1];
		 
		 if($startdate>$enddate){
		      $tempdate=$enddate;
		      $enddate=$startdate;
		      $startdate=$tempdate;	            
		 }         
		       
		$this->db->where('organiclog_date>=', $startdate);
		$this->db->where('organiclog_date<=',$enddate);
		
		} 
            
   	        $alloganiclogs=$this->db->get('tb_organiclog')->result(); 
   	    
   	        foreach($alloganiclogs as $oganiclog) {
   	    
	   	       // get all workers and times for the selected oganic log 
	   	       $includedworkers=$oganiclog->organic_workers;
	   	       $workinghours=$oganiclog->organic_times;
	   	       
	   	       $workerArray = explode(',', $includedworkers);
	   	       $timeArray = explode(',', $workinghours);
	   	       
	   	       $teamworkerposition=0;
	   	       
	   	       foreach($workerArray as $worker){
	   	          if($worker==$teammembername){  // if there is team leader name in the assigned workers array
	   	         
	   	             $amount+=$timeArray[$teamworkerposition] ;   	                         
	   	            
	   	          }
	   	           $teamworkerposition++; 
	   	       }
   	        }
   	    
   	       $oneteammember= array(
		                 'totalhours'          =>    $amount,
		                 'farmer_id'           =>    $member->farmer_id,
		                 'farmer_company'      =>    $member->farmer_company,
		                 'farmer_teamleadid'   =>    $member->farmer_teamleadid,
		                 'farmer_name'         =>    $member->farmer_name,
		                 'farmer_email'        =>    $member->farmer_email,
		                 'farmer_phone'        =>    $member->farmer_phone,
		                 'farmer_address'      =>    $member->farmer_address
		    );
	      array_push($teammembersarry,$oneteammember);
   	   
   	 }
      
      
      $result['teamleaderamount']=$teamleaderamount;     
      $result['members']=$teammembersarry;
      $this->doRespondSuccess($result);
    }
    
    function getallorganiclogbyteamleader(){
       $result=array();
       $teamleaderid=$_POST["teamleader_id"];
       
       $this->db->where('organic_teamlead', $teamleaderid);
       $this->db->order_by("organiclog_date", "DESC");
       $organic=$this->db->get("tb_organiclog")->result();
       $result["organic"]=$organic;
        $this->doRespondSuccess($result);
    }
    
   
    
    
    
    
    
    
    //============== Picker leader register module ================
    function registernewpickerleader(){
        $result=array();
          
         $companyid=$_POST["company_id"];
         $pickleadername=$_POST["pickleadername"];
         $pickleaderemail=$_POST["pickleaderemail"];
         $pickleaderphone=$_POST["pickleaderphone"];
         $pickleaderaddress=$_POST["pickleaderaddress"];
         $pickleaderpassword=$_POST["pickleaderpassword"];
         $pickleadertype=$_POST["pickleadertype"];
         $pickleaderrate=$_POST["pickleaderrate"];
         $leaderid=$_POST["leaderid"];
         
         
        
         
         $companyid=$this->doUrlDecode($companyid);
         $pickleadername=$this->doUrlDecode($pickleadername);
         $pickleaderemail=$this->doUrlDecode($pickleaderemail);
          $pickleaderaddress=$this->doUrlDecode($pickleaderaddress);
         $pickleaderphone=$this->doUrlDecode($pickleaderphone);
         $pickleaderpassword=$this->doUrlDecode($pickleaderpassword);
         $pickleadertype=$this->doUrlDecode($pickleadertype);
         $pickleaderrate=$this->doUrlDecode($pickleaderrate);
         
         $result2=  $this->Api_model->Registerpickerlead($companyid, $pickleadername, $pickleaderemail, $pickleaderpassword,$pickleaderphone, $pickleaderaddress, $pickleadertype, $pickleaderrate, $leaderid);
         if($result2==0){
       		  $this->doRespondFail("This picker name already exist in your company.");
         }else{
       		  $this->doRespondSuccess($result);
         }
    }
    
     function updatepickerlead(){
         $result=array();
        
         $leaderid=$_POST["leaderid"];      // unique id in table
         $companyid=$_POST["company_id"];
         $pickleadername=$_POST["pickleadername"];
         $pickleaderemail=$_POST["pickleaderemail"];
         $pickleaderphone=$_POST["pickleaderphone"];
         $pickleaderaddress=$_POST["pickleaderaddress"];
         $pickleaderpassword=$_POST["pickleaderpassword"];
         $pickleadertype=$_POST["pickleadertype"];
         $pickleaderrate=$_POST["pickleaderrate"];
        
         
         $companyid=$this->doUrlDecode($companyid);
         $pickleadername=$this->doUrlDecode($pickleadername);
         $pickleaderemail=$this->doUrlDecode($pickleaderemail);
         $pickleaderaddress=$this->doUrlDecode($pickleaderaddress);
         $pickleaderphone=$this->doUrlDecode($pickleaderphone);
         $pickleaderpassword=$this->doUrlDecode($pickleaderpassword);
         $pickleadertype=$this->doUrlDecode($pickleadertype);
         $pickleaderrate=$this->doUrlDecode($pickleaderrate);
         
         $result2=  $this->Api_model->Updatepickerlead($leaderid, $companyid, $pickleadername, $pickleaderemail, $pickleaderpassword, $pickleaderphone, $pickleaderaddress, $pickleadertype, $pickleaderrate);
         if($result2==0){
       		  $this->doRespondFail("This pickerlead does not exist in your company.");
         }else{
       		  $this->doRespondSuccess($result);
         }
     }
     
     function deletepickerlead($pickerleadid){
           $result=array();
          $this->Api_model->deletepickerlead($pickerleadid);
          $this->doRespondSuccess($result);
     }
     
     function getallpickerlead(){
      $result=array();
      $companyid=$_POST["company_id"];
      $leadidofpick=$_POST["leadidofpick"];    // if getall picker leader :-1, if get all picker : picker leader id
      $farmid=$_POST["farmid"];
      $date1=$_POST["date1"];
      
      $date1=$this->doUrlDecode($date1);
      
      
      
      if($leadidofpick==-1){  // one leader amount module
      }else{
              $leadertotal=0;
              $leadername="";
	      $this->db->where('pickerlead_company',$companyid);
	      $this->db->where('pickerlead_id',$leadidofpick);
	      $this->db->where('deletestatus',0);
	      $leader=$this->db->get("tb_pickerlead")->result()[0];
	      $leadername=$leader->pickerlead_name;
	      
	      
	       $this->db->where('picking_company',$companyid);     // company filter
	         if($farmid=="All"){                                 // farm filter
	         }else{
	           $this->db->where('picking_farm',$farmid);
	         }
	         
	         if($date1=="date1"){                                // date filter
	         }else{
	           $daterange=explode("_",$date1);
	           $startdate=$daterange[0];
	           $enddate=$daterange[1];
	           if($startdate>$enddate){
	             $tempdate=$startdate;
	             $startdate=$enddate;
	             $enddate=$tempdate;
	           }
	              $this->db->where('picking_date>=', $startdate);
		      $this->db->where('picking_date<=',$enddate);
		      
	         }
	         $this->db->where("pickingtype",0);
	         $allpickingsofthecompany = $this->db->get("tb_pickingandreceiving")->result();
	         foreach($allpickingsofthecompany as $onepickinglog){
	            $pickingpickers=$onepickinglog->picking_pickername;
	            $pickinglbs=$onepickinglog->picking_lbs;
	           
	           
	            
	            $picckersarray=explode(",",$pickingpickers);
	            $lbsarray=explode(",",$pickinglbs);
	            
	            $pickerpostion1=0;
	            foreach($picckersarray as $picker){
	               
	               if($picker==$leadername){
	                 $leadertotal+=$lbsarray[$pickerpostion1];
	               }
	               $pickerpostion1+=1;
	            }
	         }
	         $result["leadertotal"]=$leadertotal;
		      
	  }
	      
	      
      
      
      
      
      //========================= Pickers amount getting module===========
      $this->db->where('pickerlead_company',$companyid);
      $this->db->where('leadidofpick',$leadidofpick);
      $this->db->where('deletestatus',0);
      $memebers=$this->db->get("tb_pickerlead")->result();
      
      $pickersarrays=array();
      
      
      foreach($memebers as $onepickerleader){
        $pickername=$onepickerleader->pickerlead_name;
        $pickertotallbs=0;
        
        
        //================ Filter all picking logs by company , farm and date ==================
         $this->db->where('picking_company',$companyid);     // company filter
         if($farmid=="All"){                                 // farm filter
         }else{
           $this->db->where('picking_farm',$farmid);
         }
         
         if($date1=="date1"){                                // date filter
         }else{
           $daterange=explode("_",$date1);
           $startdate=$daterange[0];
           $enddate=$daterange[1];
           if($startdate>$enddate){
             $tempdate=$startdate;
             $startdate=$enddate;
             $enddate=$tempdate;
           }
              $this->db->where('picking_date>=', $startdate);
	      $this->db->where('picking_date<=',$enddate);
	      
         }
         
         $this->db->where("pickingtype",0);
         $allpickingsofthecompany = $this->db->get("tb_pickingandreceiving")->result();
         foreach($allpickingsofthecompany as $onepickinglog){
            $pickingpickers=$onepickinglog->picking_pickername;
            $pickinglbs=$onepickinglog->picking_lbs;
           
           
            
            $picckersarray=explode(",",$pickingpickers);
            $lbsarray=explode(",",$pickinglbs);
            
            $pickerpostion=0;
            foreach($picckersarray as $picker){
               
               if($picker==$pickername){
                 $pickertotallbs+=$lbsarray[$pickerpostion];
               }
               $pickerpostion+=1;
            }
         }
         
          $onepicker= array(
		                 'totallbs'             =>    $pickertotallbs,
		                 'pickerlead_id'        =>    $onepickerleader->pickerlead_id,
		                 'leadidofpick'         =>    $onepickerleader->leadidofpick,
		                 'pickerlead_company'   =>    $onepickerleader->pickerlead_company,
		                 'pickerlead_name'      =>    $onepickerleader->pickerlead_name,
		                 'pickerlead_email'     =>    $onepickerleader->pickerlead_email,
		                 'pickerlead_phone'     =>    $onepickerleader->pickerlead_phone,
		                 'pickerlead_address'   =>    $onepickerleader->pickerlead_address,
		                 'pickerlead_password'  =>    $onepickerleader->pickerlead_password,
		                 'rate_lb'              =>    $onepickerleader->rate_lb,
		                 'picker_type'          =>    $onepickerleader->picker_type
		    );
	  array_push($pickersarrays,$onepicker);
        
      }
      
      
      
      
      
      
      $result['members']=$pickersarrays;
      $this->doRespondSuccess($result);
     }
     
     function registernewpickinglog(){
         $result=array();
         $picking_farm=$_POST["picking_farm"];      // unique id in table
         $picking_pickerleaderid=$_POST["picking_pickerleaderid"];
         $picking_date=$_POST["picking_date"];
         $picking_pickername=$_POST["picking_pickername"];
         $picking_lbs=$_POST["picking_lbs"];
         $picking_bags=$_POST["picking_bags"];
         $picking_company=$_POST["picking_company"];
         $totallbs=$_POST["totallbs"];
         
         $picking_farm=$this->doUrlDecode($picking_farm);
         $picking_pickerleaderid=$this->doUrlDecode($picking_pickerleaderid);
         $picking_date=$this->doUrlDecode($picking_date);
         $picking_pickername=$this->doUrlDecode($picking_pickername);
         $picking_lbs=$this->doUrlDecode($picking_lbs);
         $picking_bags=$this->doUrlDecode($picking_bags);
         $picking_company=$this->doUrlDecode($picking_company);
         
         $this->Api_model->registerpickinglog($picking_farm, $picking_pickerleaderid, $picking_date, $picking_pickername, $picking_lbs, $picking_bags, $picking_company, $totallbs);
         $this->doRespondSuccess($result);
         
     }
     
     function updatepickinglog(){
         $result=array();
         $pickinglog_id=$_POST["pickinglog_id"];
         $picking_farm=$_POST["picking_farm"];      // unique id in table
         $picking_pickerleaderid=$_POST["picking_pickerleaderid"];
         $picking_date=$_POST["picking_date"];
         $picking_pickername=$_POST["picking_pickername"];
         $picking_lbs=$_POST["picking_lbs"];
         $picking_bags=$_POST["picking_bags"];
         $picking_company=$_POST["picking_company"];
         $totallbs=$_POST["totallbs"];
         $oldtotallbs=$_POST["oldtotallbs"];
         
         $picking_farm=$this->doUrlDecode($picking_farm);
         $picking_pickerleaderid=$this->doUrlDecode($picking_pickerleaderid);
         $picking_date=$this->doUrlDecode($picking_date);
         $picking_pickername=$this->doUrlDecode($picking_pickername);
         $picking_lbs=$this->doUrlDecode($picking_lbs);
         $picking_bags=$this->doUrlDecode($picking_bags);
         $picking_company=$this->doUrlDecode($picking_company);
         
         $this->Api_model->updatepickinglog($pickinglog_id, $picking_farm, $picking_pickerleaderid, $picking_date, $picking_pickername, $picking_lbs, $picking_bags, $picking_company, $totallbs,$oldtotallbs);
         $this->doRespondSuccess($result);
     }
     
     function getallpickinglogbyleaderid(){
         $result=array();
         $companyid=$_POST["companyid"];      
         $pickerleaderid=$_POST["pickerleaderid"];
         $date1=$_POST["daterange"];
         $farmid=$_POST["farmid"];
         
         $companyid=$this->doUrlDecode($companyid);
         $pickerleaderid=$this->doUrlDecode($pickerleaderid);
         $date1=$this->doUrlDecode($date1);
         $farmid=$this->doUrlDecode($farmid);
         
         if($date1=="test"){
	        if($farmid=="All"){
	          $this->db->where("picking_company", $companyid);
	        }else{
	          $this->db->where("picking_farm", $farmid);
	        }
	        if($pickerleaderid=="All"){
	        }else{
	         $this->db->where("picking_pickerleaderid", $pickerleaderid);
	        }
	          $this->db->order_by("picking_date", "DESC");
	          $this->db->where("pickingtype",0);
	          $logs=$this->db->get("tb_pickingandreceiving")->result();
	          $result["log"]=$logs;
	          $this->doRespondSuccess($result);
        }else{
	         $daterange=explode("_",$date1);
	         $startdate=$daterange[0];
	         $enddate=$daterange[1];
	         if($startdate>$enddate){
	             $this->doRespondFail("End date should be big than start date.");
	         }else{
	         
	               if($farmid=="All"){
		          $this->db->where("picking_company", $companyid);
		        }else{
		          $this->db->where("picking_farm", $farmid);
		        }
		        if($pickerleaderid=="All"){
		        }else{
		         $this->db->where("picking_pickerleaderid", $pickerleaderid);
		        }
		          $this->db->where('picking_date>=', $startdate);
		          $this->db->where('picking_date<=',$enddate);
		          $this->db->where("pickingtype",0);
		          $this->db->order_by("picking_date", "DESC");
		          $logs=$this->db->get("tb_pickingandreceiving")->result();
		          $result["log"]=$logs;
		         
		         $this->doRespondSuccess($result);
	         }
        }
         
         
     }
     
     function deletepickinglog(){
          $result=array();
         $companyid=$_POST["companyid"];      
         $pickinglogid=$_POST["pickinglogid"];
         $farmid=$_POST["farmid"];
         $totalamount=$_POST["totalamount"];
         
         $companyid=$this->doUrlDecode($companyid);
         $pickinglogid=$this->doUrlDecode($pickinglogid);
         $farmid=$this->doUrlDecode($farmid);
         $totalamount=$this->doUrlDecode($totalamount);
         
         $this->Api_model->deletepickinglog($companyid,$pickinglogid, $farmid, $totalamount);
         $this->doRespondSuccess($result);
     }
     
     
     //============================ Receiving Module =====================
     // Generate receipt
    function registernewreceiving() {
          $result=array();
          $picking_company=$_POST["picking_company"];
          $picking_farm=$_POST["picking_farm"];
          $picking_date=$_POST["picking_date"];
          $picking_lbs=$_POST["picking_lbs"];
          $picking_bags=$_POST["picking_bags"];
          $receiving_clientid=$_POST["receiving_clientid"];
          $receiving_quality=$_POST["receiving_quality"];
          $receiving_comment=$_POST["receiving_comment"];
          
          $picking_company=$this->doUrlDecode($picking_company);
          $picking_farm=$this->doUrlDecode($picking_farm);
          $picking_date=$this->doUrlDecode($picking_date);
          $picking_lbs=$this->doUrlDecode($picking_lbs);
          $picking_bags=$this->doUrlDecode($picking_bags);
          $receiving_clientid=$this->doUrlDecode($receiving_clientid);
          $receiving_quality=$this->doUrlDecode($receiving_quality);
          $receiving_comment=$this->doUrlDecode($receiving_comment);
           
          $data = $this->Api_model->savenewreceiving($picking_company,$picking_farm,$picking_date,$picking_lbs,$picking_bags,$receiving_clientid,$receiving_quality,$receiving_comment);
          $result['message'] = $this->generateReceipt($data);
          $this->doRespondSuccess($result);
    }
    
    function generateReceipt($data) {
        
        $this->load->helper('url');
        
        $this->load->library('googledrive');
        
        $mpdf = new \Mpdf\Mpdf();       

        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Codeglamour - Receipt");
        $mpdf->SetAuthor("Codeglamour");
        $mpdf->watermark_font = 'Codeglamour';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');  
        
        $html = $this->load->view('receipt', $data, true);
        $mpdf->WriteHTML($html);
        
        //$mpdf->WriteHTML(file_get_contents('invoice.html'));        
        $filename = $data['received_from'].'-'.date('Y-m-d')."R";

        $mpdf->Output('uploadfiles/'.$filename . '.pdf', Mpdf\Output\Destination::FILE); 
          
        $file = $this->googledrive->upload(FCPATH.'/uploadfiles/', $filename.'.pdf');
        if (!empty($file->id)) {
            //printf("File Uploaded Successfully! [ID: %s\n", $file->id.']');
            return "File Uploaded Successfully!";
        }
            
        /*    
        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Drive($client);
        $fileMetadata = new Google_Service_Drive_DriveFile(array('name' => $fileName));
        $content = file_get_contents($path. $fileName);
        $file = $service->files->create($fileMetadata, array(
          'data'       => $content,
          'mimeType'   => mime_content_type($path. $fileName), //'image/jpeg',
          'uploadType' => 'multipart',
          'fields'     => 'id')
        );
        return $file;                

        */
        
    }
    
    function getDriveToken() {
        
        $this->load->library('googledrive');
        
        $this->googledrive->upload(FCPATH.'/uploadfiles/', 'test'.'.pdf');        
        
    }
    
    function viewFolders() {
        
        $this->load->library('googledrive');
        
        $this->googledrive->loadFolders(); 
    }
    
    function generateInvoice($data) {
        
        $this->load->helper('url');
        
        $this->load->library('googledrive');
        
        $mpdf = new \Mpdf\Mpdf();       

        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Codeglamour - Receipt");
        $mpdf->SetAuthor("Codeglamour");
        $mpdf->watermark_font = 'Codeglamour';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');  
        
        $html = $this->load->view('invoice', $data, true);
        $mpdf->WriteHTML($html);
        
        //$mpdf->WriteHTML(file_get_contents('invoice.html'));        
        $filename = $data['sold_to'].'-'.date('Y-m-d').substr($data['method'], 0, 1);

        $mpdf->Output('uploadfiles/'.$filename . '.pdf', Mpdf\Output\Destination::FILE); 
          
        $file = $this->googledrive->upload(FCPATH.'/uploadfiles/', $filename.'.pdf');
        if (!empty($file->id)) {
            //printf("File Uploaded Successfully! [ID: %s\n", $file->id.']');
            return "File Uploaded Successfully!"; 
        }  

        
    }    
    
    function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Google Drive API PHP Quickstart');
        $client->setScopes(Google_Service_Drive::DRIVE_METADATA_READONLY);
        $client->setAuthConfig('client_secret.json');
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        $credentialsPath = expandHomeDirectory('credentials.json');
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            // Store the credentials to disk.
            if (!file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($accessToken));
            printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    /**
     * Expands the home directory alias '~' to the full path.
     * @param string $path the path to expand.
     * @return string the expanded path.
     */
    function expandHomeDirectory($path)
    {
        $homeDirectory = getenv('HOME');
        if (empty($homeDirectory)) {
            $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
        }
        return str_replace('~', realpath($homeDirectory), $path);
    }

    

     
    function updatereceiving(){
          $result=array();
          $id=$_POST["id"];
          $picking_company=$_POST["picking_company"];
          $picking_farm=$_POST["picking_farm"];
          $picking_date=$_POST["picking_date"];
          $picking_lbs=$_POST["picking_lbs"];
          $picking_bags=$_POST["picking_bags"];
          $receiving_clientid=$_POST["receiving_clientid"];
          $receiving_quality=$_POST["receiving_quality"];
          $receiving_comment=$_POST["receiving_comment"];
          
           $picking_company=$this->doUrlDecode($picking_company);
           $picking_farm=$this->doUrlDecode($picking_farm);
           $picking_date=$this->doUrlDecode($picking_date);
           $picking_lbs=$this->doUrlDecode($picking_lbs);
           $picking_bags=$this->doUrlDecode($picking_bags);
           $receiving_clientid=$this->doUrlDecode($receiving_clientid);
           $receiving_quality=$this->doUrlDecode($receiving_quality);
           $receiving_comment=$this->doUrlDecode($receiving_comment);
           
          $data = $this->Api_model->updatereceiving($id,$picking_company,$picking_farm,$picking_date,$picking_lbs,$picking_bags,$receiving_clientid,$receiving_quality,$receiving_comment);
          
          $result['message'] = $this->generateReceipt($data);
          
          $this->doRespondSuccess($result);
     }
     
     function getallreceiving(){
         $result=array();
         $companyid=$_POST["company_id"];      
         $client_id=$_POST["client_id"];
         $date1=$_POST["date1"];
         $farmid=$_POST["farm_id"];
         
         $companyid=$this->doUrlDecode($companyid);
         $client_id=$this->doUrlDecode($client_id);
         $date1=$this->doUrlDecode($date1);
         $farmid=$this->doUrlDecode($farmid);
         
         if($date1=="test"){
	        if($farmid=="All"){
	          $this->db->where("picking_company", $companyid);
	        }else{
	          $this->db->where("picking_farm", $farmid);
	        }
	        if($client_id=="All"){
	        }else{
	         $this->db->where("receiving_clientid", $client_id);
	        }
	          $this->db->order_by("picking_date", "DESC");
	          $this->db->where("pickingtype",1);
	          $logs=$this->db->get("tb_pickingandreceiving")->result();
	          $result["log"]=$logs;
	          $this->doRespondSuccess($result);
        }else{
	         $daterange=explode("_",$date1);
	         $startdate=$daterange[0];
	         $enddate=$daterange[1];
	         if($startdate>$enddate){
	             $tempdate=$startdate;
	             $startdate=$enddate;
	             $enddate=$tempdate;
	         }else{
	         
	               if($farmid=="All"){
		          $this->db->where("picking_company", $companyid);
		        }else{
		          $this->db->where("picking_farm", $farmid);
		        }
		        if($client_id=="All"){
		        }else{
		         $this->db->where("receiving_clientid", $client_id);
		        }
		          $this->db->where('picking_date>=', $startdate);
		          $this->db->where('picking_date<=',$enddate);
		          $this->db->where("pickingtype",1);
		          $this->db->order_by("picking_date", "DESC");
		          $logs=$this->db->get("tb_pickingandreceiving")->result();
		          $result["log"]=$logs;
		         
		         $this->doRespondSuccess($result);
	         }
        }
     }
     
     function deletereceiving($recevingid){
      $result=array();
       $this->db->where("picking_id", $recevingid);
       $this->db->delete("tb_pickingandreceiving");
        $this->doRespondSuccess($result);
     }
     
      
     
     
     //============ Normal user module ===============
     function registernewnormaluser(){
          $result=array();
          
         $companyid=$_POST["company_id"];
         $name=$_POST["name"];
         $email=$_POST["email"];
         $phone=$_POST["phone"];
         $address=$_POST["address"];
         $password=$_POST["password"];
         $permission=$_POST["permission"];
         
        
         
         $companyid=$this->doUrlDecode($companyid);
         $name=$this->doUrlDecode($name);
         $email=$this->doUrlDecode($email);
         $phone=$this->doUrlDecode($phone);
         $address=$this->doUrlDecode($address);
         $password=$this->doUrlDecode($password);
         $permission=$this->doUrlDecode($permission);
         
         $result2=  $this->Api_model->Registernewnormaluser($companyid, $name, $email, $phone, $address, $password, $permission);
         if($result2==0){
       		  $this->doRespondFail("This user email already exist in your company.");
         }else{
       		  $this->doRespondSuccess($result);
         }
     }
     
     
     
    function updatenormaluser(){
          $result=array();
          $id=$_POST["id"];
         $companyid=$_POST["company_id"];
         $name=$_POST["name"];
         $email=$_POST["email"];
         $phone=$_POST["phone"];
         $address=$_POST["address"];
         $password=$_POST["password"];
         $permission=$_POST["permission"];
         
        
         
         $companyid=$this->doUrlDecode($companyid);
         $name=$this->doUrlDecode($name);
         $email=$this->doUrlDecode($email);
         $phone=$this->doUrlDecode($phone);
         $address=$this->doUrlDecode($address);
         $password=$this->doUrlDecode($password);
         $permission=$this->doUrlDecode($permission);
         
         $result2=  $this->Api_model->Updatenormaluser($id, $companyid, $name, $email, $phone, $address, $password, $permission);
         if($result2==0){
       		  $this->doRespondFail("This user not exist in your company.");
         }else{
       		  $this->doRespondSuccess($result);
         }
    }
    
    function deletenormaluser($userid){
          $result=array();
          $this->Api_model->deletenormaluser($userid);
          $this->doRespondSuccess($result);
    }
    
    function getAllnormaluser(){
         $result=array();              
         $companyid=$_POST["company_id"];         
            
         $companyid=$this->doUrlDecode($companyid);
         
         $this->db->where("normaluser_company", $companyid);
       	 
   	 $teamleads= $this->db->get('tb_normaluser')->result();
   	 $result['employees']=$teamleads;
   	 $this->doRespondSuccess($result);
    }
     
     
     //==================== Processing Module=================
     function getallreceivingpickinglog(){
         $result=array();
        
         $companyid=$_POST["company_id"];
         $date=$_POST["date"];
         
         $date=$this->doUrlDecode($date);
         $companyid=$this->doUrlDecode($companyid);
         
         $this->db->where("picking_company",$companyid);
         $this->db->where("picking_date",$date);
         $logs=$this->db->get("tb_pickingandreceiving")->result();
         $result["logs"]=$logs;
         
         $this->db->where("me_company",$companyid);
        $methodlist=$this->db->get("tb_processingmethods")->result();
         
         
         $result["methods"]=$methodlist;
         $this->doRespondSuccess($result);
         
     }
     
     function addnewprocessiong(){      // this on e
         $result=array();
         $company_id=$_POST["company_id"];
         $processing_type=$_POST["processing_type"];
         $receivingorpickinglog_id=$_POST["receivingorpickinglog_id"];
         $processing_date=$_POST["processing_date"];
         $lbs_parchmentproduced=$_POST["lbs_parchmentproduced"];
         $parchment_date=$_POST["parchment_date"];
         $moisture=$_POST["moisture"];
         $processed_lbs=$_POST["processed_lbs"];
         $method=$_POST["method"];
        
        
         $processing_farmorclientname=$_POST["processing_farmorclientname"];
         $burlap_bags=$_POST["burlap_bags"];
         $grainpro_bags=$_POST["grainpro_bags"];
         $comment=$_POST["comment"];
         $status=$_POST["status"];  // pending or closed
         
        
         
         $processing_type=$this->doUrlDecode($processing_type);
         $receivingorpickinglog_id=$this->doUrlDecode($receivingorpickinglog_id);
         $processing_date=$this->doUrlDecode($processing_date);
         $lbs_parchmentproduced=$this->doUrlDecode($lbs_parchmentproduced);
         $parchment_date=$this->doUrlDecode($parchment_date);
         $moisture=$this->doUrlDecode($moisture);
         $processed_lbs=$this->doUrlDecode($processed_lbs);
         $method=$this->doUrlDecode($method);
          $parchment_date=$this->doUrlDecode($parchment_date);
         
         $processing_farmorclientname=$this->doUrlDecode($processing_farmorclientname);
         $burlap_bags=$this->doUrlDecode($burlap_bags);
         $grainpro_bags=$this->doUrlDecode($grainpro_bags);
         $comment=$this->doUrlDecode($comment);
         $status=$this->doUrlDecode($status);
         
         
         
         
            /*--if($processing_type==0){  // if it's picking log 
	   	    $this->db->where("picking_id",$receivingorpickinglog_id);
	   	    $farmid=$this->db->get("tb_pickingandreceiving")->result()[0]->picking_farm;
	   	    
	   	    $this->db->where("product_farm",$farmid);
	   	    $this->db->where("product_name","Cherries");
	   	    $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
	   	    
	   	  //  echo $currentquantity;
	   	    
	   	    $this->db->where("product_farm",$farmid);
	   	    $this->db->where("product_name","Cherries");
	   	    
	   	    $totalusedquantity=$lbs_parchmentproduced+$skin_quantity;
	   	    
	   	    if($totalusedquantity>$currentquantity){
	   	       $this->doRespondFail("Current available Cherries is less than requested lbs.");
	   	    }else{
	   	       $this->Api_model->addnewprocessinglog($company_id,$processing_type,$receivingorpickinglog_id,$processing_date,$lbs_parchmentproduced,$parchment_date,$moisture,$processed_lbs,$method,$processing_farmorclientname,$burlap_bags,$grainpro_bags,$comment,$status);
         $this->doRespondSuccess($result);
	   	    }
	   	    
	   }else{ --*/
	     $data = $this->Api_model->addnewprocessinglog($company_id,$processing_type,$receivingorpickinglog_id,$processing_date,$lbs_parchmentproduced,$parchment_date,$moisture,$processed_lbs,$method,$processing_farmorclientname,$burlap_bags,$grainpro_bags,$comment,$status);
         
         // generate invoice pdf
         
         if ($status == "Closed") {
             
             $result['message'] = $this->generateInvoice($data);             
         }                                                      
         
         $this->doRespondSuccess($result);
	  // }
        
     }
     
     
     function updateprocessinglog(){
         $result=array();
         $processingid=$_POST["processingid"];
         $company_id=$_POST["company_id"];
         $processing_type=$_POST["processing_type"];
         $receivingorpickinglog_id=$_POST["receivingorpickinglog_id"];
         $processing_date=$_POST["processing_date"];
         $lbs_parchmentproduced=$_POST["lbs_parchmentproduced"];
         $parchment_date=$_POST["parchment_date"];
         $moisture=$_POST["moisture"];
         $processed_lbs=$_POST["processed_lbs"];
         $method=$_POST["method"];
        
        
         $processing_farmorclientname=$_POST["processing_farmorclientname"];
         $burlap_bags=$_POST["burlap_bags"];
         $grainpro_bags=$_POST["grainpro_bags"];
         $comment=$_POST["comment"];
         $status=$_POST["status"];  // pending or closed
         
        
         
         $processing_type=$this->doUrlDecode($processing_type);
         $receivingorpickinglog_id=$this->doUrlDecode($receivingorpickinglog_id);
         $processing_date=$this->doUrlDecode($processing_date);
         $lbs_parchmentproduced=$this->doUrlDecode($lbs_parchmentproduced);
         $parchment_date=$this->doUrlDecode($parchment_date);
         $moisture=$this->doUrlDecode($moisture);
         $processed_lbs=$this->doUrlDecode($processed_lbs);
         $method=$this->doUrlDecode($method);
         $parchment_date=$this->doUrlDecode($parchment_date);
         
         $processing_farmorclientname=$this->doUrlDecode($processing_farmorclientname);
         $burlap_bags=$this->doUrlDecode($burlap_bags);
         $grainpro_bags=$this->doUrlDecode($grainpro_bags);
         $comment=$this->doUrlDecode($comment);
         $status=$this->doUrlDecode($status);
         
         $data = $this->Api_model->updateprocessinglog($processingid,$company_id,$processing_type,$receivingorpickinglog_id,$processing_date,$lbs_parchmentproduced,$parchment_date,$moisture,$processed_lbs,$method,$processing_farmorclientname,$burlap_bags,$grainpro_bags,$comment,$status);
         
         if ($status == "Closed") {             
              $result['message'] = $this->generateInvoice($data);             
         }
         
        
         $this->doRespondSuccess($result);
     }
     
     
     
     
     
     function getAllprocessinglog(){
         $result=array();
         $company_id=$_POST["company_id"];
         $filtertype=$_POST["filtertype"];
         $farmorclientname=$_POST["farmorclientname"];         
         $processingdate=$_POST["processingdate"];
         
         $company_id=$this->doUrlDecode($company_id);
         $filtertype=$this->doUrlDecode($filtertype);
         $farmorclientname=$this->doUrlDecode($farmorclientname);        
         $processingdate=$this->doUrlDecode($processingdate);
         
         $this->db->where("company_id",$company_id);
         
         if($filtertype==0){                      // all    
         }else if($filtertype==1){                // filter client
           $this->db->where("processing_type", 1);
           $this->db->where("processing_farmorclientname", $farmorclientname);
         }else if($filtertype==2){               // filter farm
           $this->db->where("processing_type", 0);
           $this->db->where("processing_farmorclientname", $farmorclientname);
         }
         
         if($processingdate=="test"){
         }else{
	         $daterange=explode("_",$processingdate);
	         $startdate=$daterange[0];
	         $enddate=$daterange[1];
	         if($startdate>$enddate){
	             $tempdate=$startdate;
	             $startdate=$enddate;
	             $enddate=$tempdate;
	         }
	         
	               
	         $this->db->where('processing_date>=', $startdate);
	         $this->db->where('processing_date<=',$enddate);        
	         
         }
         
          $this->db->order_by("processing_date", "DESC");
         $logs=$this->db->get("tb_processing")->result();
         
         $pickersarrays=array();
         
         foreach($logs as $log){
           $receivingpickingid=$log->receivingorpickinglog_id;
           
           $this->db->where("picking_id", $receivingpickingid);
           $picking=$this->db->get("tb_pickingandreceiving")->result()[0];
           
           $pickedlbs=$picking->picking_lbs;
           $usedlbs=$picking->used_lbscherries;
           //$availablelbs=(int)$pickedlbs-(int)$usedlbs;
           
            $onepicker= array(
		                 'usedlbs'             =>    $usedlbs,
		                 'pickedlbs'           =>   $pickedlbs,
		                 'processing_id'        =>    $log->processing_id,
		                 'processing_type'         =>    $log->processing_type,
		                 'receivingorpickinglog_id'   =>    $log->receivingorpickinglog_id,
		                 'processing_date'      =>    $log->processing_date,
		                 'parchment_date'     =>    $log->parchment_date,
		                 'moisture'     =>    $log->moisture,
		                 
		                 'method'  =>    $log->method,
		               
		                 'processing_farmorclientname'          =>    $log->processing_farmorclientname,
		                 
		                  'lbs_parchmentproduced'     =>    $log->lbs_parchmentproduced,
		                 'burlap_bags'     =>    $log->burlap_bags,
		                 'grainpro_bags'   =>    $log->grainpro_bags,
		                 'comment'  =>    $log->comment,
		                 'company_id'              =>    $log->company_id,
		                 'status'          =>    $log->status,
		                  'processed_lbs'          =>    $log->processed_lbs
		                 
		    );
	  array_push($pickersarrays,$onepicker);
         }
         
         $result["log"]=$pickersarrays;
         
         
         $this->doRespondSuccess($result);
         
     }
     
     function deleteprocessing($processingid){
      $result=array();
      $this->Api_model->deleteprocessing($processingid);
       $this->doRespondSuccess($result);
     }
     
     
     
     
     
     
    
     
     
     
     
     function getallfarmbycompanyid(){
        $result=array();         
         $companyid=$_POST["company_id"];        
         $companyid=$this->doUrlDecode($companyid);
         $this->db->where('company_id', $companyid);         
         $farms = $this->db->get('tb_farm')->result();
         $result['farms']=$farms;
         $this->doRespondSuccess($result);
     }
     
     function getallfarmersbycompanyid(){
        $result=array();   
      	$companyid=$_POST["company_id"]; 
      	
      	$this->db->where('company_id', $companyid);         
         $farms = $this->db->get('tb_farm')->result();
         $result['farms']=$farms;
         
         $this->db->where('farmer_company', $companyid);    
         $farmmers= $this->db->get('tb_workers')->result(); 
         $result['farmmers']=$farmmers;
         
         $this->doRespondSuccess($result);
     }
     
     function registernewfarmer(){
        $result=array();   
      	$companyid=$_POST["company_id"]; 
      	$farmsid=$_POST["farms_id"];  
      	$username=$_POST['user_name'];
      	$password=$_POST['password'];
      	$address=$_POST['address'];
      	$userphone=$_POST['phone'];
      	$useremail=$_POST['email'];
      	
      	 $username=$this->doUrlDecode($username);
      	 $password=$this->doUrlDecode($password);
      	 $address=$this->doUrlDecode($address);
      	 $userphone=$this->doUrlDecode($userphone);
      	 $useremail=$this->doUrlDecode($useremail);
      	 
      	  $result1=  $this->Api_model->AdminExist($useremail, $password);
           if($result1==0){  // not company email
           
              	  $result2=  $this->Api_model->Employeeexist($useremail, $password);
	           if($result2==0){
	                $this->Api_model->Registernewfarmer($companyid, $farmsid, $username,$password, $address, $userphone, $useremail );
	                $this->doRespondSuccess($result);
	           }else {
	               $this->doRespondFail("This email is already registered");
	           }
	           
           }else { //incorrect password
           
               $this->doRespondFail("This email already registered as a company manager");
               
           }      	      
         
     }
     
     function updatefarmer(){     
        $result=array();  
        $farmer_id=$_POST["farmer_id"]; 
      	$companyid=$_POST["company_id"]; 
      	$farmsid=$_POST["farms_id"];  
      	$username=$_POST['user_name'];
      	$password=$_POST['password'];
      	$address=$_POST['address'];
      	$userphone=$_POST['phone'];
      	$useremail=$_POST['email'];
      	
      	 $companyid=$this->doUrlDecode($companyid);
      	 $username=$this->doUrlDecode($username);
      	 $password=$this->doUrlDecode($password);
      	 $address=$this->doUrlDecode($address);
      	 $userphone=$this->doUrlDecode($userphone);
      	 $useremail=$this->doUrlDecode($useremail);
      	 
      	 $this->Api_model->updatefarmer($farmer_id ,$companyid, $farmsid, $username,$password, $address, $userphone, $useremail);
      	 $this->doRespondSuccess($result);
     }
     
    function deletefarmer($farmer_id){
        $result=array();  
       $this->db->where("farmer_id", $farmer_id);
       $this->db->delete("tb_workers");
       $this->doRespondSuccess($result);
    }
    
    
    //=========================================================================== Organic Module ===================================================================
    function addneworganiclog(){
        $result=array();  
        $organic_companyid=$_POST["organic_companyid"]; 
      	$organic_farmid=$_POST["organic_farmid"]; 
      	$organiclog_date=$_POST["organiclog_date"];  
      	$organiclog_acitivty=$_POST['organiclog_acitivty'];
      	$organiclog_productused=$_POST['organiclog_productused'];
      	$organiclog_productquantity=$_POST['organiclog_productquantity'];
      	$organiclog_refofbill=$_POST['organiclog_refofbill'];
      	$assigned_teamleader=$_POST['assigned_teamleader'];
      	$logstatus=$_POST['logstatus'];
      
      	
      	 $organic_companyid=$this->doUrlDecode($organic_companyid);
      	 $organic_farmid=$this->doUrlDecode($organic_farmid);
      	 $organiclog_date=$this->doUrlDecode($organiclog_date);
      	 $organiclog_acitivty=$this->doUrlDecode($organiclog_acitivty);
      	 $organiclog_productused=$this->doUrlDecode($organiclog_productused);
      	 $organiclog_productquantity=$this->doUrlDecode($organiclog_productquantity);
      	 $organiclog_refofbill=$this->doUrlDecode($organiclog_refofbill);
      	 $assigned_teamleader=$this->doUrlDecode($assigned_teamleader);
      	 $logstatus=$this->doUrlDecode($logstatus);
      	 
      	  if(strlen($organiclog_refofbill)>0){
      	    $this->db->where("expenses_invoice",$organiclog_refofbill);
      	    $this->db->where("expenses_company",$organic_companyid);
      	    $expenses=$this->db->get("tb_expensses");
      	    $numrows=$expenses->num_rows();
      	    if($numrows==0){
      	      $this->doRespondFail("There is no registered expenese");
      	    }else{
      	         $this->Api_model->registerneworganic($organic_companyid,$organic_farmid, $organiclog_date, $organiclog_acitivty,$organiclog_productused, $organiclog_productquantity, $organiclog_refofbill, $assigned_teamleader, $logstatus);
        	 $this->doRespondSuccess($result);
      	    }
      	  }else{
      	        $this->Api_model->registerneworganic($organic_companyid,$organic_farmid, $organiclog_date, $organiclog_acitivty,$organiclog_productused, $organiclog_productquantity, $organiclog_refofbill, $assigned_teamleader, $logstatus);
      	        $this->doRespondSuccess($result);
      	  }
      	 
      	
    
    }
    
    function updateorganiclog(){
        $result=array();  
        $organiclog_id=$_POST["organiclog_id"]; 
        $organic_companyid=$_POST["organic_companyid"]; 
      	$organic_farmid=$_POST["organic_farmid"]; 
      	$organiclog_date=$_POST["organiclog_date"];  
      	$organiclog_acitivty=$_POST['organiclog_acitivty'];
      	$organiclog_productused=$_POST['organiclog_productused'];
      	$organiclog_productquantity=$_POST['organiclog_productquantity'];
      	$organiclog_refofbill=$_POST['organiclog_refofbill'];
      	$assigned_teamleader=$_POST['assigned_teamleader'];
      	$logstatus=$_POST['logstatus'];
      
      
         $organiclog_id=$this->doUrlDecode($organiclog_id);	
      	 $organic_companyid=$this->doUrlDecode($organic_companyid);
      	 $organic_farmid=$this->doUrlDecode($organic_farmid);
      	 $organiclog_date=$this->doUrlDecode($organiclog_date);
      	 $organiclog_acitivty=$this->doUrlDecode($organiclog_acitivty);
      	 $organiclog_productused=$this->doUrlDecode($organiclog_productused);
      	 $organiclog_productquantity=$this->doUrlDecode($organiclog_productquantity);
      	 $organiclog_refofbill=$this->doUrlDecode($organiclog_refofbill);
      	 $assigned_teamleader=$this->doUrlDecode($assigned_teamleader);
      	 $logstatus=$this->doUrlDecode($logstatus);
      	 
      	 if(strlen($organiclog_refofbill)>0){
      	    $this->db->where("expenses_invoice",$organiclog_refofbill);
      	    $this->db->where("expenses_company",$organic_companyid);
      	    $expenses=$this->db->get("tb_expensses");
      	    $numrows=$expenses->num_rows();
      	    if($numrows==0){
      	      $this->doRespondFail("There is no registered expenese");
      	    }else{
      	         $this->Api_model->updateorganic($organiclog_id, $organic_companyid,$organic_farmid, $organiclog_date, $organiclog_acitivty,$organiclog_productused, $organiclog_productquantity, $organiclog_refofbill, $assigned_teamleader, $logstatus);
      	         $this->doRespondSuccess($result);
      	    }
      	  }else{
      	       $this->Api_model->updateorganic($organiclog_id, $organic_companyid,$organic_farmid, $organiclog_date, $organiclog_acitivty,$organiclog_productused, $organiclog_productquantity, $organiclog_refofbill, $assigned_teamleader, $logstatus);
      	       $this->doRespondSuccess($result);
      	  }
      	 
      	 
    }
    
    function updategoganicfromteamleaderside(){
       $result=array();  
        $organiclog_id=$_POST["organiclog_id"]; 
        $organic_workers=$_POST["organic_workers"]; 
        $organic_times=$_POST["organic_times"]; 
        $organicquantiy=$_POST["quantity"]; 
        
         $organiclog_id=$this->doUrlDecode($organiclog_id);	
      	 $organic_workers=$this->doUrlDecode($organic_workers);
      	 $organic_times=$this->doUrlDecode($organic_times);
      	 $organicquantiy=$this->doUrlDecode($organicquantiy);
      	 $this->db->where("organiclog_id", $organiclog_id);
      	 $cnt = $this->db->get('tb_organiclog')->num_rows();
              if ($cnt == 0) {
                $this->doRespondFail("There is no such organic log");
            }else {               
                 $this->db->where("organiclog_id", $organiclog_id);
                 $this->db->set("organic_workers", $organic_workers);
                 $this->db->set("organic_times", $organic_times);
                 $this->db->set("organic_status", 1);
                 $this->db->set("organiclog_productquantity", $organicquantiy);
      	         $this->db->update('tb_organiclog');
      	         $this->doRespondSuccess($result);
            }
      	 
      	 
      	 
      	 
    }
    
    function getallorganiclogbyfarmid(){
        $result=array();  
        $farm_id=$_POST["farm_id"]; 
        $date1=$_POST["date1"]; 
        $companyid=$_POST["company_id"];
        $teamleaderid=$_POST["teamleader_id"];
        
        
         $date1=$this->doUrlDecode($date1);
        
        if($date1=="test"){
	        if($farm_id=="All"){
	          $this->db->where("organic_companyid", $companyid);
	        }else{
	          $this->db->where("organic_farmid", $farm_id);
	        }
	        if($teamleaderid=="All"){
	        }else{
	         $this->db->where("organic_teamlead", $teamleaderid);
	        }
          $this->db->order_by("organiclog_date", "DESC");
          $logs=$this->db->get("tb_organiclog")->result();
          $result["log"]=$logs;
           $this->doRespondSuccess($result);
        }else{
         $daterange=explode("_",$date1);
         $startdate=$daterange[0];
         $enddate=$daterange[1];
	         if($startdate>$enddate){
	             $this->doRespondFail("End date should be big than start date.");
	         }else{
	         
	               if($farm_id=="All"){
		          $this->db->where("organic_companyid", $companyid);
		        }else{
		          $this->db->where("organic_farmid", $farm_id);
		        }
		        
		        if($teamleaderid=="All"){
		        }else{
		         $this->db->where("organic_teamlead", $teamleaderid);
		        }
		         $this->db->where('organiclog_date>=', $startdate);
		         $this->db->where('organiclog_date<=',$enddate);
		          $this->db->order_by("organiclog_date", "DESC");
		          $logs=$this->db->get("tb_organiclog")->result();
		          $result["log"]=$logs;
		         
		         $this->doRespondSuccess($result);
	         }
        }
    }
    
    function deleteorganic($organiclog_id){
          $result=array();
   	  $this->db->where("organiclog_id", $organiclog_id);
   	  $this->db->delete("tb_organiclog");
   	  $this->doRespondSuccess($result);
    }
    
    
    function addnewproduction(){
       $result=array();  
        $production_companyid=$_POST["production_companyid"]; 
      	$production_farmid=$_POST["production_farmid"]; 
      	$production_date=$_POST["production_date"];  
      	$production_product=$_POST['production_product'];      // product id 
      	$production_quantity=$_POST['production_quantity'];   // product quantity . should be added in product table quantity, Should be a number
      	$production_roasted=$_POST['production_roasted'];
      	$production_batch=$_POST['production_batch'];
      	$production_moisture=$_POST['production_moisture'];
      	$production_millprocessed=$_POST['production_millprocessed'];
      
      	
      	 $production_companyid=$this->doUrlDecode($production_companyid);
      	 $production_farmid=$this->doUrlDecode($production_farmid);
      	 $production_date=$this->doUrlDecode($production_date);
      	 $production_product=$this->doUrlDecode($production_product);
      	 $production_quantity=$this->doUrlDecode($production_quantity);
      	  $production_roasted=$this->doUrlDecode($production_roasted);
      	 $production_batch=$this->doUrlDecode($production_batch);
      	 $production_moisture=$this->doUrlDecode($production_moisture);
      	 $production_millprocessed=$this->doUrlDecode($production_millprocessed);
      	 
      	 $this->Api_model->registernewproduction($production_companyid, $production_farmid,  $production_date,  $production_product,$production_quantity, $production_roasted, $production_batch, $production_moisture, $production_millprocessed );
      	 $this->doRespondSuccess($result);
    
    }
    
    function deleteproduction($production_id){
         $result=array();  
         $this->db->where("production_id", $production_id);
         $currentitem=$this->db->get("tb_production");
         
         $currentquantity=$currentitem->result()[0]->production_quantity;
         $currentproduct=$currentitem->result()[0]->production_product;  
         $currentcompany=$currentitem->result()[0]->production_companyid;
         $currentroast=$currentitem->result()[0]->production_roasted; 
       
         $this->db->where("product_company",$currentcompany);
         $this->db->where("product_name",$currentproduct);
         $currentquantityinproducttable=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
         
        
        
        // decrease previouse product's quantity 
         $this->db->where("product_company",$currentcompany);
         $this->db->where("product_name",$currentproduct);
         $this->db->set("product_quantity", $currentquantityinproducttable-$currentquantity);
         $this->db->update("tb_productforproduction");
         
          if(strlen($currentroast)>0){
                  $this->db->where("product_company",$currentcompany);
	         $this->db->where("product_name",$currentroast);
	         $currentquantityinproducttableofroast=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
	         
	         // increase previouse product's quantity 
	         $this->db->where("product_company",$currentcompany);
	         $this->db->where("product_name",$currentroast);
	         $this->db->set("product_quantity", $currentquantityinproducttableofroast + $currentquantity);
	         $this->db->update("tb_productforproduction");
         }
         
         $this->db->where("production_id", $production_id);
   	 $this->db->delete("tb_production");
   	 $this->doRespondSuccess($result);
   	 
   	 
        
        
    }
    
    function updateproduction(){
        $result=array();  
        $production_id=$_POST["production_id"];
        $production_companyid=$_POST["production_companyid"]; 
      	$production_farmid=$_POST["production_farmid"]; 
      	$production_date=$_POST["production_date"];  
      	$production_product=$_POST['production_product'];      // product id 
      	$production_quantity=$_POST['production_quantity'];   // product quantity . should be added in product table quantity, Should be a number
      	$production_batch=$_POST['production_batch'];      	
      	$production_moisture=$_POST['production_moisture'];
      	$production_millprocessed=$_POST['production_millprocessed'];
      
      	
      	 $production_companyid=$this->doUrlDecode($production_companyid);
      	 $production_farmid=$this->doUrlDecode($production_farmid);
      	 $production_date=$this->doUrlDecode($production_date);
      	 $production_product=$this->doUrlDecode($production_product);
      	 $production_quantity=$this->doUrlDecode($production_quantity);
      	 $production_batch=$this->doUrlDecode($production_batch);
      	 $production_moisture=$this->doUrlDecode($production_moisture);
      	 $production_millprocessed=$this->doUrlDecode($production_millprocessed);
      	 
      	 $this->Api_model->updateproduction($production_id, $production_companyid, $production_farmid,  $production_date,  $production_product,$production_quantity, $production_batch, $production_moisture, $production_millprocessed );
      	 $this->doRespondSuccess($result);
    }
    
    function getAllproductionbyfarmid(){
        $result=array();  
        $farm_id=$_POST["farm_id"]; 
        $date1=$_POST["date1"];        
        $companyid=$_POST["companyid"];
        $productid=$_POST["product"];
        
         $date1=$this->doUrlDecode($date1);
         $productid=$this->doUrlDecode($productid);
         $farm_id=$this->doUrlDecode($farm_id);
        
        if($date1=="test"){
               if($farm_id=="All"){
	          $this->db->where("production_companyid", $companyid);
	        }else{
	          $this->db->where("production_farmid", $farm_id);
	        }
        if($productid=="All"){
	         
        }else{
          $this->db->where("production_product", $productid);
        }
        $this->db->order_by("production_date", "DESC");
         
          $logs=$this->db->get("tb_production")->result();
          $result["productions"]=$logs;
          $this->doRespondSuccess($result);
        }else{
         $daterange=explode("_",$date1);
         $startdate=$daterange[0];
         $enddate=$daterange[1];
           if($startdate>$enddate){
	             $this->doRespondFail("End date should be big than start date.");
	    }else{
         
               if($farm_id=="All"){
	          $this->db->where("production_companyid", $companyid);
	        }else{
	          $this->db->where("production_farmid", $farm_id);
	        };
	        if($productid=="All"){
	         
	        }else{
	          $this->db->where("production_product", $productid);
	        }
	        
	         $this->db->where('production_date>=', $startdate);
	         $this->db->where('production_date<=',$enddate);
	         $this->db->order_by("production_date", "DESC");	        
	          $logs=$this->db->get("tb_production")->result();
	          $result["productions"]=$logs;
	         
	         $this->doRespondSuccess($result);
           }
        }
    }
    
    
    
    function registernewincome(){
        $result=array();  
        $income_company=$_POST["income_company"];
        $income_date=$_POST["income_date"]; 
      	$income_category=$_POST["income_category"]; 
      	$income_client=$_POST["income_client"];  
      	$income_invoice=$_POST['income_invoice'];     
      	$income_amount=$_POST['income_amount'];   
      	$income_taxstatus=$_POST['income_taxstatus'];      	
      	$income_invicestatus=$_POST['income_invicestatus'];
      	$income_productsold=$_POST['income_productsold'];   // mulitpule product ids
      	$income_productquantity=$_POST['income_productquantity'];  // multiple product quantities
      
      	
      	 $income_company=$this->doUrlDecode($income_company);
      	 $income_date=$this->doUrlDecode($income_date);     
      	 $income_category=$this->doUrlDecode($income_category);
      	 $income_client=$this->doUrlDecode($income_client);
      	 $income_invoice=$this->doUrlDecode($income_invoice);
      	 $income_amount=$this->doUrlDecode($income_amount);
      	 $income_taxstatus=$this->doUrlDecode($income_taxstatus);
      	 $income_invicestatus=$this->doUrlDecode($income_invicestatus);
      	 $income_productsold=$this->doUrlDecode($income_productsold);
      	 $income_productquantity=$this->doUrlDecode($income_productquantity);
      	 
      	 $this->Api_model->registernewincome($income_company, $income_date, $income_category,  $income_client,  $income_invoice,$income_amount, $income_taxstatus, $income_invicestatus, $income_productsold,$income_productquantity);
      	 $this->doRespondSuccess($result);
    }
    
    function deleteincomebyid($income_id){
         $result=array();  
         $incomeproductquantity=array();
         $this->db->where("income_id", $income_id);
         $currentitem=$this->db->get("tb_income")->result();
         $currentproductsolde=$currentitem[0]->income_productsold;
         $currentproductquantity=$currentitem[0]->income_productquantity;  
         
         
           $incomeproductsold=explode(",",$currentproductsolde);
   	   $incomeproductquantity=explode(",",$currentproductquantity);
   	  
   	   $i=0;
   	   
   	   if(sizeof($incomeproductsold)>0 && strlen($incomeproductsold[0])>0){
   	   
   	   foreach($incomeproductsold as $oneproduct){
   	     $this->db->where("product_name",$oneproduct);
   	     $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
   	     
   	  //    print_r($oneproduct);
   	   // print_r($currentquantity);
   	  //  print_r($incomeproductquantity[$i]);
   	     
   	     $this->db->where("product_name",$oneproduct);
   	     $this->db->set("product_quantity",$currentquantity+$incomeproductquantity[$i]);
   	     $this->db->update("tb_productforproduction");
   	     
   	     $i++;
   	     
   	   }
   	   }
         
         
       
        
         
         $this->db->where("income_id", $income_id);
   	 $this->db->delete("tb_income");
   	 $this->doRespondSuccess($result);
    }
    
    function getallincomebycompanyid(){
        $result=array();  
        $company_id=$_POST["company_id"]; 
        $client=$_POST["client"]; 
        $category=$_POST["category"]; 
        $date1=$_POST["date1"]; 
        $checkstatus=$_POST["checkstatus"];
        
         $date1=$this->doUrlDecode($date1);
         $category=$this->doUrlDecode($category);
         $client=$this->doUrlDecode($client);
         $checkstatus=$this->doUrlDecode($checkstatus);
        
        if($date1=="test"){
	          $this->db->where("income_company", $company_id);
	          if($client!="All"){
	            $this->db->where("income_client", $client);
	          }
	          if($category!="All"){
	            $this->db->where("	income_category", $category);
	          }
	          if($checkstatus!="All"){
	            $this->db->where("income_invicestatus", $checkstatus);
	          }
	          
	          $this->db->order_by("income_date", "DESC");
	          $logs=$this->db->get("tb_income")->result();
	          $result["incomes"]=$logs;
	          $this->doRespondSuccess($result);
        }else{
	         $daterange=explode("_",$date1);
	         $startdate=$daterange[0];
	         $enddate=$daterange[1];
	         
	          if($startdate>$enddate){
	             $this->doRespondFail("End date should be big than start date.");
	         }else{
	         
		         $this->db->where("income_company", $company_id);
		          if($client!="All"){
		            $this->db->where("income_client", $client);
		          }
		          if($category!="All"){
		            $this->db->where("	income_category", $category);
		          }
		          if($checkstatus!="All"){
		            $this->db->where("income_invicestatus", $checkstatus);
		          }
		         $this->db->where('income_date>=', $startdate);
		         $this->db->where('income_date<=',$enddate);
		         
		          $this->db->order_by("income_date", "DESC");
		        
		          $logs=$this->db->get("tb_income")->result();
		          $result["incomes"]=$logs;
		         
		         $this->doRespondSuccess($result);
	         }
        }
    }
    
    function updateincomestatus(){
        $result=array();  
        $income_id=$_POST["income_id"]; 
        $status=$_POST["status"]; 
        
         $date1=$this->doUrlDecode($status);
         
         $this->db->where("income_id", $income_id);
         $this->db->set("income_invicestatus",$status);
         $this->db->update("tb_income");
         $this->doRespondSuccess($result);
    }
    
    function updateincomedate(){
        $result=array();  
        $income_id=$_POST["income_id"]; 
        $status=$_POST["date"]; 
        
         $date1=$this->doUrlDecode($status);
         
         $this->db->where("income_id", $income_id);
         $this->db->set("income_date",$status);
         $this->db->update("tb_income");
         $this->doRespondSuccess($result);
    }
    
    function registernewexpenese(){
        $result=array();  
        $expenses_company=$_POST["expenses_company"];
        $expenses_date=$_POST["expenses_date"]; 
      	$expenses_invoice=$_POST["expenses_invoice"]; 
      	$expenses_provider=$_POST["expenses_provider"];  
      	$expenses_amount=$_POST['expenses_amount'];     
      	$expenses_farm=$_POST['expenses_farm'];   
      	$expenses_category=$_POST['expenses_category'];      	
      	
      
      	
      	 $expenses_company=$this->doUrlDecode($expenses_company);
      	 $expenses_date=$this->doUrlDecode($expenses_date);     
      	 $expenses_invoice=$this->doUrlDecode($expenses_invoice);
      	 $expenses_provider=$this->doUrlDecode($expenses_provider);
      	 $expenses_amount=$this->doUrlDecode($expenses_amount);
      	 $expenses_farm=$this->doUrlDecode($expenses_farm);
      	 $expenses_category=$this->doUrlDecode($expenses_category);
      	
      	 
      	 $this->Api_model->registernewexpenese($expenses_company, $expenses_date, $expenses_invoice,  $expenses_provider,  $expenses_amount, $expenses_farm, $expenses_category);
      	 $this->doRespondSuccess($result);
    }
    
    function updateexpenses(){
        $result=array();  
        $expenses_id=$_POST["expenses_id"];
        $expenses_company=$_POST["expenses_company"];
        $expenses_date=$_POST["expenses_date"]; 
      	$expenses_invoice=$_POST["expenses_invoice"]; 
      	$expenses_provider=$_POST["expenses_provider"];  
      	$expenses_amount=$_POST['expenses_amount'];     
      	$expenses_farm=$_POST['expenses_farm'];   
      	$expenses_category=$_POST['expenses_category'];      	
      	
      
      	$expenses_id=$this->doUrlDecode($expenses_id);
      	 $expenses_company=$this->doUrlDecode($expenses_company);
      	 $expenses_date=$this->doUrlDecode($expenses_date);     
      	 $expenses_invoice=$this->doUrlDecode($expenses_invoice);
      	 $expenses_provider=$this->doUrlDecode($expenses_provider);
      	 $expenses_amount=$this->doUrlDecode($expenses_amount);
      	 $expenses_farm=$this->doUrlDecode($expenses_farm);
      	 $expenses_category=$this->doUrlDecode($expenses_category);
      	
      	 
      	 $this->Api_model->updateexpenese($expenses_id, $expenses_company, $expenses_date, $expenses_invoice,  $expenses_provider,  $expenses_amount, $expenses_farm, $expenses_category);
      	 $this->doRespondSuccess($result);
    }
    
    function deleteexpense($expenses_id){
          $result=array();
   	  $this->db->where("expenses_id", $expenses_id);
   	  $this->db->delete("tb_expensses");
   	  $this->doRespondSuccess($result);
    }
    
    function getallexpensebyfarmid(){
        $result=array();  
        $expenses_farm=$_POST["expenses_farm"]; 
        $date1=$_POST["date1"]; 
        $company_id=$_POST["company_id"];
        $category=$_POST["category"];
        
        
         $date1=$this->doUrlDecode($date1);
         $category=$this->doUrlDecode($category);
        
        if($date1=="test"){
        
              if($expenses_farm=="All"){
	          $this->db->where("expenses_company", $company_id);
	        }else{
	           $this->db->where("expenses_farm", $expenses_farm);
	        }
	        
	        if($category=="All"){
	        }else{	         
	         $this->db->where("expenses_category",$category);
	        }
          $this->db->order_by("expenses_date", "DESC");
          $logs=$this->db->get("tb_expensses")->result();
          $result["expenses"]=$logs;
          $this->doRespondSuccess($result);
        }else{
         $daterange=explode("_",$date1);
         $startdate=$daterange[0];
         $enddate=$daterange[1];
          if($startdate>$enddate){
		 $this->doRespondFail("End date should be big than start date.");
	   }else{
         
               if($expenses_farm=="All"){
	          $this->db->where("expenses_company", $company_id);
	        }else{
	           $this->db->where("expenses_farm", $expenses_farm);
	        }
	         $this->db->where('expenses_date>=', $startdate);
	         $this->db->where('expenses_date<=',$enddate);
	         
	        if($category=="All"){
	        }else{
	        $this->db->where("expenses_category",$category);
	        }
        
	         $logs=$this->db->get("tb_expensses")->result();
	         $result["expenses"]=$logs;	         
	         $this->doRespondSuccess($result);
          }
        }
    }
    
    
    function registernewroastinglog(){
        $result=array();  
        $roast_company=$_POST["roast_company"];
        $roast_farm=$_POST["roast_farm"];
        $roast_date=$_POST["roast_date"]; 
      	$roast_typein=$_POST["roast_typein"]; 
      	$roast_typeout=$_POST["roast_typeout"];  
      	$roast_intemp=$_POST['roast_intemp'];     
      	$roast_outtemp=$_POST['roast_outtemp'];   
      	$roast_duration=$_POST['roast_duration']; 
      	$roast_lbsin=$_POST['roast_lbsin'];  
      	$roast_lbsout=$_POST['roast_lbsout'];      	
      	
      
      	
      	 $roast_company=$this->doUrlDecode($roast_company);
      	 $roast_farm=$this->doUrlDecode($roast_farm);
      	 $roast_date=$this->doUrlDecode($roast_date);     
      	 $roast_typein=$this->doUrlDecode($roast_typein);
      	 $roast_typeout=$this->doUrlDecode($roast_typeout);
      	 $roast_intemp=$this->doUrlDecode($roast_intemp);
      	 $roast_outtemp=$this->doUrlDecode($roast_outtemp);
      	 $roast_duration=$this->doUrlDecode($roast_duration);
      	 $roast_lbsin=$this->doUrlDecode($roast_lbsin);
      	 $roast_lbsout=$this->doUrlDecode($roast_lbsout);
      	
      	 
      	 $this->Api_model->registernewroastinglog($roast_company, $roast_farm ,$roast_date, $roast_typein,  $roast_typeout,  $roast_intemp, $roast_outtemp, $roast_duration, $roast_lbsin, $roast_lbsout);
      	 $this->doRespondSuccess($result);
    }
    
    function updateroastinglog(){
        $result=array();  
        $id=$_POST["id"];
        $roast_company=$_POST["roast_company"];
        $roast_farm=$_POST["roast_farm"]; 
        $roast_date=$_POST["roast_date"]; 
      	$roast_typein=$_POST["roast_typein"]; 
      	$roast_typeout=$_POST["roast_typeout"];  
      	$roast_intemp=$_POST['roast_intemp'];     
      	$roast_outtemp=$_POST['roast_outtemp'];   
      	$roast_duration=$_POST['roast_duration']; 
      	$roast_lbsin=$_POST['roast_lbsin'];  
      	$roast_lbsout=$_POST['roast_lbsout'];      	
      	
      
      	
      	 $roast_company=$this->doUrlDecode($roast_company);
      	  $roast_farm=$this->doUrlDecode($roast_farm);
      	 $roast_date=$this->doUrlDecode($roast_date);     
      	 $roast_typein=$this->doUrlDecode($roast_typein);
      	 $roast_typeout=$this->doUrlDecode($roast_typeout);
      	 $roast_intemp=$this->doUrlDecode($roast_intemp);
      	 $roast_outtemp=$this->doUrlDecode($roast_outtemp);
      	 $roast_duration=$this->doUrlDecode($roast_duration);
      	 $roast_lbsin=$this->doUrlDecode($roast_lbsin);
      	 $roast_lbsout=$this->doUrlDecode($roast_lbsout);
      	
      	 
      	 $this->Api_model->updateroastinglog($id, $roast_company,  $roast_farm, $roast_date, $roast_typein,  $roast_typeout,  $roast_intemp, $roast_outtemp, $roast_duration, $roast_lbsin, $roast_lbsout);
      	 $this->doRespondSuccess($result);
    }
    
    function deleteroastinglog($id){
          $result=array();
          $this->db->where("id", $id);
          $item=$this->db->get("tb_roastlog")->result()[0];
          
          $currenttypein=$item->roast_typein;
          $lbsin=$item->	roast_lbsin;
          $currenttypeout=$item->roast_typeout;
          $lbsout=$item->roast_lbsout;
          
          if(strlen($currenttypein)>0){
                    $this->db->where("product_name",$currenttypein);
                    $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
                    
                    $this->db->where("product_name",$currenttypein);
                    $this->db->set("product_quantity",$currentquantity+$lbsin);
                    $this->db->update("tb_productforproduction");
                    
                  }
                  
                  if(strlen($currenttypeout)>0){
                    $this->db->where("product_name",$currenttypeout);
                    $currentquantity=$this->db->get("tb_productforproduction")->result()[0]->product_quantity;
                    
                    $this->db->where("product_name",$currenttypeout);
                    $this->db->set("product_quantity",$currentquantity-$lbsout);
                    $this->db->update("tb_productforproduction");
                  }
          
          
   	  $this->db->where("id", $id);
   	  $this->db->delete("tb_roastlog");
   	  $this->doRespondSuccess($result);
    }
    
    function getallroastinglogbycompanyid(){
        $result=array();  
        $roast_company=$_POST["roast_company"]; 
        $date1=$_POST["date1"]; 
        $farm_id=$_POST["farm_id"];
        
         $date1=$this->doUrlDecode($date1);
        
        if($date1=="test"){
          
           if($farm_id=="All"){
	          $this->db->where("roast_company", $roast_company);
	   }else{
	          $this->db->where("roast_farm", $farm_id);
	   }
        
          //$this->db->where("roast_company", $roast_company);
           $this->db->order_by("id", "DESC");
          $logs=$this->db->get("tb_roastlog")->result();
          $result["roastinglog"]=$logs;
          $this->doRespondSuccess($result);
        }else{
		 $daterange=explode("_",$date1);
		 $startdate=$daterange[0];
		 $enddate=$daterange[1];         
		 if($startdate>$enddate){
		             $this->doRespondFail("End date should be big than start date.");
		  }else{	  
		     if($farm_id=="All"){
		          $this->db->where("roast_company", $roast_company);
		     }else{
		          $this->db->where("roast_farm", $farm_id);
		     }
		 
		// $this->db->where("roast_company", $roast_company);
			 $this->db->where('roast_date>=', $startdate);
			 $this->db->where('roast_date<=',$enddate);
			 $this->db->order_by("id", "DESC");
			 $logs=$this->db->get("tb_roastlog")->result();
			 $result["roastinglog"]=$logs;
			 
			 $this->doRespondSuccess($result);
		
		}
        }
        
    }
    
    function getallproduction($companyid){
                 $result=array();  
                 $this->db->where('product_company',$companyid); 
                 $this->db->order_by("product_quantity", "ASC"); // ASC  DESC      
	         $product= $this->db->get('tb_productforproduction')->result();
	         $result['productforproduction']=$product;
	         $this->doRespondSuccess($result);
    }
      function getallproducts(){
                 $result=array();  
                 
                 $roast_company=$_POST["company_id"]; 
                 $category=$_POST["category"]; 
       
        
                 $category=$this->doUrlDecode($category);
                 
                 $this->db->where('product_company',$roast_company); 
                 
                 if($category=="All"){
                 }else{
                 
                   $this->db->where('product_type',$category); 
                 }
                 
                 $this->db->order_by("product_quantity", "ASC"); // ASC  DESC      
	         $product= $this->db->get('tb_productforproduction')->result();
	         $result['productforproduction']=$product;
	         $this->doRespondSuccess($result);
    }
    
    
     
     
     //==================================================================
     
     
     //=============================================== New One================================================
     function uploadnightimage() {
     
         $photo_country =  $_POST["photo_country"];         
         $photo_date =  $_POST["photo_date"]; 
         $photo_desc =  $_POST["photo_desc"]; 
         
          $photo_country = $this->doUrlDecode($photo_country); 
          $photo_date = $this->doUrlDecode($photo_date); 
          $photo_desc = $this->doUrlDecode($photo_desc); 

         $result = array();   
         $image_name ="image_".time();
         $upload_path =  "uploadfiles/images/";  
             
         $upload_url = base_url().$upload_path;
         // Upload file          
         $w_uploadConfig = array(
                 'upload_path' => $upload_path,
                 'upload_url' => $upload_url,
                 'allowed_types' => "*",
                 'overwrite' => TRUE,
                 'max_size' => 1000000,
                 'max_width' => 4000,
                 'max_height' => 3000,
                 'file_name' => $image_name

             );

         $this->load->library('upload', $w_uploadConfig);
            
         if ($this->upload->do_upload('image')) { 
                
              $imageurl = $w_uploadConfig['upload_url'].$this->upload->file_name;  
              
              $this->Api_model->addNewphoto($photo_country, $photo_date, $photo_desc, $imageurl);
              
              //$result['image_url'] = $imageurl;
              $this->doRespondSuccess($result);
              
         } else {
             
             $this->doRespondFail("Upload failed");
         }
     }
     
    
     
     
     function sendPushNotification($sendername, $noticontent, $token){

       
            
            $message_title = $sendername;//$title;  
            $message_content = $noticontent;
            
            $url = "https://fcm.googleapis.com/fcm/send";
    //        api_key available in google-services.json

            $api_key = "AIzaSyAfRNs2IoYaOFPtVI-13oJQRW5bZXv8pus";

            $notification["to"] = $token;
            $notification["priority"] = "high" ;
                           
            $notification['notification'] = array(
                                    "body" => $message_content,
                                    "title" => $message_title,
                                    "sound" => "notisound.mp3",
                                    
                                    );

            $headers = array(
                            'Content-Type:application/json',
                            'Authorization:key='.$api_key
                            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($notification));

            $result = curl_exec($ch);

            if ($result === FALSE) {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);
        
        return;
    }
    
    function getallproviders($companyid){
     $result=array();
     $providers=array();
     $this->db->where("provider_company", $companyid);
     $providerarray=$this->db->get("tb_provider")->result();
     foreach($providerarray as $oneprovider){
     
       $thisyearamount=$thismonthamount=0;     
       $proivdername= $oneprovider->provider_name;
       $thisyear=date("Y");
       $thismonth=date("m");
       
       $this->db->where("expenses_provider",$proivdername);
       $providerhistory=$this->db->get("tb_expensses")->result();
       foreach($providerhistory as $onehistoryitem){
         $date=$onehistoryitem->expenses_date;
         $amount=$onehistoryitem->expenses_amount;
         
         $historyyear=explode("-",$date)[0];
         $histroymonthg=explode("-",$date)[1];
         
         if($thisyear == $historyyear){
           $thisyearamount+=$amount;
           if($thismonth == $histroymonthg){
           $thismonthamount+= $amount;
           }
         }
       }
       
     
      $oneitem= array(
                'provider_name' => $oneprovider->provider_name,
                'email' => $oneprovider->	provider_email,
                'phone' => $oneprovider->provider_phone,
                'address' => $oneprovider->provider_address,
                'year'   =>date("Y"),
                'month' =>date("m"),
                'thismonthamount' =>$thismonthamount,
                'thisyearamount'  =>$thisyearamount
               
            );
       
      array_push($providers, $oneitem);
     }
     $result["providers"]=$providers;
      $this->doRespondSuccess($result);
    
    }
    
    function sedverity($email){
    $result=array();
    $this->Api_model->setVerificationDetails($email);
  //  $result["results"]=$responses;
     $this->doRespondSuccess($result);
    }
     
     
     
     
     
   //===========%%%%%%%%%%%%%%%%%%%%%%%%%%%%================================  
     
     
    
    
    
}

?>